import numpy as np
import support_functions as sf
from matplotlib import pyplot as plt
import matplotlib as mpl
from matplotlib import rc
import scipy
PATH_TO_DATA = "simulation-data"
PATH_TO_VALIDATION_DATA = "Diverdata.mat"
def main():
    mpl.rcParams['axes.prop_cycle'] = mpl.cycler(color=["DDAA33", "DDAA33", "BB5566", "BB5566", "BB5566", "004488", "004488", "000000", "000000"]) 
    rc('text', usetex=True)
    bathymetry = scipy.io.loadmat("initial_bed_level.mat")['data']['Val'][0][0]

    simulations_increase = sf.return_simulation_cases(PATH_TO_DATA + "/21-50-data")
    simulations_decrease = sf.return_simulation_cases(PATH_TO_DATA + "/50-21-data")
    data = sf.return_validation_data(PATH_TO_VALIDATION_DATA)
    cycle = sf.validation_points_cycle(data)
    X, Y, _ = sf.read_coordinate_data(simulations_increase[0])
    
    data_decrease = sf.read_simulation_data(simulations_decrease[1], 'water_level_hles_no_slip')
    data_increase = sf.read_simulation_data(simulations_increase[1], 'water_level')
    
    validation_points = sf.return_validation_point_coordinates()

    ## Simulations

    plt.figure()
    validation_point = 1    
    test_increase = sf.normalize_data_increase(data_increase[10:55 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    test_decrease = sf.normalize_data_decrease(data_decrease[10:55 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    line1, = plt.plot(test_increase)
    plt.plot(np.flipud(test_decrease))

    # validation_point = 2
    # test_increase = sf.normalize_data_increase(data_increase[17:62 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # test_decrease = sf.normalize_data_decrease(data_decrease[15:60 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # line2, = plt.plot(test_increase)
    # plt.plot(np.flipud(test_decrease))    
    
    validation_point = 3
    test_increase = sf.normalize_data_increase(data_increase[17:77 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    test_decrease = sf.normalize_data_decrease(data_decrease[15:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    line3, = plt.plot(test_increase)
    plt.plot(np.flipud(test_decrease))  

    # validation_point = 4
    # test_increase = sf.normalize_data_increase(data_increase[28:83 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # test_decrease = sf.normalize_data_decrease(data_decrease[20:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # line4, = plt.plot(test_increase)
    
    plt.plot(np.flipud(test_decrease))
    validation_point = 5
    test_increase = sf.normalize_data_increase(data_increase[30:100 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    test_decrease = sf.normalize_data_decrease(data_decrease[25:95 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    line5, = plt.plot(test_increase)
    plt.plot(np.flipud(test_decrease))

    # validation_point = 6
    # test_increase = sf.normalize_data_increase(data_increase[39:106 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # test_decrease = sf.normalize_data_decrease(data_decrease[39:106 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # line6, = plt.plot(test_increase)
    # plt.plot(np.flipud(test_decrease))

    validation_point = 7
    test_increase = sf.normalize_data_increase(data_increase[56:156 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    test_decrease = sf.normalize_data_decrease(data_decrease[40:140 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    line7, = plt.plot(test_increase)
    plt.plot(np.flipud(test_decrease))
 
    # validation_point = 8
    # test_increase = sf.normalize_data_increase(data_increase[60:190 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # test_decrease = sf.normalize_data_decrease(data_decrease[50:180 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # line8, = plt.plot(test_increase)
    # plt.plot(np.flipud(test_decrease))
    # plt.plot(test_decrease)
    # # plt.legend([line1, line2, line3, line4, line5, line6, line7, line8], ["Point 1", "Point 2","Point 3","Point 4","Point 5","Point 6", "Point 7", "Point 8"]) 
    plt.legend([line1, line3, line5, line7], ["Point 1","Point 3","Point 5","Point 7"], fontsize=12)  
    plt.xlabel("t [min]", fontsize=12)
    plt.ylabel(r"$WSE_{norm}$", fontsize=12)
    # plt.title(r"Simulated hysteresis-loop for $WSE_{norm}$ in selected validation points", fontsize=14)  
    plt.xlim(0, 115)
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig("hysteresis_loop_simulated_new.png", dpi=400, bbox_inches='tight')

    # Validation
    # plt.figure()
    # validation_point = 1    
    # test_increase = sf.normalize_data_increase(cycle[5:75, validation_point - 1])
    # test_decrease = sf.normalize_data_decrease(cycle[109:179 , validation_point -1])
    # line1, = plt.plot(test_increase, 'd')
    # plt.plot(np.flipud(test_decrease), 'd')

    # validation_point = 3    
    # test_increase = sf.normalize_data_increase(cycle[4:89, validation_point - 1])
    # test_decrease = sf.normalize_data_decrease(cycle[120:205 , validation_point -1])
    # line3, = plt.plot(test_increase, 'o')
    # plt.plot(np.flipud(test_decrease), 'o')  

    # validation_point = 5    
    # test_increase = sf.normalize_data_increase(cycle[:97, validation_point - 1])
    # test_decrease = sf.normalize_data_decrease(cycle[125:222 , validation_point -1])
    # line5, = plt.plot(test_increase, 'x')
    # plt.plot(np.flipud(test_decrease), 'x')    

    # validation_point = 7
    # test_increase = np.ones(112)
    # test_increase[:97] = sf.normalize_data_increase(cycle[:97, validation_point - 1])
    # test_increase[97:115] = test_increase[62:77]
    # test_decrease = sf.normalize_data_decrease(cycle[105:217 , validation_point -1])
    # line7, = plt.plot(test_increase, '+')
    # plt.plot(np.flipud(test_decrease), '+')  
    # plt.legend([line1, line3, line5, line7], ["Point 1","Point 3","Point 5","Point 7"], fontsize=12)  
    # plt.xlabel("t [min]", fontsize=12)
    # plt.ylabel(r"$WSE_{norm}$", fontsize=12)
    # # plt.title(r"Measured hysteresis-loop for $WSE_{norm}$ in selected validation points", fontsize=14)  
    # plt.xlim(0, 115)
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("hysteresis_loop_measured.png", dpi=400, bbox_inches='tight')


#     # Point 1
#     temp = np.linspace(0, 240, 240)
#     temp[:120] = data_increase[0:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     temp[120:] = data_decrease[:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     fig, (ax1, ax2) = plt.subplots(2,1)
#     ax1.plot(data_increase[5:65 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
#     ax1.plot(np.flipud(data_decrease[5:65 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]))
#     ax1.plot(cycle[0:60, 0], 'o')
#     ax1.plot(np.flipud(cycle[108:170, 0]), 'o')
#     ax2.plot(cycle[:, 0], 'o')
#     ax2.plot(temp[10:])
#     # Point 2
#     validation_point = 2
#     temp[:120] = data_increase[0:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     temp[120:] = data_decrease[:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     fig, (ax1, ax2) = plt.subplots(2,1)
#     ax1.plot(data_increase[5:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]], 'd')
#     ax1.plot(np.flipud(data_decrease[5:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]), 'd')
#     ax2.plot(temp)
    
#     # Point 3
#     validation_point = 3
#     temp[:120] = data_increase[0:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     temp[120:] = data_decrease[:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     fig, (ax1, ax2) = plt.subplots(2,1)
#     ax1.plot(data_increase[5:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]], 'd')
#     ax1.plot(np.flipud(data_decrease[5:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]), 'd')
#     ax2.plot(temp)
#     # Point 4
#     validation_point = 4
#     temp[:120] = data_increase[0:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     temp[120:] = data_decrease[:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     fig, (ax1, ax2) = plt.subplots(2,1)
#     ax1.plot(data_increase[5:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]], 'd')
#     ax1.plot(np.flipud(data_decrease[5:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]), 'd')
#     ax2.plot(temp)

#    # Point 5
#     validation_point = 5
#     temp[:120] = data_increase[0:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     temp[120:] = data_decrease[:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     fig, (ax1, ax2) = plt.subplots(2,1)
#     ax1.plot(data_increase[5:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
#     ax1.plot(np.flipud(data_decrease[5:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]))
#     ax1.plot(cycle[0:60, 4], 'o')
#     ax1.plot(np.flipud(cycle[108:170, 4]), 'o')
#     ax2.plot(cycle[:, 4], 'o')
#     ax2.plot(temp)

#    # Point 6
#     validation_point = 6
#     temp[:120] = data_increase[0:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     temp[120:] = data_decrease[:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     fig, (ax1, ax2) = plt.subplots(2,1)
#     ax1.plot(data_increase[5:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]], 'd')
#     ax1.plot(np.flipud(data_decrease[5:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]), 'd')
#     ax2.plot(temp)

#    # Point 7
#     validation_point = 7
#     temp[:120] = data_increase[0:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     temp[120:] = data_decrease[:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     fig, (ax1, ax2) = plt.subplots(2,1)
#     ax1.plot(data_increase[5:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]], 'd')
#     ax1.plot(np.flipud(data_decrease[5:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]), 'd')
#     ax2.plot(temp)

#     # Point 8
#     validation_point = 8
#     temp[:120] = data_increase[0:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     temp[120:] = data_decrease[:120 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]
#     fig, (ax1, ax2) = plt.subplots(2,1)
#     ax1.plot(data_increase[5:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]], 'd')
#     ax1.plot(np.flipud(data_decrease[5:75 ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]]), 'd')
#     ax2.plot(temp)

    # plt.figure()
    # plt.plot(data_increase[0, validation_points['{}'.format(validation_point)][0], :])
    # plt.plot(bathymetry[validation_points['{}'.format(validation_point)][0], :])
    # plt.show()

if __name__ == '__main__':
    main()
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("hysteresis_loop_measured.png", dpi=400)