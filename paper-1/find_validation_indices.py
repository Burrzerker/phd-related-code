import scipy
import numpy as np
import support_functions as sf
from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)
from matplotlib import pyplot as plt

PATH_TO_DATA = "simulation-data"

def main():
    bathymetry = scipy.io.loadmat("initial_bed_level.mat")['data']['Val'][0][0]
    validation_point = 8 # Chosen validation point (1-8)

    simulations_increase = sf.return_simulation_cases(PATH_TO_DATA + "/21-50-data")

    X, Y, temp = sf.read_coordinate_data(simulations_increase[0])
    temp = temp[-1, :, :]
    
    x = np.linspace(np.amin(X), np.amax(X), X.shape[1])
    y = np.linspace(np.amin(Y), np.amax(Y), Y.shape[0])
    
    validation_points = sf.return_validation_point_coordinates()
    validation_points_gps = sf.validation_points_gps()

    # print("X validation index: {}".format(validation_points['{}'.format(validation_point)[0]][0]))
    # print("Y validation index: {}".format(validation_points['{}'.format(validation_point)[0]][1]))

    for index, number in enumerate(y):
        if round(number - validation_points_gps['{}'.format(validation_point)[0]][0]) == 0:
            y_index = index
            print("Y:{}, index: {}".format(y[index], y_index))
    
    for index, number in enumerate(x):
        if abs(round(number - validation_points_gps['{}'.format(validation_point)[0]][1])) < 10: 
            x_index = index
            print("x:{}, index: {}".format(x[index], x_index))

    print(validation_points['{}'.format(validation_point)][1], validation_points['{}'.format(validation_point)][0])
    print("X validation gps coordinate: {}".format(validation_points_gps['{}'.format(validation_point)[0]][1]))
    print("Y validation gps coordinate: {}".format(validation_points_gps['{}'.format(validation_point)[0]][0]))
    # print(x[validation_points['{}'.format(validation_point)][0]], y[validation_points['{}'.format(validation_point)][1]])
    print(validation_points_gps['{}'.format(validation_point)][1], validation_points_gps['{}'.format(validation_point)][0])
    
    fig, ax = plt.subplots(1,1)
    ax.contourf(temp)
    ax.set_aspect('equal')
    for point in range(1,9):
        # print(point)
        ax.scatter(validation_points['{}'.format(point)][1], validation_points['{}'.format(point)][0], color='red')
    # ax.scatter(x_index, 2000-y_index, color='orange')
    # plt.xlim(left=0, right=220)
    plt.grid(True)
    fig.gca().invert_yaxis()
    
    fig, ax = plt.subplots(1,1)
    ax.contourf(X, Y, temp)
    # ax.set_aspect('equal')
    # ax.scatter(x[validation_points['{}'.format(validation_point)][1]], y[validation_points['{}'.format(validation_point)][0]])
    for point in range(1,9):
        # print(point)
        ax.scatter(validation_points_gps['{}'.format(point)][1], validation_points_gps['{}'.format(point)][0], color='red')

    # ax.scatter(x_index, 2000-y_index, color='orange')
    # plt.scatter(validation_points_gps['{}'.format(validation_point)][1], validation_points_gps['{}'.format(validation_point)][0], color="red")
    # plt.scatter(x[validation_points['{}'.format(validation_point)][0]], y[validation_points['{}'.format(validation_point)][1]], color="orange")

if __name__ == '__main__':
    main()
    plt.show()