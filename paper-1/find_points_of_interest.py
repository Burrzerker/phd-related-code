
import support_functions as sf
import numpy as np
import scipy
from matplotlib import pyplot as plt
from matplotlib import rc
import matplotlib as mpl
from animation import read_background_image
PATH_TO_DATA = "simulation-data"
BACKGROUND_IMAGE = "test.tif"

# validation_points = { # temp
#     '1': (330, 40), 
#     #'2': (452, 50), # Good but using south river bank
#     '2': (457, 100),
#     '3': (487, 140), # Sort of good, not using the same calibration as in panama paper
#     # '3': (525, 140),
#     '4': (666, 175),
#     '5': (780, 180),
#     '6': (995, 70),
#     '7': (1300, 165),
#     '8': (1853, 177),
# }
def main():
    bathymetry = scipy.io.loadmat("initial_bed_level.mat")['data']['Val'][0][0]

    simulations_decrease = sf.return_simulation_cases(PATH_TO_DATA + "/start-stop")
    print(simulations_decrease)
    background = read_background_image(BACKGROUND_IMAGE)
    X, Y, _ = sf.read_coordinate_data(simulations_decrease[0])
    scenario_24 = sf.read_simulation_data(simulations_decrease[0])
    scenario_29 = sf.read_simulation_data(simulations_decrease[1])
    scenario_36 = sf.read_simulation_data(simulations_decrease[2])      
    scenario_48 = sf.read_simulation_data(simulations_decrease[3])
    scenario_72 = sf.read_simulation_data(simulations_decrease[4])
    # scenario_144 = sf.read_simulation_data(simulations_decrease[5])
    validation_points = sf.return_validation_point_coordinates()

    point_60 = 135
    point_50 = 150
    point_40 = 666
    point_30 = 980
    point_20 = 1680
    plt.figure()
    asd = 800
    plt.plot(scenario_24[:, asd, 100])

    plt.figure()
    plt.plot(bathymetry[asd, :])
    plt.plot(scenario_24[-1, asd, :])

    temp_1 = np.empty(shape=(2077, 217))
    temp_1[:, :] = np.nan

    temp_1[0, :] = 1
    temp_1[1, :] = 1
    temp_1[2, :] = 1
    temp_1[3, :] = 1
    temp_1[4, :] = 1

    # temp_1[point_60, :] = 1
    # temp_1[point_60 + 1, :] = 1
    # temp_1[point_60 + 2, :] = 1
    # temp_1[point_60 + 3, :] = 1
    # temp_1[point_60 + 4, :] = 1

    # temp_1[point_50, :] = 3
    # temp_1[point_50 + 1, :] = 3
    # temp_1[point_50 + 2, :] = 3
    # temp_1[point_50 + 3, :] = 3
    # temp_1[point_50 + 4, :] = 3

    # temp_1[point_40, :] = 4
    # temp_1[point_40 + 1, :] = 4
    # temp_1[point_40 + 2, :] = 4
    # temp_1[point_40 + 3, :] = 4
    # temp_1[point_40 + 4, :] = 4

    # temp_1[point_30, :] = 5
    # temp_1[point_30 + 1, :] = 5
    # temp_1[point_30 + 2, :] = 5
    # temp_1[point_30 + 3, :] = 5
    # temp_1[point_30 + 4, :] = 5

    # temp_1[point_20, :] = 6
    # temp_1[point_20 + 1, :] = 6
    # temp_1[point_20 + 2, :] = 6
    # temp_1[point_20 + 3, :] = 6
    # temp_1[point_20 + 4, :] = 6
    # temp_1[point_20 + 5, :] = 6
    # temp_1[point_20 + 6, :] = 6
    # temp_1[point_20 + 7, :] = 6
    # temp_1[point_20 + 8, :] = 6
    # temp_1[136, :] = 1
    

    fig, ax = plt.subplots(1,1)
    plt.contourf(X, Y, temp_1, cmap="autumn")
    ax.set_xticks([], [])
    ax.set_yticks([], [])
    plt.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])

if __name__ == '__main__':
    main()
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("perpetual_dynamics.png", dpi=400, bbox_inches='tight')
    plt.show()