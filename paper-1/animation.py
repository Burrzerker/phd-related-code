"""
Script for generating animations and contours for Stornorrfors dry reach
"""

import support_functions as sf
import scipy
import cv2
import numpy as np
import matplotlib.image as mpimg
from matplotlib import pyplot as plt
import matplotlib.animation as animation
from osgeo import gdal

PATH_TO_DATA = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/results/50-21-48-start-stop"
BACKGROUND_IMAGE = "test.tif"

def main():
    
    # simulations_21_250 = sf.return_simulation_cases(PATH_TO_DATA + "/250-21-data")
    # simulations_250_21 = sf.return_simulation_cases(PATH_TO_DATA + "/250-21-data")
    X, Y, _ = sf.read_coordinate_data("simulation-data/start-stop/24", 'water_level')
    background = read_background_image(BACKGROUND_IMAGE)

    
    dict_21_250 = {}
    dict_250_21 = {}

    dict_new = {}
    dict_new[0] = sf.read_simulation_data("simulation-data/start-stop/24", 'depth_averaged_velocity')
    dict_new[1] = sf.read_simulation_data("simulation-data/start-stop/29", 'depth_averaged_velocity')
    dict_new[2] = sf.read_simulation_data("simulation-data/start-stop/36", 'depth_averaged_velocity')
    dict_new[3] = sf.read_simulation_data("simulation-data/start-stop/48", 'depth_averaged_velocity')
    # for index, case in enumerate(simulations_21_250):
    #     data = sf.read_simulation_data(case, 'depth_averaged_velocity')
    #     dict_250_21[index] = data
    # data = sf.read_simulation_data(PATH_TO_DATA, 'depth_averaged_velocity')

    # for index, case in enumerate(simulations_250_21):
    #     data = sf.read_simulation_data(case, 'depth_averaged_velocity')
    #     dict_250_21[index] = data
    # case = 0
    # animate_stornorrfors(X, Y, dict_21_250, background)
    # plot_contour(dict_21_250, X, Y, background)
    save_png_contour_files(dict_new, X, Y, background)
    # single_plot_animation(data, X, Y, background)


def single_plot_animation(data, X, Y, background):
    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams.update({'font.size': 12})
    nr_of_timesteps = len(data[:, 0, 0])
    # timesteps = np.linspace(0, nr_of_timesteps-1, nr_of_timesteps)
    timesteps = range(0, nr_of_timesteps-1)
    for timestep in timesteps:
        fig, ax1 = plt.subplots(1,1, figsize=(15,13))
        fig.suptitle(r"50 $\rightarrow$ 21 m$^3$/s every 48th minute", x=0.4)
        ax1.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
        c = ax1.contourf(X, Y, data[timestep, :, :], levels=np.linspace(0.005, 5, 100))
        ax1.set_title("Depth averaged velocity at timestep: {} minutes".format(timestep))
        ax1.set_xticks([], [])
        ax1.set_yticks([], [])
        fig.tight_layout()
        fig.subplots_adjust(top=0.95)
        for ax in fig.get_axes():
            ax.label_outer()
        fig.colorbar(c, orientation='vertical', aspect=50).set_label(label=r"Depth Averaged Velocity m/s", weight='normal')
        plt.savefig("/home/anton/phd-related-code/paper-1/50-21-48-start-stop-png/timestep-{}.png".format(timestep), dpi=400, bbox_inches='tight')
        plt.close(fig)
        print("Completion: {}%".format(100 * timestep / nr_of_timesteps)) 

        
def save_png_contour_files(data, X, Y, background):
    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams.update({'font.size': 12})
    nr_of_timesteps = len(data[0][:, 0, 0])
    # timesteps = np.linspace(0, nr_of_timesteps-1, nr_of_timesteps)
    timesteps = range(0, nr_of_timesteps-1)
    for timestep in timesteps:
        fig, axes = plt.subplots(2,2, figsize=(15,13))
        ((ax1, ax2), (ax3, ax4)) = axes
        fig.suptitle(r"Depth averaged velocity at time: {} minutes".format(timestep+1), x=0.4)
        ax1.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
        c = ax1.contourf(X, Y, data[0][timestep, :, :], levels=np.linspace(0.005, 5, 100))
        ax1.set_title("60 flow changes per day")
        ax1.set_xticks([], [])
        ax1.set_yticks([], [])
        
        ax2.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
        ax2.contourf(X, Y, data[1][timestep, :, :], levels=np.linspace(0.005, 5, 100))
        ax2.set_title("50 flow changes per day")
        ax2.set_xticks([], [])
        ax2.set_yticks([], [])
        
        ax3.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
        ax3.contourf(X, Y, data[2][timestep, :, :], levels=np.linspace(0.005, 5, 100))
        ax3.set_title("40 flow changes per day")
        ax3.set_xticks([], [])
        ax3.set_yticks([], [])

        ax4.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
        ax4.contourf(X, Y, data[3][timestep, :, :], levels=np.linspace(0.005, 5, 100))
        ax4.set_title("30 flow changes per day")
        ax4.set_xticks([], [])
        ax4.set_yticks([], [])
        fig.tight_layout()
        fig.subplots_adjust(top=0.95)
        for ax in fig.get_axes():
            ax.label_outer()
        fig.colorbar(c, ax=axes.ravel().tolist(), orientation='vertical', aspect=50).set_label(label=r"WSE [MASL]", weight='normal')
        plt.savefig("/home/anton/phd-related-code/paper-1/start-stop-animation-velocity-png/timestep-{}.png".format(timestep), dpi=400, bbox_inches='tight')
        plt.close(fig)
        print("Completion: {}%".format(100 * timestep / nr_of_timesteps)) 



def plot_contour(data, X, Y, background):
    print("West: {}, East: {}, South: {}, North: {}".format(np.min(X), np.max(X), np.min(Y), np.max(Y)))
    timestep = 0
    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams.update({'font.size': 12})
    
    fig, axes = plt.subplots(2,2, figsize=(15,13))
    ((ax1, ax2), (ax3, ax4)) = axes
    fig.suptitle("Depth averaged velocity at time: {}".format(timestep), x=0.4, )
    ax1.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    c = ax1.contourf(X, Y, data[0][timestep, :, :], levels=np.linspace(0.005, 5, 100))
    ax1.set_title("Closing time: {} minute".format(1))
    ax1.set_xticks([], [])
    ax1.set_yticks([], [])
    
    ax2.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    ax2.contourf(X, Y, data[1][timestep, :, :], levels=np.linspace(0.005, 5, 100))
    ax2.set_title("Closing time: {} minutes".format(5))
    ax2.set_xticks([], [])
    ax2.set_yticks([], [])
    
    ax3.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    ax3.contourf(X, Y, data[3][timestep, :, :], levels=np.linspace(0.005, 5, 100))
    ax3.set_title("Closing time: {} minutes".format(30))
    ax3.set_xticks([], [])
    ax3.set_yticks([], [])

    ax4.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    ax4.contourf(X, Y, data[4][timestep, :, :], levels=np.linspace(0.005, 5, 100))
    ax4.set_title("Closing time: {} minutes".format(45))
    ax4.set_xticks([], [])
    ax4.set_yticks([], [])
    fig.tight_layout()
    fig.subplots_adjust(top=0.95)
    for ax in fig.get_axes():
        ax.label_outer()
    fig.colorbar(c, ax=axes.ravel().tolist(), orientation='vertical', aspect=50).set_label(label=r"Depth Averaged Velocity m/s", weight='normal')

def read_background_image(path):
    """
    Reads .tif background map for contour plots
    :param path: path to .tif file
    :returns: np.ndarray that contains image data
    """
    background = gdal.Open(BACKGROUND_IMAGE)
    background_array = background.ReadAsArray()
    shape = background_array.shape
    
    flipped_channels = np.ndarray((shape[1], shape[2], shape[0]))
    flipped_channels[:, :, 0] = background_array[0, :, :]
    flipped_channels[:, :, 1] = background_array[1, :, :]
    flipped_channels[:, :, 2] = background_array[2, :, :]
    flipped_channels[:, :, 3] = background_array[3, :, :]
    return flipped_channels.astype(np.uint8)


if __name__ == "__main__":
    main()
    plt.show()