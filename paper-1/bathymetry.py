import scipy
import numpy as np
import support_functions as sf
from matplotlib import pyplot as plt
from matplotlib import rc
from animation import read_background_image

BACKGROUND_IMAGE = "background.tif"

def main():
    rc('text', usetex=True)
    plt.rcParams["font.family"] = "Times New Roman"
    bathymetry = scipy.io.loadmat("initial_bed_level.mat")['data']['Val'][0][0]
    X, Y, temp = sf.read_coordinate_data("simulation-data/50-21-data/50-21-1")
    background = read_background_image(BACKGROUND_IMAGE)


    fig, ax = plt.subplots(1,1)
    c = plt.contourf(X, Y, bathymetry)
    # plt.suptitle("Manning number distribution", fontsize=10, y=0.93)
    # plt.colorbar(c).set_label("Meters above sea level")
    fig.colorbar(c, ax=ax,orientation='vertical', shrink=1, aspect=25, pad=0.05).set_label(label=r'Metres Above Sea Level $[MASL]$',size=10,weight='normal')
    ax.set_xticks([], [])
    ax.set_yticks([], [])
    plt.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])


if __name__ == '__main__':
    main()
    # plt.tight_layout()
    # plt.show()
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig("bathymetry.png", dpi=600, bbox_inches='tight') # 
