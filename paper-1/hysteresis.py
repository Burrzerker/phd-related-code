"""
Script for plotting the hysteresis in Stornorrfors
"""
import support_functions as sf
import scipy
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc

PATH_TO_DATA = "simulation-data"
PATH_TO_VALIDATION_DATA = "Diverdata.mat"
PLOT_WATER_LEVEL = False

def main():
    rc('text', usetex=True)
    bathymetry = scipy.io.loadmat("initial_bed_level.mat")['data']['Val'][0][0]
    validation_point = 2 # Chosen validation point (1-8)

    
    simulations_increase = sf.return_simulation_cases(PATH_TO_DATA + "/21-50-data")
    simulations_decrease = sf.return_simulation_cases(PATH_TO_DATA + "/50-21-data")
    X, Y, temp = sf.read_coordinate_data(simulations_increase[0])
    x = np.linspace(np.amin(X), np.amax(X), X.shape[0])
    y = np.linspace(np.amin(Y), np.amax(Y), Y.shape[1])


    validation_points = sf.return_validation_point_coordinates()

    validation_data = sf.return_validation_data(PATH_TO_VALIDATION_DATA)
    gps_validation = sf.validation_points_gps()

    # fig = plt.figure()
    # plt.contourf(X,Y, temp[0, :, :])
    # plt.scatter(gps_validation['{}'.format(validation_point)][1], gps_validation['{}'.format(validation_point)][0], color='red')
    # plt.scatter(gps_validation['2'][1], gps_validation['2'][0], color='red')

    # fig = plt.figure()
    # plt.plot((bathymetry[validation_points['{}'.format(validation_point)][0] +2 ,:] - bathymetry[validation_points['{}'.format(validation_point)][0] ,:])/8)
    # plt.plot(bathymetry[validation_points['{}'.format(validation_point)][0] ,:])
    # plt.figure()
    # plt.plot(validation_data[55981:55981+241,0])
    # plt.plot(validation_data[:,3])

    dict_increase = {}
    dict_decrease = {}
    for index, case in enumerate(simulations_increase):
        data = sf.read_simulation_data(case, 'water_level')
        dict_increase[index] = data 

    for index, case in enumerate(simulations_decrease):
        data = sf.read_simulation_data(case, 'water_level')
        dict_decrease[index] = data

    

    # index = 42730
    # index = 36955
    # index = 48480
    # index = 54272
    # index = 59995
    index = 65765
    
    validation_index = validation_point - 1
    validation_data = validation_data[index:index+241, :]
    # print(validation_data.shape)

    # fig, axes = plt.subplots(3,2)
    # ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = axes
    # ax1.set_title("1 minute closing/opening time")
    # ax1.plot(dict_increase[0][: ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # ax1.plot(dict_decrease[0][: ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # ax1.plot(validation_data[index:index+241, validation_index])

    # ax2.set_title("5 minutes closing/opening time")
    # ax2.plot(dict_increase[1][: ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # ax2.plot(dict_decrease[1][: ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # ax2.plot(validation_data[index:index+241, validation_index])
    
    # ax3.set_title("15 minutes closing/opening time")
    # ax3.plot(dict_increase[2][: ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # ax3.plot(dict_decrease[2][: ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # ax3.plot(validation_data[index:index+241, validation_index])

    # ax4.set_title("30 minutes closing/opening time")
    # ax4.plot(dict_increase[3][: ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # ax4.plot(dict_decrease[3][: ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # ax4.plot(validation_data[index:index+241, validation_index])

    # ax5.set_title("45 minutes closing/opening time")
    # ax5.plot(dict_increase[4][: ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # ax5.plot(dict_decrease[4][: ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # ax5.plot(validation_data[index:index+241, validation_index])

    # ax6.set_title("60 minutes closing/opening time")
    # ax6.plot(dict_increase[5][: ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # ax6.plot(dict_decrease[5][: ,validation_points['{}'.format(validation_point)][0], validation_points['{}'.format(validation_point)][1]])
    # ax6.plot(validation_data[index:index+241, validation_index])

    # fig, axes = plt.subplots(4,2)
    # ((ax1, ax2), (ax3, ax4), (ax5, ax6), (ax7, ax8)) = axes
    # ax1.plot(dict_increase[1][:, validation_points['1'][0], validation_points['1'][1]])
    # ax1.plot(dict_decrease[1][:, validation_points['1'][0], validation_points['1'][1]])
    # ax1.plot(validation_data[:, 0])
    # ax1.set_title('Validation point 1')
    
    # ax2.plot(dict_increase[1][:, validation_points['2'][0], validation_points['2'][1]])
    # ax2.plot(dict_decrease[1][:, validation_points['2'][0], validation_points['2'][1]])
    # ax2.plot(validation_data[:, 1])
    # ax2.set_title('Validation point 2')
    
    # ax3.plot(dict_increase[1][:, validation_points['3'][0], validation_points['3'][1]])
    # ax3.plot(dict_decrease[1][:, validation_points['3'][0], validation_points['3'][1]])
    # ax3.plot(validation_data[:, 2])
    # ax3.set_title('Validation point 3')

    # ax4.plot(dict_increase[1][:, validation_points['4'][0], validation_points['4'][1]])
    # ax4.plot(dict_decrease[1][:, validation_points['4'][0], validation_points['4'][1]])
    # ax4.plot(validation_data[:, 3])
    # ax4.set_title('Validation point 4')

    # ax5.plot(dict_increase[1][:, validation_points['5'][0], validation_points['5'][1]])
    # ax5.plot(dict_decrease[1][:, validation_points['5'][0], validation_points['5'][1]])
    # ax5.plot(validation_data[:, 4])
    # ax5.set_title('Validation point 5')

    # ax6.plot(dict_increase[1][:, validation_points['6'][0], validation_points['6'][1]])
    # ax6.plot(dict_decrease[1][:, validation_points['6'][0], validation_points['6'][1]])
    # ax6.plot(validation_data[:, 5])
    # ax6.set_title('Validation point 6')
    
    # ax7.plot(dict_increase[1][:, validation_points['7'][0], validation_points['7'][1]])
    # ax7.plot(dict_decrease[1][:, validation_points['7'][0], validation_points['7'][1]])
    # ax7.plot(validation_data[:, 6])
    # ax7.set_title('Validation point 7')

    # ax8.plot(dict_increase[1][:, validation_points['8'][0], validation_points['8'][1]])
    # ax8.plot(dict_decrease[1][:, validation_points['8'][0], validation_points['8'][1]])
    # ax8.plot(validation_data[:, 7])
    # ax8.set_title('Validation point 8')

    # fig = plt.figure()
    # plt.plot(dict_increase[5][-1 ,validation_points['{}'.format(validation_point)][0], :])
    # plt.plot(bathymetry[validation_points['{}'.format(validation_point)][0] ,:])
    simulation_50 = []
    validation_50 = []
    simulation_21 = []
    validation_21 = []
    for case in range(1,9):        
        
        simulation_50.append(dict_increase[1][-1, validation_points['{}'.format(case)][0], validation_points['{}'.format(case)][1]])
        validation_50.append(validation_data[-1, case - 1])

        simulation_21.append(dict_increase[1][0, validation_points['{}'.format(case)][0], validation_points['{}'.format(case)][1]])
        validation_21.append(validation_data[0, case - 1])
    # plt.figure()
    # plt.plot(simulation_50, validation_50, 'kd')
    # plt.plot(validation_50, validation_50, 'k')
    # plt.xlabel(r'Validation data $WSE$ $[MASL]$', fontsize=12)
    # plt.ylabel(r'Calibrated simulation data $WSE$ $[MASL]$', fontsize=12)
    # plt.title('Correlation plot between validation and simulation data', fontsize=14)
    # plt.legend(['Simulation', 'Validation'], fontsize=12)
    # # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("correlation.png", dpi=400)
    simulation_50 = np.asarray(simulation_50)
    validation_50 = np.asarray(validation_50)
    print("Standard deviation: {}".format(abs(np.std(simulation_50 - validation_50))))
    print("Max: {}".format(np.max(abs(simulation_50 - validation_50))))
    print("Min: {}".format(np.min(abs(simulation_50 - validation_50))))
    print("Mean: {}".format(np.mean(abs(simulation_50 - validation_50))))
    print("Median: {}".format(np.median(abs(simulation_50 - validation_50))))
    print("Correlation: {}".format(np.corrcoef(simulation_50, validation_50)))
    # plt.figure()
    # plt.plot(simulation_21, validation_21, 'rx')
    # plt.plot(validation_21, validation_21)
    

def plot_water_level(water_level_rise, water_level_fall, validation_points):
    # water_level = water_level[~np.isnan(water_level)]
    # water_level_fall = np.flipud(water_level_fall)
    fall_start, fall_stop = sf.hysteresis_points_of_interest(water_level_fall)
    rise_start, rise_stop = sf.hysteresis_points_of_interest(water_level_rise)
    print("Fall start: {}, Fall stop: {}".format(fall_start, fall_stop))
    print("Rise start: {}, Rise stop: {}".format(rise_start, rise_stop))
    print("Time fall: {}, Time rise: {}".format(fall_stop - fall_start, rise_stop - rise_start))
    plt.figure()
    line1, = plt.plot(water_level_rise, 'bx')
    line2, = plt.plot(water_level_fall, 'r+')
    plt.axvline(x=fall_start, color='r')
    plt.axvline(x=fall_stop, color='r')
    plt.axvline(x=rise_start, color='b')
    plt.axvline(x=rise_stop, color='b')
    plt.legend([line1, line2], ['Increase', 'Decrease'])

if __name__ == '__main__':
    main()
    plt.show()

