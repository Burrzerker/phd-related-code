
import numpy as np
import matplotlib.pyplot as plt


def main():
    data = np.genfromtxt("test.csv", delimiter=',')
    data = data[:,0]
    data = data[220:320]
    
    mean = np.mean(data)
    
    max_fluctuation = np.max(abs(data - mean))
    min_fluctuation = np.min(abs(data - mean))
    st_dev = np.std(data-mean)

    print("Mean: {}".format(mean))
    print("Max fluctuation: {}".format(max_fluctuation))
    print("Min fluctuation: {}".format(min_fluctuation))
    print("Standard deviation: {}".format(st_dev))

    plt.figure()
    plt.plot(data)
    plt.show()

if __name__ == '__main__':
    main()