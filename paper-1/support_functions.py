import os
import collections
import re
import shutil
import cv2 as cv
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.io import loadmat as loadmat

def normalize_data_decrease(data):
    return (data - data[-1]) / (data[0] - data[-1]) 

def normalize_data_increase(data):
    print(data[0])
    print(data[-1])
    return (data - data[0]) / (data[-1] - data[0]) 

def generate_png_for_image_processing(data, X, Y):

    data[data < 10**-10] = np.nan # For 50-21 case
    data[~np.isnan(data)] = 1

    plt.contourf(X, Y, data)

    plt.hlines(np.amax(Y), np.amin(X), np.amax(X))
    plt.hlines(np.amin(Y), np.amin(X), np.amax(X))
    plt.vlines(np.amax(X), np.amin(Y), np.amax(Y))
    plt.vlines(np.amin(X), np.amin(Y), np.amax(Y))
    plt.axis('off')
    # plt.show()
    plt.savefig("/home/anton/phd-related-code/paper-1/temp/temp.png", bbox_inches='tight', dpi=400)
    plt.close()

def compute_area(data, X, Y):
    area_of_square = (np.amax(Y) - np.amin(Y)) * ((np.amax(X) - np.amin(X)))
    try:
        shutil.rmtree("temp")
    except:
        pass
    os.mkdir("temp")
    generate_png_for_image_processing(data, X, Y)
    img = cv.imread("temp/temp.png", 0)
    img = img[45:-45, 45:-45]
    total_number_of_pixels = img.shape[0] * img.shape[1]
    number_of_active_pixels = 0
    for row in img:
        for number in row:
            if number != 255:
                number_of_active_pixels += 1 

    return area_of_square * number_of_active_pixels / total_number_of_pixels

def return_simulation_cases(path):
    """
    Returns the path to folders containing .mat file exported from Delft3D
    :param path: path to folder containing subfolders with data
    :return: list contianing the path to the simulations in numerical order 
    """
    list_of_simulations = []
    for dir in os.listdir(path):
        list_of_simulations.append("{}/{}".format(path, dir))
    list_of_simulations.sort(key=natural_keys)
    return list_of_simulations

def read_coordinate_data(simulation, variable="water_level"):
    """
    Returns the X and Y matrices from Delft
    :param Simulation: Path to a .mat file exported from quickplot in Delft3D
    :returns: X and Y matrices
    """
    data = loadmat("{}/{}".format(simulation, variable))
    return data['data']['X'][0][0][0:-1, 0:-1], data['data']['Y'][0][0][0:-1, 0:-1], data['data']['Val'][0][0]

def read_simulation_data(simulation, variable="water_level"):
    """
    Return simulation data, water_level is default other options are
    depth_averaged_velocity and water_depth
    :params simulation:  
    """
    data = loadmat("{}/{}.mat".format(simulation, variable))
    return data['data']['Val'][0][0]

def hysteresis_points_of_interest(variable):
    """
    Find the start of change and end of change for some variable
    :param variable: variable under change, i.e. water level
    :return: indices where start and end in change happens
    """
    threshold = 0.001
    for index, value in enumerate(variable):
        try:
            if abs(variable[index+1] - value) > threshold:
                index_start = index
                break
        except IndexError:
            pass
    for index, value in enumerate(variable):    
        try:
            if abs(value - variable[index-1]) < threshold and index > index_start:
                index_end = index
                break
        except IndexError:
            pass
    return index_start, index_end

def validation_points_cycle(data):
    """
    Crops the validation data so that a increase-decrease cycle is achieved
    :param data: numpy ndarray containing validation data
    :returns data: numpy ndarray with one cycle  
    """

    point_1 = np.ndarray((243))
    point_2 = np.ndarray((243))
    point_3 = np.ndarray((243))
    point_4 = np.ndarray((243))
    point_5 = np.ndarray((243))
    point_6 = np.ndarray((243))
    point_7 = np.ndarray((243))
    point_8 = np.ndarray((243))

    point_1[:100] = data[65775:65775 + 100, 0]
    point_1[100:] = data[67800:67800 + 143, 0]
    point_2[:100] = data[65782:65782 + 100, 1]
    point_2[100:] = data[67800:67800 + 143, 1]
    point_3[:100] = data[65785:65785 + 100, 2]
    point_3[100:] = data[67800:67800 + 143, 2]
    point_4[:100] = data[65793:65793 + 100, 3]
    point_4[100:] = data[67800:67800 + 143, 3]
    point_5[:100] = data[65800:65800 + 100, 4]
    point_5[100:] = data[67800:67800 + 143, 4]
    point_6[:120] = data[65810:65810 + 120, 5]
    point_6[120:] = data[67820:67820 + 123, 5]
    point_7[:100] = data[65827:65827 + 100, 6]
    point_7[100:] = data[67830:67830 + 143 , 6]
    point_8[:100] = data[65845:65845 + 100, 7]
    point_8[100:] = data[67860:67860 + 143, 7]
    # print(data[:67800:67800+193, 0])
    points = np.ndarray((243,8))
    points[:, 0] = point_1
    points[:, 1] = point_2
    points[:, 2] = point_3
    points[:, 3] = point_4
    points[:, 4] = point_5
    points[:, 5] = point_6
    points[:, 6] = point_7
    points[:, 7] = point_8
    return points

def return_validation_data(path):
    """
    Returns time series validation data from .mat file
    Removes the points outside of the domain
    :param path: path to .mat file
    :returns: matrix containing time series in each point
    """
    data = loadmat(path)
    data = data['Diverdata']
    diver_data = np.ndarray(shape=(72000, 8))
    diver_data[:,0] = data[:,3] + 34.2
    diver_data[:,1] = data[:,4] + 28.077
    diver_data[:,2] = data[:,5] + 27.575
    diver_data[:,3] = data[:,6] + 25.846
    diver_data[:,4] = data[:,7] + 22.886
    diver_data[:,5] = data[:,8] + 17.82
    diver_data[:,6] = data[:,9] + 14.816
    diver_data[:,7] = data[:,10] + 12.429
    return diver_data 


def return_validation_point_coordinates():
    """
    Returns the mesh coordinates for validation points
    :return: dict containing validation points
    """
    # validation_points = { # From CSV file validation_points.csv
    #     '1': (340, 31),
    #     '2': (455, 151),
    #     '3': (526, 196),
    #     '4': (674, 212),
    #     '5': (781, 1186),
    #     '6': (996, 96),
    #     '7': (1317, 181),
    #     '8': (1853, 177),
    # }
    validation_points = { # Modified more likely validation points
        '1': (335, 40),
        '2': (455, 151),
        '3': (475, 100),
        '4': (666, 175),
        '5': (780, 180),
        '6': (995, 70),
        '7': (1317, 181),
        '8': (1853, 177),
    }
    # validation_points = { # temp
    #     '1': (330, 40), 
    #     #'2': (452, 50), # Good but using south river bank
    #     '2': (457, 100),
    #     '3': (487, 140), # Sort of good, not using the same calibration as in panama paper
    #     # '3': (525, 140),
    #     '4': (666, 175),
    #     '5': (780, 180),
    #     '6': (995, 70),
    #     '7': (1300, 165),
    #     '8': (1853, 177),
    # }
    return validation_points

def validation_points_gps():
    validation_points = { 
        '1': (7093311.787, 747666.108),
        '2': (7092890.281, 747953.528),
        '3': (7092671.091, 748178.573),
        '4': (7092180.106, 748621.261),
        '5': (7091807.547, 748917.575),
        '6': (7091271.659, 749690.696),
        '7': (7090123.920, 750487.561),
        '8': (7089637.984, 751094.390),
    }
    return validation_points
    
def natural_keys(text):
    """
    sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    """
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

def atoi(text):
    return int(text) if text.isdigit() else text