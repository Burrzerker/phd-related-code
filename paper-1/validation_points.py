import support_functions as sf
from matplotlib import pyplot as plt
from matplotlib import rc
import tikzplotlib

PATH_TO_VALIDATION_DATA = "Diverdata.mat"
def main():
    rc('text', usetex=True)
    data = sf.return_validation_data(PATH_TO_VALIDATION_DATA)
    cycle = sf.validation_points_cycle(data)

    fig, axes = plt.subplots(4,2, sharex=True)
    ((ax1, ax2), (ax3, ax4), (ax5, ax6), (ax7, ax8)) = axes
    # fig.suptitle("Typical increase-decrease cycle", fontsize=12) # (21 $m^3/s \\rightarrow$ 50 $m^3/s \\rightarrow$ 21 $m^3/s$)
    size_title = 10
    size_ylabel = 8
    size_marker = 2
    size_tick_label = 8
    ax1.plot(cycle[:, 0], 'kx', markersize=size_marker)
    ax1.set_title('Validation point 1', fontsize=size_title)
    ax1.tick_params(axis='both', which='major', labelsize=size_tick_label)
    ax1.set_ylabel("WSE", fontsize=size_ylabel)
    
    ax2.plot(cycle[:, 1], 'kx', markersize=size_marker)
    ax2.set_title('Validation point 2', fontsize=size_title)
    ax2.tick_params(axis='both', which='major', labelsize=size_tick_label)
    ax2.set_ylabel("WSE", fontsize=size_ylabel)

    ax3.plot(cycle[:, 2], 'kx', markersize=size_marker)
    ax3.set_title('Validation point 3', fontsize=size_title)
    ax3.tick_params(axis='both', which='major', labelsize=size_tick_label)
    ax3.set_ylabel("WSE", fontsize=size_ylabel)

    ax4.plot(cycle[:, 3], 'kx', markersize=size_marker)
    ax4.set_title('Validation point 4', fontsize=size_title)
    ax4.tick_params(axis='both', which='major', labelsize=size_tick_label)
    ax4.set_ylabel("WSE", fontsize=size_ylabel)
    
    ax5.plot(cycle[:, 4], 'kx', markersize=size_marker)
    ax5.set_title('Validation point 5', fontsize=size_title)
    ax5.tick_params(axis='both', which='major', labelsize=size_tick_label)
    ax5.set_ylabel("WSE", fontsize=size_ylabel)

    ax6.plot(cycle[:, 5], 'kx', markersize=size_marker)
    ax6.set_title('Validation point 6', fontsize=size_title)
    ax6.tick_params(axis='both', which='major', labelsize=size_tick_label)
    ax6.set_ylabel("WSE", fontsize=size_ylabel)
    
    ax7.plot(cycle[:, 6], 'kx', markersize=size_marker)
    ax7.set_title('Validation point 7', fontsize=size_title)
    ax7.tick_params(axis='both', which='major', labelsize=size_tick_label)
    ax7.set_ylabel("WSE", fontsize=size_ylabel)
    ax7.set_xlabel("t [min]")

    ax8.plot(cycle[:, 7], 'kx', markersize=size_marker)
    ax8.set_title('Validation point 8', fontsize=size_title)
    ax8.tick_params(axis='both', which='major', labelsize=size_tick_label)
    ax8.set_ylabel("WSE", fontsize=size_ylabel)
    ax8.set_xlabel("t [min]")

if __name__ == '__main__':
    main()
    # tikzplotlib.save("test.tex")
    plt.tight_layout(rect=[0, 0.03, 1, 0.95])
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig("test.png", dpi=400, bbox_inches='tight')
    # plt.show()