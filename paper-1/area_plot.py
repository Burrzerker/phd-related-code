import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc
import matplotlib as mpl
from scipy import integrate

def main():
    rc('text', usetex=True)
    area_matrix = np.genfromtxt('area.csv', delimiter=',')
    time = np.linspace(0, len(area_matrix[:,0])-1, len(area_matrix[:,0]))
    mpl.rcParams['axes.prop_cycle'] = mpl.cycler(color=["4477AA", "66CCEE", "227733", "CCBB44", "EE6677", "AA3377"]) 

    integral_60 = integrate.trapz(area_matrix[:, 0], time)
    integral_50 = integrate.trapz(area_matrix[:, 1], time)
    integral_40 = integrate.trapz(area_matrix[:, 2], time)
    integral_30 = integrate.trapz(area_matrix[:, 3], time)
    integral_20 = integrate.trapz(area_matrix[:, 4], time)
    integral_10 = integrate.trapz(area_matrix[:, 5], time)
    
    print("Integral 10: {}".format(integral_10 / 10**6))
    print("Integral 20: {}".format(integral_20 / 10**6))
    print("Integral 30: {}".format(integral_30 / 10**6))
    print("Integral 40: {}".format(integral_40 / 10**6))
    print("Integral 50: {}".format(integral_50 / 10**6))
    print("Integral 60: {}".format(integral_60 / 10**6))

    plt.figure()
    plt.plot(time, area_matrix[:, 0] / 10**6) 
    plt.plot(time, area_matrix[:, 1] / 10**6)
    plt.plot(time, area_matrix[:, 2] / 10**6)
    plt.plot(time, area_matrix[:, 3] / 10**6) 
    plt.plot(time, area_matrix[:, 4] / 10**6)
    plt.plot(time, area_matrix[:, 5] / 10**6) 
    plt.ylabel(r'$A_{wetted}$ [$km^2$]', fontsize=12)
    plt.xlabel('Time in minutes since initial change in discharge', fontsize=12)
    plt.title('Wetted area for the different scenarios', fontsize=14)
    plt.legend(["60", "50", "40", "30", "20", "10"], fontsize=12)

if __name__ == '__main__':
    main()
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("wetted_area.png", dpi=400, bbox_inches='tight')
    # plt.show()