import time
import support_functions as sf
import numpy as np
from matplotlib import pyplot as plt
PATH_TO_DATA = "simulation-data"


def main():
      
    simulations_decrease = sf.return_simulation_cases(PATH_TO_DATA + "/start-stop")
    print(simulations_decrease)
    X, Y, _ = sf.read_coordinate_data(simulations_decrease[0])
    area_matrix = np.ndarray((361, len(simulations_decrease)))
    timesteps = range(0, 361, 1)

    # data = sf.read_simulation_data(simulations_decrease[-1])
    # print(sf.compute_area(data[0, :, :], X, Y))
    # print(sf.compute_area(data[100, :, :], X, Y))
    # print(sf.compute_area(data[-1, :, :], X, Y))

       
    data = sf.read_simulation_data(simulations_decrease[-1], 'water_level')
    for index, case in enumerate(simulations_decrease):
        data = sf.read_simulation_data(case)
        
        print("CASE: {}".format(case))
        print("=" * 50)

        for number, time in enumerate(timesteps):
            print("Completion: {:.2f}%".format(100 * time/(len(timesteps))))
            # print("Area: {} m^2 at time: {} minutes".format(sf.compute_area(data[time, :, :], X, Y), time))

            # area_list.append(sf.compute_area(data[time, :, :], X, Y))
            area_matrix[number, index] = sf.compute_area(data[time, :, :], X, Y)

    np.savetxt('area.csv', area_matrix, delimiter=',')
    
    plt.plot(area_matrix, timesteps)
    plt.show()

if __name__ == '__main__':
    main()