import numpy as np
class Found(Exception): pass
def main():
    with open("data.txt") as file:
        numbers = file.readlines()[0]
    numbers = np.fromstring(numbers, sep=",")
    bingo_boards = np.genfromtxt("data.txt", dtype=int, skip_header=1)
    # print(bingo_boards[355:360, :])
    nr_of_rows = len(bingo_boards[:,0])
    all_bingo_boards = list(range(0, nr_of_rows, 5))
    list_of_complete_bingo_boards = []
    list_of_used_numbers = []
    for number in numbers:
        for index in all_bingo_boards: # Check all bingo boards
            bingo_board = bingo_boards[index:index+5, :]
            if number in bingo_board:
                location = np.where(bingo_board == number)
                bingo_boards[location[0][0] + index, location[1][0]] = 1000 # Arbitrary number outside of the range of numbers

                if (5000 in np.sum(bingo_board, axis=1)) or (5000 in np.sum(bingo_board, axis=0)):
                    if index == 30:
                        print(number)
                    list_of_complete_bingo_boards.append(index)
                    list_of_used_numbers.append(number)
                    break
    # print(len(list_of_complete_bingo_boards))
    final_index = list_of_complete_bingo_boards[-1]
    final_matrix = bingo_boards[final_index:final_index+5, :]
    # print(final_matrix)
    # print(final_index)
    # print(list_of_used_numbers)
    test = np.unique(list_of_complete_bingo_boards)
    print(test)
    temp = []
    max_index = 0
    list_index = 0
    for index, item in enumerate(test):
        comp = np.where(item == list_of_complete_bingo_boards)[0]
        if max_index <= np.max(comp[0]):
            max_index = np.max(comp[0])
            list_index = index
    final_index = 30
    final_matrix = bingo_boards[final_index:final_index+5, :]
    print(max_index)
    print(list_index)
    print(list_of_complete_bingo_boards)
    final_matrix = final_matrix[final_matrix != 1000]
    
    print(int(np.sum(final_matrix, axis=None) * 30))
main()