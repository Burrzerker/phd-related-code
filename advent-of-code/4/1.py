import numpy as np
class Found(Exception): pass
def main():
    with open("data.txt") as file:
        numbers = file.readlines()[0]
    numbers = np.fromstring(numbers, sep=",")
    bingo_boards = np.genfromtxt("data.txt", dtype=int, skip_header=1)
    nr_of_rows = len(bingo_boards[:,0])
    try:
        for number in numbers:
            for index in range(0, nr_of_rows, 5): # Check all bingo boards
                bingo_board = bingo_boards[index:index+5, :]
                if number in bingo_board:
                    location = np.where(bingo_board == number)
                    bingo_boards[location[0][0] + index, location[1][0]] = 1000 # Arbitrary number outside of the range of numbers
                    if (5000 in np.sum(bingo_board, axis=1)) or (5000 in np.sum(bingo_board, axis=0)):
                        raise Found
    except Found:
        final_index = index
        final_number = number
    final_matrix = bingo_boards[final_index:final_index+5, :]
    final_matrix = final_matrix[final_matrix != 1000]
    print(int(np.sum(final_matrix, axis=None) * final_number))
main()