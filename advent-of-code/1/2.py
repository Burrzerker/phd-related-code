import numpy as np

def main():
    data = np.loadtxt("data.txt")
    # print(np.count_nonzero(np.diff(data)>0))
    # print(data)
    # sliding_window = np.add.reduceat(data, range(0, len(data), 3))
    # sliding_window = np.convolve(data, np.ones(3, dtype=data.dtype), mode='valid')
    print(np.count_nonzero(np.diff(np.convolve(data, np.ones(3, dtype=data.dtype), mode='valid'))>0))
    # print(np.count_nonzero(np.diff(sliding_window)>0))
main()