def main():
    horizontal = 0
    vertical = 0
    aim = 0
    with open("data.txt") as f:
        for line in f.readlines():
            line = line.split()
            if line[0] == "forward":
                horizontal += int(line[1])
                vertical += aim*int(line[1])
            if line[0] == "up":
                aim -= int(line[1])
            if line[0] == "down":
                aim += int(line[1])
    print(horizontal*vertical)
main()