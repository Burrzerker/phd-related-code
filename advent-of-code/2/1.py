def main():
    horizontal = 0
    vertical = 0
    with open("data.txt") as f:
        for line in f.readlines():
            line = line.split()
            if line[0] == "forward":
                horizontal += int(line[1])
            if line[0] == "up":
                vertical -= int(line[1])
            if line[0] == "down":
                vertical += int(line[1])
    print(horizontal*vertical)
main()