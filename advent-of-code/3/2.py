import numpy as np
def main():
    data = np.genfromtxt("data.txt", dtype=int, delimiter=1)
    oxygen_binary = data
    co2_binary = data
    for index in range(len(data[0,:])):
        oxygen_binary = find_oxygen_generator_rating(oxygen_binary, index)
        co2_binary = find_co2_scrubber_rating(co2_binary, index)
    oxygen_binary = "".join(map(str, (list(oxygen_binary)))).replace(" ", "").replace("[", "").replace("]", "")
    co2_binary = "".join(map(str, (list(co2_binary)))).replace(" ", "").replace("[", "").replace("]", "")
    print(int(oxygen_binary, 2) * int(co2_binary, 2))

def find_oxygen_generator_rating(data, index):
    count = np.bincount(data[:,index])
    condition = np.ones((len(data[:,0]), 12), dtype=bool)
    if len(data[:,0]) >=2:
        if count[1] >= count[0]: # More 1 than 0
            column = data[:,index] != 0 # All rows that don't start with 0
            for i in range(len(data[0, :])):
                condition[:, i] = column
        else: # More 0 than 1
            column = data[:,index] == 0 # All rows that start with 0
            for i in range(len(data[0, :])):
                condition[:, i] = column
        return np.reshape(data[condition], (int(len(data[condition])/12), 12))
    else:
        return(data)
    
def find_co2_scrubber_rating(data, index):
    count = np.bincount(data[:,index])
    condition = np.ones((len(data[:,0]), 12), dtype=bool)
    if len(data[:,0]) >=2: 
        if count[1] >= count[0]: # More 1 than 0
            column = data[:,index] == 0 # All rows that start with 0
            for i in range(len(data[0, :])):
                condition[:, i] = column
        else: # More 0 than 1
            column = data[:,index] != 0 # All rows that start with 1
            for i in range(len(data[0, :])):
                condition[:, i] = column
        return np.reshape(data[condition], (int(len(data[condition])/12), 12))
    else:
        return(data)

main()