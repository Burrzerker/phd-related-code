import numpy as np

def main():
    # with open("data.txt") as file:
    #     print(file.readcol)
    data = np.genfromtxt("data.txt", dtype=int, delimiter=1)
    gamma = "".join(map(str, (list(np.array(np.round(np.mean(data, axis=0)), dtype=int)))))
    epsilon = "".join(map(str, (list(np.array(1 - np.floor(np.add(0.5, np.mean(data, axis=0))), dtype=int)))))
    print(int(epsilon, 2) * int(gamma, 2))
    print(int(gamma, 2))
    print(int(epsilon, 2))
main()