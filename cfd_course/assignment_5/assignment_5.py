import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from scipy.optimize import fsolve


def residuals():
    residuals_colocated = {
        '42': '/home/anton/phd-related-code/cfd_course/assignment_5/results/colocated_residuals_42.csv',
        '62': '/home/anton/phd-related-code/cfd_course/assignment_5/results/colocated_residuals_62.csv',
        '82': '/home/anton/phd-related-code/cfd_course/assignment_5/results/colocated_residuals_82.csv'
    }
    residuals_staggered = {
        '42': '/home/anton/phd-related-code/cfd_course/assignment_5/results/staggered_residuals_42.csv',
        '62': '/home/anton/phd-related-code/cfd_course/assignment_5/results/staggered_residuals_62.csv',
        '82': '/home/anton/phd-related-code/cfd_course/assignment_5/results/staggered_residuals_82.csv'
    }
    colocated_residuals_42 = pd.read_csv(residuals_colocated['42'])
    colocated_residuals_62 = pd.read_csv(residuals_colocated['62'])
    colocated_residuals_82 = pd.read_csv(residuals_colocated['82'])
    
    staggered_residuals_42 = pd.read_csv(residuals_staggered['42'])
    staggered_residuals_62 = pd.read_csv(residuals_staggered['62'])
    staggered_residuals_82 = pd.read_csv(residuals_staggered['82'])

    fig, axs = plt.subplots(1, 3, sharex=True, sharey=True)
    line1, line2, line3 = axs[0].semilogy(colocated_residuals_42)
    axs[0].set_title('Residuals Colocated N=42')
    axs[0].set_ylabel('Residual')
    axs[1].semilogy(colocated_residuals_62)
    axs[1].set_title('Residuals Colocated N=62')
    axs[1].set_xlabel('Number of iterations')
    axs[2].semilogy(colocated_residuals_82)
    axs[2].set_title('Residuals Colocated N=82')
    axs[2].legend([line1, line2, line3], ['U', 'V', 'P'])
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig('colocated_residuals.png', dpi=600, bbox_inches='tight')
    fig, axs = plt.subplots(1, 3, sharex=True, sharey=True)
    line1, line2, line3 = axs[0].semilogy(staggered_residuals_42)
    axs[0].set_title('Residuals Staggered N=42')
    axs[0].set_ylabel('Residual')
    axs[1].semilogy(staggered_residuals_62)
    axs[1].set_title('Residuals Staggered N=62')
    axs[1].set_xlabel('Number of iterations')
    axs[2].semilogy(staggered_residuals_82)
    axs[2].set_title('Residuals Staggered N=82')
    axs[2].legend([line1, line2, line3], ['U', 'V', 'P'])
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig('staggered_residuals.png', dpi=600, bbox_inches='tight')

def read_data():
    u_colocated = {
        '42': '/home/anton/phd-related-code/cfd_course/assignment_5/results/u_colocated_42.csv',
        '62': '/home/anton/phd-related-code/cfd_course/assignment_5/results/u_colocated_62.csv',
        '82': '/home/anton/phd-related-code/cfd_course/assignment_5/results/u_colocated_82.csv'
    }
    u_staggered = {
        '42': '/home/anton/phd-related-code/cfd_course/assignment_5/results/u_staggered_42.csv',
        '62': '/home/anton/phd-related-code/cfd_course/assignment_5/results/u_staggered_62.csv',
        '82': '/home/anton/phd-related-code/cfd_course/assignment_5/results/u_staggered_82.csv'
    }
    v_colocated = {
        '42': '/home/anton/phd-related-code/cfd_course/assignment_5/results/v_colocated_42.csv',
        '62': '/home/anton/phd-related-code/cfd_course/assignment_5/results/v_colocated_62.csv',
        '82': '/home/anton/phd-related-code/cfd_course/assignment_5/results/v_colocated_82.csv'
    }
    v_staggered = {
        '42': '/home/anton/phd-related-code/cfd_course/assignment_5/results/v_staggered_42.csv',
        '62': '/home/anton/phd-related-code/cfd_course/assignment_5/results/v_staggered_62.csv',
        '82': '/home/anton/phd-related-code/cfd_course/assignment_5/results/v_staggered_82.csv'
    }
    u_colocated_42 = pd.read_csv(u_colocated['42'])
    u_colocated_62 = pd.read_csv(u_colocated['62'])
    u_colocated_82 = pd.read_csv(u_colocated['82'])
    v_colocated_42 = pd.read_csv(v_colocated['42'])
    v_colocated_62 = pd.read_csv(v_colocated['62'])
    v_colocated_82 = pd.read_csv(v_colocated['82'])

    u_staggered_42 = pd.read_csv(u_staggered['42'])
    u_staggered_62 = pd.read_csv(u_staggered['62'])
    u_staggered_82 = pd.read_csv(u_staggered['82'])
    v_staggered_42 = pd.read_csv(v_staggered['42'])
    v_staggered_62 = pd.read_csv(v_staggered['62'])
    v_staggered_82 = pd.read_csv(v_staggered['82'])

    u_colocated_42, v_colocated_42 = fix_boundary_condition(u_colocated_42, v_colocated_42, 42)
    u_colocated_62, v_colocated_62 = fix_boundary_condition(u_colocated_62, v_colocated_62, 62)
    u_colocated_82, v_colocated_82 = fix_boundary_condition(u_colocated_82, v_colocated_82, 82)

    u_staggered_42, v_staggered_42 = fix_boundary_condition(u_staggered_42, v_staggered_42, 42)
    u_staggered_62, v_staggered_62 = fix_boundary_condition(u_staggered_62, v_staggered_62, 62)
    u_staggered_82, v_staggered_82 = fix_boundary_condition(u_staggered_82, v_staggered_82, 82)

    return u_colocated_42, v_colocated_42, u_colocated_62, v_colocated_62, u_colocated_82, v_colocated_82, u_staggered_42, v_staggered_42, u_staggered_62, v_staggered_62, u_staggered_82, v_staggered_82 

def stress():
    u_colocated_42, v_colocated_42, u_colocated_62, v_colocated_62, u_colocated_82, v_colocated_82, u_staggered_42, v_staggered_42, u_staggered_62, v_staggered_62, u_staggered_82, v_staggered_82 = read_data() 
    
    if plot_stress:
        x_42 = np.linspace(0, 1, u_colocated_42.shape[0])
        x_62 = np.linspace(0, 1, u_colocated_62.shape[0])
        x_82 = np.linspace(0, 1, u_colocated_82.shape[0])

        plt.subplot(2,3,1)
        plt.streamplot(x_42, x_42, u_colocated_42, v_colocated_42, density=2, color=np.sqrt(u_colocated_42**2 + v_colocated_42**2))
        plt.title('Colocated, N=42')
        plt.subplot(2,3,2)
        plt.streamplot(x_62, x_62, u_colocated_62, v_colocated_62, density=2, color=np.sqrt(u_colocated_62**2 + v_colocated_62**2))
        plt.title('Colocated, N=62')
        plt.subplot(2,3,3)
        plt.streamplot(x_82, x_82, u_colocated_82, v_colocated_82, density=2, color=np.sqrt(u_colocated_82**2 + v_colocated_82**2))
        plt.title('Colocated, N=82')
        plt.subplot(2,3,4)
        plt.streamplot(x_42, x_42, u_staggered_42, v_staggered_42, density=2, color=np.sqrt(u_staggered_42**2 + v_staggered_42**2))
        plt.title('Staggered, N=42')
        plt.subplot(2,3,5)
        plt.streamplot(x_62, x_62, u_staggered_62, v_staggered_62, density=2, color=np.sqrt(u_staggered_62**2 + v_staggered_62**2))
        plt.title('Colocated, N=62')
        plt.subplot(2,3,6)
        plt.streamplot(x_82, x_82, u_staggered_82, v_staggered_82, density=2, color=np.sqrt(u_staggered_82**2 + v_staggered_82**2))
        plt.title('Colocated, N=82')
    stress_colocated_42 = compute_stress_on_boundary(u_colocated_42, v_colocated_42, 42)
    stress_colocated_62 = compute_stress_on_boundary(u_colocated_62, v_colocated_62, 62)
    stress_colocated_82 = compute_stress_on_boundary(u_colocated_82, v_colocated_82, 82)

    stress_staggered_42 = compute_stress_on_boundary(u_staggered_42, v_staggered_42, 42)
    stress_staggered_62 = compute_stress_on_boundary(u_staggered_62, v_staggered_62, 62)
    stress_staggered_82 = compute_stress_on_boundary(u_staggered_82, v_staggered_82, 82)
    stress_colocated = [stress_colocated_42, stress_colocated_62, stress_colocated_82]
    stress_staggered = [stress_staggered_42, stress_staggered_62, stress_staggered_82]    
    
    for index, stress in enumerate(stress_colocated):
        print("{}: Stress colocated: {}".format(index, stress))
    for index, stress in enumerate(stress_staggered):
        print("{}: Stress stagered: {}".format(index, stress))

    return stress_colocated, stress_staggered

def fix_boundary_condition(u, v, N):
    """
    Add the correct boundary conditions for the vector fields
    """
    U = np.zeros((N,N))
    V = np.zeros((N,N))
    U[0,1:-1] = 1
    V[0,1:-1] = 0
    u = u.to_numpy()
    v = v.to_numpy()
    U[1:, 0:] = u
    V[1:, 0:] = v
    return np.flipud(U), np.flipud(V)


def compute_stress_on_boundary(u, v, N):
    """
    North and south walls T = visc * du/dy
    West and east walls T = visc * dv/dx
    """
    viscosity = 10**-3
    delta = 1 / N # Assume uniform grid (not necesarily true in this case)

    stress_north = sum(viscosity * (u[1, :]- u[0, :]) / delta)
    stress_south = sum(viscosity * (u[-2, :]- u[-1, :]) / delta)
    stress_east = sum(viscosity * (v[:,-2] - v[:, -1]) / delta)
    stress_west = sum(viscosity * (v[:,1] - v[:, 0]) / delta)    
    
    return stress_north + stress_south + stress_west + stress_east

 

def richardsson_extrapolation():
    print('Richardson Extrapolation')
    stress_colocated, stress_staggered = stress()
    representative_sizes = [1/40, 1/60, 1/80] # 1/N

        # return ""
    # Definitions
    r32 = representative_sizes[2]/representative_sizes[1]
    r21 = representative_sizes[1]/representative_sizes[0]
    e_32_colocated = stress_colocated[2] - stress_colocated[1]
    e_21_colocated = stress_colocated[1] - stress_colocated[0]
    e_32_staggered = stress_staggered[2] - stress_staggered[1]
    e_21_staggered = stress_staggered[1] - stress_staggered[0]

    s_colocated = np.sign(e_32_colocated/e_21_colocated)
    s_staggered = np.sign(e_32_staggered/e_21_staggered)
    def f_colocated(p_colocated):
        return (1/np.log(r21))*abs(np.log(np.abs(e_32_colocated/e_21_colocated)) + np.log((r21**p_colocated - s_colocated)/(r32**p_colocated - s_colocated))) - p_colocated  
    def f_staggered(p_staggered):
        return (1/np.log(r21))*abs(np.log(np.abs(e_32_staggered/e_21_staggered)) + np.log((r21**p_staggered - s_staggered)/(r32**p_staggered - s_staggered))) - p_staggered  
    p_colocated = fsolve(f_colocated, 2)
    p_staggered = fsolve(f_staggered, 2)
     
    print("Apparent slope colocated p: {}".format(p_colocated))
    print("Apparent slope staggered p: {}".format(p_staggered))

    phi_21_colocated = (r21**p_colocated*stress_colocated[2] - stress_colocated[1])/(r21**p_colocated - 1)
    phi_32_colocated = (r32**p_colocated*stress_colocated[1] - stress_colocated[0])/(r32**p_colocated - 1)
    print("Extrapolated value for colocated: {}".format(phi_32_colocated))
    
    phi_21_staggered = (r21**p_staggered*stress_staggered[2] - stress_staggered[1])/(r21**p_staggered - 1)
    phi_32_staggered = (r32**p_staggered*stress_staggered[1] - stress_staggered[0])/(r32**p_staggered - 1)
    print("Extrapolated value for staggered: {}".format(phi_32_staggered))
    

plot_residuals = True
plot_stress = False
if plot_residuals:
    residuals()
richardsson_extrapolation()

plt.show()