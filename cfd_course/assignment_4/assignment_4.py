# -*- coding: utf-8 -*-
"""
Created on Fri Jan 10 10:14:30 2020

@author: Anton Burman
"""
import numpy as np
from matplotlib import pyplot as plt

def tri_diag_matrix(a, b, c, N, k1=-1, k2=0, k3=1):
    return np.diag(np.ones(N-1)*a, k1) + np.diag(np.ones(N)*b, k2) + np.diag(np.ones(N-1)*c, k3)

plots = True

# Physical parameters
gamma = 0.1
u = 1
rho = 1
# dt = 0.002975 Theoretical stability limit
dt = 0.002975
N = 41
dx = 1/N
Nt = 300000 # Number of timesteps
threshold = 1e-5

peclet = rho * u / gamma
# Boundary conditions
x_l = 0
x_r = 1

# Spatial discretization
a = (-u / (2*dx)) + (gamma / (rho*dx**2))
b = (-2 * gamma) / (rho * dx**2)
c = (u / (2*dx)) + (gamma / (rho*dx**2))
explicit_system = tri_diag_matrix(a, b, c, N)
print(peclet)

# =========== Solvers ===========
def euler_forward():
    print("Euler forward")
    solution_matrix = np.zeros((Nt + 1, N))
    initial_condition = np.zeros(N)
    initial_condition[-1] = 1
    solution_matrix[:, -1] = 1 # Boundary condition
    
    step = 0
    error = [0]
    
    while True:
       if step % 1000 == 0:
           print(step)
       phi = solution_matrix[step, :]
       phi_new = phi + dt*np.dot(explicit_system, phi)
       phi_new[-1] = x_r
       phi_new[0] = x_l
       e = np.linalg.norm(phi_new-phi)
       error.append(e)
       solution_matrix[step + 1] = phi_new
       if step > 2 and e < threshold:
           break
       step += 1
    print("Forward Euler timesteps: {}".format(step))

    return solution_matrix, error, step


def runge_kutta_third_order():
    print("Runge kutta third order")
    solution_matrix = np.zeros((Nt + 1, N))
    initial_condition = np.zeros(N)
    initial_condition[-1] = 1
    solution_matrix[:, -1] = 1 # Boundary condition
    
    step = 0
    error = [0]
    while True:
        if step % 1000 == 0:
            print(step)
        # Predictor step
        phi = solution_matrix[step, :]
        phi_1 = phi + (dt/3)*np.dot(explicit_system, phi)
        phi_1[-1] = x_r
        phi_1[0] = x_l
        # Midpoint rule step
        phi_2 = phi + (2*dt/3)*np.dot(explicit_system, phi_1)
        phi_2[-1] = x_r
        phi_2[0] = x_l
        # Trapetzoidal step
        phi_new = phi + (dt/4)*(np.dot(explicit_system, solution_matrix[step, :]) + 3* np.dot(explicit_system, phi_2))
        phi_new[-1] = x_r
        phi_new[0] = x_l
        solution_matrix[step + 1, :] = phi_new
        e = np.linalg.norm(phi_new-phi)
        error.append(e)
        if step > 2 and e < threshold:
            break 
        step += 1
    print("Runge Kutta timesteps: {}".format(step))
    return solution_matrix, error, step

def ode_function(t, y):
    """
    Support function for scipy.integrate.solve_ivp
    """
    y[-1] = x_r
    y[0] = x_l
    y = np.dot(y, explicit_system)
    y[-1] = x_r
    y[0] = x_l
    return y

def adams_bashfort_moulton():
    from scipy.integrate import solve_ivp
    print("Adams Bashfort Moulton method")

    initial_condition = np.zeros(N)
    initial_condition[-1] = 1

    sol = solve_ivp(ode_function, [0, 1], initial_condition, method='LSODA', rtol=1e-8, atol=1e-9)
    t = sol.t
    y = sol.y
    y[-1, :] = x_r
    y[0,:] = x_l
    return t, y

t, solution_adams = adams_bashfort_moulton()
solution_runge_kutta, error_runge_kutta, iterations_runge_kutta = runge_kutta_third_order()
solution_euler_forward, error_euler_forward, iterations_euler_forward = euler_forward()

# =========== Plots ===========
plots = False
if plots:
    x_plot = np.linspace(0, 1, N)

    fig, axs = plt.subplots(2, sharex=True, sharey=False)
    axs[0].plot(np.linspace(0, iterations_euler_forward, len(error_euler_forward)), error_euler_forward)
    axs[0].set_xlabel(r"Number of iterations")
    axs[0].set_ylabel(r"$||\phi^{n}-\phi^{n-1}||$")
    axs[0].set_title("Error as a funciton of number of iterations, Euler forward")
    fig.subplots_adjust(hspace=.5)
    axs[1].plot(np.linspace(0, iterations_runge_kutta, len(error_runge_kutta)), error_runge_kutta)
    axs[1].set_xlabel(r"Number of iterations")
    axs[1].set_ylabel(r"$||\phi^{n}-\phi^{n-1}||$")
    axs[1].set_title("Error as a funciton of number of iterations, Huen's method")
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig('assignment_4/error.png', dpi=600, bbox_inches='tight')

    x_exact =np.linspace(0, 1, 1000)
    exact_solution = x_l + ((np.exp(x_exact*peclet*1) - 1) / (np.exp(peclet))) * (x_r - x_l)

    fig, axs = plt.subplots(1, 3, sharex=True, sharey=True)
    line2, = axs[0].plot(x_plot, solution_adams[:,-1], 'rx')
    line1, = axs[0].plot(x_plot, solution_runge_kutta[len(error_runge_kutta)-1, :], 'b-')
    line5, = axs[0].plot(x_exact, exact_solution, 'k--', linewidth=1)
    axs[0].set_ylabel(r"$\phi_{steady}$")
    axs[0].set_title("Adam's Method")
    axs[0].set_aspect(1)
    line3, = axs[1].plot(x_plot, solution_euler_forward[len(error_euler_forward)-1, :], 'ro')
    axs[1].plot(x_plot, solution_runge_kutta[len(error_runge_kutta)-1, :], 'b-')
    axs[1].set_title("Euler Forward")
    axs[1].set_xlabel(r"$x$")
    axs[1].set_aspect(1)
    line4, = axs[2].plot(x_plot, solution_runge_kutta[len(error_runge_kutta)-1, :], 'r*')
    axs[2].plot(x_plot, solution_runge_kutta[len(error_runge_kutta)-1, :], 'b-')
    axs[2].set_title("Heun's Method")
    axs[2].set_aspect(1)
    axs[2].legend([line1, line2, line3, line4, line5], ["Reference solution", "Adam's method", "Euler Forward", "Heun's method", "Assignment 1 solution"], bbox_to_anchor=(1.1, 1.05))
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig('assignment_4/steady_state.png', dpi=600, bbox_inches='tight')

    plt.show()
