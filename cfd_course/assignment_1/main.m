% Matlab M-file for the first home assignment in the CFD course 2019
%
% Written by Rikard Gebart, 16 January 1998; Modified 30 October 2019;
 
%%% prepare for start
tic         % Start a stopwatch timer
clear       % clear variables and functions from memory
 
%%% input variables
rho=1.0;    % density

u = 1;      % initial velocity in the X direction
L = 1;      % domain size
gamma = 0.02;    % kappa (diffusivity)
Peclet=rho*u*L/gamma;   % Peclet number
 
%%% discretise the geometry
Nint = 41;         % number of intervals
delta_x= L/Nint;      % delta X
N = Nint-1;         % number of internal nodes (there are also two boundary nodes)
 
%%% Boundary conditions
phi_left = 0; % Left boundary phi
phi_right = 1;   % Right boundary phi
%%
%%%%%%%%%%%%%%%%%%%%%%
%%%   CDS scheme   %%%
%%%%%%%%%%%%%%%%%%%%%%
 
%%% Initialize coefficients
 
Ae = zeros (N,1);
Aw = zeros (N,1);
Ap = zeros (N,1);
xnode = zeros (N,1);
 
PhiCDS = zeros (N,1);   % solution vector for CDS
PhiUDS = zeros (N,1);   % solution vector for UDS
PhiHUDS = zeros (N,1);  % solution vector for HUDS
A = sparse (N,3);
Q = zeros (N,1);
 
% generate a vector with node locations that can be used for plotting
% Please note that the coordinates would be different with a control volume
% discretisation
% See sketch below for node locations relative to the boundaries denoted
% with capital I
%
% I-----o-----o-----o--    --o-----o-----o-----o-----I
 
for i=1:N
    xnode(i) = i*delta_x;
end
 
%%% Calculate diffusion and convection (flux) coefficients 
 
Ade=-(rho*u/(2*delta_x));
Adw=-(rho*u/(2*delta_x));
Adp=-(Ade+Adw);
 
Ace= (rho*u/(2*delta_x));
Acw=-(rho*u/(2*delta_x));
Acp=-(Ace+Acw);     % for CDS this will be zero
 
% define system matrix coefficients
for i=1:N
    Ae(i)=Ade+Ace;  % East matrix coefficient for row i
    Aw(i)=Adw+Acw;  % West matrix coefficient for row i
    Ap(i)=Adp+Acp;  % Diagonal matrix component for row i
end
 
% Assemble matrix coefficients in system matrix
 
 
% Special treatment of first and last row in the matrix (equations for the
% first and last node)
A(1,1) = Ap(1);
A(1,2) = Ae(1);     
A(N,N) = Ap(N);
A(N,N-1) = Aw(N);   
 
for i=2:N-1
    A(i,i) = Ap(i);
    A(i,i-1) = Aw(i);   
    A(i,i+1) = Ae(i);   
end
 
 
%%% Boundary conditions
% Please note that the boundary coefficients would be different with a FV
% method
 
% left boundary x=0
 
    % To be written!! (Define Q(i) for this boundary)
Q(1)=0;
 
 
% right boundary x=L
 
    % To be written!! (Define Q(i) for this boundary)
Q(N)=-Aw(N);
 
 
    
% Solve the equation system
PhiCDS= A\Q;
 

 %%
%%%%%%%%%%%%%%%%%%%%%%
%%%   UDS scheme   %%%
%%%%%%%%%%%%%%%%%%%%%%
 
%%% Initialize coefficients
 
Ae = zeros (N,1);
Aw= zeros (N,1);
An= zeros (N,1);
As= zeros (N,1);
Ap = zeros (N,1);
 
A = sparse (N,3);
Q = zeros (N,1);
 
%%% Calculate diffusion and convection coefficients
 
Ade = -(gamma/2*delta_x^2);
Adw = -(gamma/delta_x^2);
Adp = -(Ade+Adw);
 
Ace = -(gamma/(2*delta_x^2));
Acw = -rho*u/delta_x;
Acp = -(Ace+Acw);
 
for i=1:N
    Ae(i)=Ade+Ace;  % East matrix coefficient
    Aw(i)=Adw+Acw;  % West matrix coefficient
    Ap(i)=Adp+Acp;  % Diagonal matrix component
end
 
% Assemble matrix coefficients in system matrix
 
 
% Special treatment of first and last row in the matrix (equations for the
% first and last node)
A(1,1) = Ap(1);
A(1,2) = Ae(1);     
A(N,N) = Ap(N);
A(N,N-1) = Aw(N);   
 
for i=2:N-1
    A(i,i) = Ap(i);
    A(i,i-1) = Aw(i);   
    A(i,i+1) = Ae(i);   
end
 
 
%%% Boundary conditions
% Please note that the boundary coefficients would be different with a FV
% method
 
% left boundary x=0
 
    % To be written!! (Define Q(i) for this boundary)
Q(1)=0;
 
 
% right boundary x=L
 
    % To be written!! (Define Q(i) for this boundary)
Q(N)=-Aw(N);
 
 
    
% Solve the equation system
PhiUDS= A\Q;
 
%%% draw the graph 
 
% Make new vectors that include the boundary values
 
xp = zeros(N+2,1);
xexact = zeros(100,1);
phiUDS_p = zeros(N+2,1);
phiCDS_p = zeros(N+2,1);
phiExact_p = zeros(N+2,1);  % Vector for exact solution

% Generate a tightly spaced exact solution that can be used for comparison
for i=1:100
    xexact(i) =(i-1)*L/99;
    phiExact_p(i) = phi_left+(exp(xexact(i)*Peclet/L)-1)*(phi_right - phi_left)/(exp(Peclet)-1);
    
end
 
 
% First value (left boundary)
xp(1) = 0;
phiUDS_p(1) = phi_left;
phiCDS_p(1) = phi_left;
 
% Internal values
for i=2:N+1
    xp(i) = xnode(i-1);
    phiUDS_p(i) = PhiUDS(i-1);
    phiCDS_p(i) = PhiCDS(i-1);
end
 
% Last value (right boundary)
xp(N+2) = xp(N+1)+delta_x;
phiUDS_p(N+2) = phi_right;
phiCDS_p(N+2) = phi_right;
 
plot(xp,phiUDS_p,xp,phiCDS_p,xexact,phiExact_p);
 
 
%%% stop the stop-watch
toc