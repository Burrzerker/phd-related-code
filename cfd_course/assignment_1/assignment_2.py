"""
Created on Fri Nov  1 15:52:10 2019
@author: Anton Burman
"""
import numpy as np
from matplotlib import pyplot as plt
def tri_diag_matrix(a, b, c, N, k1=-1, k2=0, k3=1):
    return np.diag(np.ones(N-1)*a, k1) + np.diag(np.ones(N)*b, k2) + np.diag(np.ones(N-1)*c, k3)
# Defining variables, taken from MATLAB script
L = 1.0                       # Domain size
u = 1.0                       # Velocity
gamma = 0.005                 # Diffusivity
rho = 1.0                     # Density
Peclet = np.float128(rho*L*u / gamma)

print "Current Peclet number is: {}".format(Peclet)
#=================
# Defining the geometry
N_tot = 301                   # Number of nodes
delta_x = L/N_tot             # Grid size
N_tot = int(N_tot)
phi_left = 0.0
phi_right = 1.0
#=================
## Exact solution
x = np.linspace(0, L, int(N_tot))
x_exact =np.linspace(0, L, 1000)
exact_solution = phi_left + ((np.exp(x_exact*Peclet*L) - 1) / (np.exp(Peclet))) * (phi_right - phi_left)
#=================
# Central Differencing Scheme (CDS)
a_cds = (-gamma/(delta_x**2))- (rho*u/(2*delta_x))
b_cds = 2*gamma/(delta_x**2)
c_cds = ((rho*u/(2*delta_x)) - (gamma/(delta_x**2)))

# Boundary Conditions CDS
q = np.zeros(N_tot)
q[0] = -phi_left*a_cds
q[-1] = -phi_right * c_cds
system_cds = tri_diag_matrix(a_cds, b_cds, c_cds, N_tot)

# Solution CDS
phi_cds = np.linalg.solve(system_cds, q)
solution_cds = np.zeros(len(x)) # Solution vector
solution_cds[:-1] = phi_cds[1:] # Ensure right boundary condition CDS
solution_cds[-1] = phi_right 
#=================
# First Order Upwind Scheme (FUDS)
a_fuds = -((gamma/delta_x**2) + (rho*u/delta_x)) 
b_fuds = -((-2*gamma/delta_x**2) - (rho*u/delta_x)) 
c_fuds = -(gamma/(delta_x**2))

# Boundary Conditions FUDS
q = np.zeros(N_tot)
q[0] = -phi_left*a_fuds
q[-1] = -phi_right * c_fuds
system_fuds = tri_diag_matrix(a_fuds, b_fuds, c_fuds, N_tot)

# Solution FUDS
phi_fuds = np.linalg.solve(system_fuds, q)
solution_fuds = np.zeros(len(x)) # Solution vector
solution_fuds[:-1] = phi_fuds[1:] # Ensure right boundary condition CDS
solution_fuds[-1] = phi_right 
#=================
# Second Order Upwind Scheme (SUDS)
a_suds = -((2*gamma/delta_x**2) + (3*rho*u/(2*delta_x)))
b_suds = ((2*rho*u/delta_x) + (5*gamma/delta_x**2)) 
c_suds = -((4*gamma/delta_x**2) + (rho*u/(2*delta_x)))
d_suds = (gamma/delta_x**2)

# Boundary Conditions SUDS
q = np.zeros(N_tot)
q[0] = -phi_left*a_fuds
q[-1] = -phi_right*c_fuds

# Add 4th diagonal to the matrix
system_suds = tri_diag_matrix(a_suds, b_suds, c_suds, N_tot) +  np.diag(np.ones(N_tot-2)*d_suds, 2)

# Implement first order upwind scheme for right side boundary condition
# Last row of the matrix
system_suds[-1][-2] = a_fuds 
system_suds[-1][-1] = b_fuds

# Second last row of the matrix
system_suds[-2][-3] = a_fuds
system_suds[-2][-2] = b_fuds
system_suds[-2][-1] = c_fuds

# Solution SUDS
phi_suds = np.linalg.solve(system_suds, q)
solution_suds = np.zeros(len(x)) # Solution vector
solution_suds[:-1] = phi_suds[1:] # Ensure right boundary condition CDS
solution_suds[-1] = phi_right 
solution_suds[0] = phi_left
#=================
#Plot
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams.update({'font.size': 16})
fig = plt.figure()
line1, = plt.plot(x, solution_fuds, 'rd--', linewidth=2)
line2, = plt.plot(x, solution_cds, 'bx--', linewidth=2)
line3, = plt.plot(x, solution_suds, 'g+--', linewidth=2)
line4, = plt.plot(x_exact, exact_solution, 'k--', linewidth=1)
plt.plot(x_exact, np.ones(len(x_exact)), '--', linewidth=1)
plt.title("Comparison Between CDS, FUDS and SUDS Solution, number of nodes: {}, Peclet: {}".format(N_tot, Peclet))
plt.xlabel('$x$')
plt.ylabel("$\phi(x)$")
plt.legend([line1, line2, line3, line4], ['FUDS', 'CDS', 'SUDS', 'Exact'])
plt.show()

