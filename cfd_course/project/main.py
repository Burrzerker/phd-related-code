import cv2 as cv
import os
import shutil
import numpy as np
from scipy.optimize import fsolve
from matplotlib import pyplot as plt
from matplotlib import rc
from scipy.io import loadmat as loadmat

PATH_TO_SIMULATIONS = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/mesh-study/results"

def main():
    data_coarse = loadmat(PATH_TO_SIMULATIONS + "/water_level_coarsest.mat")
    X_coarse = data_coarse['data']['X'][0][0][0:-1, 0:-1]
    Y_coarse = data_coarse['data']['Y'][0][0][0:-1, 0:-1]
    val_coarse = data_coarse['data']['Val'][0][0]
    area_coarse = compute_area(val_coarse, X_coarse, Y_coarse)

    data_less_fine = loadmat(PATH_TO_SIMULATIONS + "/water_level_less_fine.mat")
    X_less_fine = data_less_fine['data']['X'][0][0][0:-1, 0:-1]
    Y_less_fine = data_less_fine['data']['Y'][0][0][0:-1, 0:-1]
    val_less_fine = data_less_fine['data']['Val'][0][0]

    data_finer = loadmat(PATH_TO_SIMULATIONS + "/water_level_finer.mat")
    X_finer = data_finer['data']['X'][0][0][0:-1, 0:-1]
    Y_finer = data_finer['data']['Y'][0][0][0:-1, 0:-1]
    val_finer = data_finer['data']['Val'][0][0]

    data_finest = loadmat(PATH_TO_SIMULATIONS + "/water_level_finest.mat")
    X_finest = data_finest['data']['X'][0][0][0:-1, 0:-1]
    Y_finest = data_finest['data']['Y'][0][0][0:-1, 0:-1]
    val_finest = data_finest['data']['Val'][0][0]


    area_coarse = compute_area(val_coarse, X_coarse, Y_coarse)
    area_less_fine = compute_area(val_less_fine, X_less_fine, Y_less_fine)
    area_finer = compute_area(val_finer, X_finer, Y_finer)
    area_finest = compute_area(val_finest, X_finest, Y_finest)

    area = [area_coarse,  area_finer, area_finest]
    # for areas in area:
    #     print(areas)
    richardsson_extrapolation(area)
    
def representative_size(M, N):
    return np.sqrt(1 / M*N)

def richardsson_extrapolation(area):
    """
    Coarse: MxN = 521x26
    Less Fine: MxN = 1040x74
    Finer: MxN = 2078x218
    Finest: MxN = 4154x218
    Representative size = sqrt(1/MxN)
    """
    coarse = representative_size(521, 26)
    less_fine = representative_size(1040, 74)
    finer = representative_size(2078, 218)
    finest = representative_size(4154, 218)
    representative_sizes = [coarse, less_fine, finer, finest] 

    r32 = representative_sizes[3]/representative_sizes[2]
    r21 = representative_sizes[2]/representative_sizes[1]
    e_32 = area[3] - area[2]
    e_21 = area[2] - area[1]

    s = np.sign(e_32/e_21)
    def f(p):
        return (1/np.log(r21))*abs(np.log(np.abs(e_32/e_21)) + np.log((r21**p - s)/(r32**p - s))) - p  
    p = fsolve(f, 2)
    print("Apparent slope p: {}".format(p))

    phi_32 = (r32**p*area[2] - area[1])/(r32**p - 1)
    phi_21 = (r21**p*area[3] - area[2])/(r21**p - 1)
    print("Extrapolated value for 32: {}".format(phi_32))
    print("Extrapolated value for 21: {}".format(phi_21))
    # area.append(phi_21)
    rc('text', usetex=True)
    plt.figure()
    line_1 = plt.plot([0.0086, 0.0036, 0.0015, 0.0011], np.array(area) / 10**6, 'bd', markersize=14)
    line_2 = plt.plot([0], phi_21 / 10**6, 'ro', markersize=14)
    plt.grid()
    plt.title("Wetted Area as a Function of Representative Size", fontsize=14)
    plt.legend(['Simulated values', 'Richardson Extrapolation'], fontsize=14)
    plt.xlabel('Representative size [$1/m$]', fontsize=14)
    plt.ylabel('$A_{wetted}$ $[km^2]$', fontsize=14)
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("extrapolation.png", dpi=400) # , bbox_inches='tight'
    # plt.plot(representative_sizes, area, 'd')
    # plt.plot(phi_21, 'x')
    # plt.plot(phi_32, 'd')


def generate_png_for_image_processing(data, X, Y):
    # print(data)
    # data[data < 10**-10] = np.nan # For 50-21 case
    data[~np.isnan(data)] = 1

    plt.contourf(X, Y, data)

    plt.hlines(np.amax(Y), np.amin(X), np.amax(X))
    plt.hlines(np.amin(Y), np.amin(X), np.amax(X))
    plt.vlines(np.amax(X), np.amin(Y), np.amax(Y))
    plt.vlines(np.amin(X), np.amin(Y), np.amax(Y))
    plt.axis('off')
    # plt.show()
    plt.savefig("/home/anton/phd-related-code/cfd_course/project/temp/temp.png", bbox_inches='tight', dpi=400)
    plt.close()

def compute_area(data, X, Y):
    area_of_square = (np.amax(Y) - np.amin(Y)) * ((np.amax(X) - np.amin(X)))
    try:
        shutil.rmtree("temp")
    except:
        pass
    os.mkdir("temp")
    generate_png_for_image_processing(data, X, Y)
    img = cv.imread("temp/temp.png", 0)
    img = img[45:-45, 45:-45]
    total_number_of_pixels = img.shape[0] * img.shape[1]
    number_of_active_pixels = 0
    for row in img:
        for number in row:
            if number != 255:
                number_of_active_pixels += 1 

    return area_of_square * number_of_active_pixels / total_number_of_pixels

if __name__ == "__main__":
    main()
    plt.show()