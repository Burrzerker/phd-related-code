# -*- coding: utf-8 -*-
import sys
import numpy as np
import matplotlib.pyplot as plt
import time
sys.path.append('/home/anton/phd-related-code/cfd_course/assignment_2')
from assignment_2 import uds_scheme, cds_scheme, sort_result_matrix
from scipy.sparse.linalg import gmres, bicg, bicgstab
# Variables 
N = 8
gamma = 0.005
u = 1.0
v = 0.5
delta = 1/N
NN = np.ones(N**2)

def is_diagonally_dominant(x):
    abs_x = np.abs(x)
    return np.all(2*np.diag(abs_x) >= np.sum(abs_x, axis=1))


# Boundary conditions
phi_low=1      # Lower boundary phi
phi_left=100	 # Left boundary phi
phi_right=0	 # Right boundary phi
phi_up=0	    # Upper  boundary phi

x = np.linspace(delta/2, 1-delta/2, N)
y = np.linspace(delta/2, 1-delta/2, N)
## Define coordinates for all cell centres (for plotting)
[X,Y] = np.meshgrid(x,y);

# Import UDS and CDS matrices from previous exercice, also import exact solution
result_uds, A_uds, Q_uds = uds_scheme(N, NN)
result_cds, A_cds, Q_cds = cds_scheme(N, NN)

# Check symmetry
print("Grid is {}x{} large".format(N, N))
print("UDS Symmetry: {}".format(np.all(np.abs(A_uds-A_uds.T) < 1e-8)))
print("CDS Symmetry: {}".format(np.all(np.abs(A_cds-A_cds.T) < 1e-8)))
print("UDS Diagonally Dominant: {}".format(is_diagonally_dominant(A_uds)))
print("CDS Diagonally Dominant: {}".format(is_diagonally_dominant(A_cds)))
# Find Eigenvalues

# Conjugate gradient
#phi_cg_uds = scipy.sparse.linalg.cg(A_uds, Q_uds)
t = time.time()
phi_cg_uds, info_uds = bicg(A_uds, Q_uds)
print("BICG UDS Elapsed time: {}".format(time.time() - t))
uds_cg_solution = sort_result_matrix(phi_cg_uds, N)

t = time.time()
phi_cg_cds, info_cds = bicg(A_cds, Q_cds)
print("BICG CDS Elapsed time: {}".format(time.time() - t))
cds_cg_solution = sort_result_matrix(phi_cg_cds, N)

print("UDS BICG Exit Code: {}".format(info_uds))
print("CDS BICG Exit Code: {}".format(info_cds))

t = time.time()
phi_stab_uds, info_stab_uds = bicgstab(A_uds, Q_uds)
print("BICG Stab UDS Elapsed time: {}".format(time.time() - t))
uds_bicgstab_solution = sort_result_matrix(phi_stab_uds, N)

t = time.time()
phi_stab_cds, info_stab_cds = bicgstab(A_cds, Q_cds)
print("BICG Stab CDS Elapsed time: {}".format(time.time() - t))
cds_bicgstab_solution = sort_result_matrix(phi_stab_cds, N)

print("UDS BICG STAB Exit Code: {}".format(info_stab_uds))
print("CDS BICG STAB Exit Code: {}".format(info_stab_cds))

t = time.time()
phi_gmres_uds, info_gmres_uds = gmres(A_uds, Q_uds)
print("GMRES UDS Elapsed time: {}".format(time.time() - t))
uds_gmres_solution = sort_result_matrix(phi_gmres_uds, N)

t = time.time()
phi_gmres_cds, info_gmres_cds = gmres(A_cds, Q_cds)
print("GMRES CDS Elapsed time: {}".format(time.time() - t))
cds_gmres_solution = sort_result_matrix(phi_gmres_cds, N)

print("UDS BICG STAB Exit Code: {}".format(info_stab_uds))
print("CDS BICG STAB Exit Code: {}".format(info_stab_cds))

fig, axes = plt.subplots(3,2)
((ax1, ax2), (ax3, ax4), (ax5, ax6)) = axes
fig.suptitle('Comparison between UDS and CDS for Iterative Methods, Grid={}x{}'.format(N,N))
c1 = ax1.imshow(uds_cg_solution)
ax1.set_title("BIGC UDS")
ax1.axis('off')
fig.colorbar(c1, ax=ax1, orientation='vertical')
c2 = ax2.imshow(cds_cg_solution)
ax2.set_title("BICG CDS")
ax2.axis('off')
fig.colorbar(c2, ax=ax2, orientation='vertical')
c3 = ax3.imshow(uds_bicgstab_solution)
ax3.set_title("BICG Stable UDS")
ax3.axis('off')
fig.colorbar(c3, ax=ax3, orientation='vertical')
c4 = ax4.imshow(cds_bicgstab_solution)
ax4.set_title("BICG Stable CDS")
ax4.axis('off')
fig.colorbar(c4, ax=ax4, orientation='vertical')
c5= ax5.imshow(uds_gmres_solution)
ax5.set_title("GMRES UDS")
ax5.axis('off')
fig.colorbar(c5, ax=ax5, orientation='vertical')
c6 = ax6.imshow(cds_gmres_solution)CG 
ax6.set_title("GMRES CDS")
ax6.axis('off')
fig.colorbar(c6, ax=ax6, orientation='vertical')
mng = plt.get_current_fig_manager()
mng.full_screen_toggle()
plt.savefig('comparison_{}x{}.png'.format(N,N), dpi=600, bbox_inches='tight') # 

