"""
Created on Thu Nov 21 13:23:33 2019

@author: Anton Burman
"""
import numpy as np
import matplotlib.pyplot as plt

# Variables 
N = 50
gamma = 0.005
u = 1.0
v = 0.5
delta = 1/N
NN = np.ones(N**2)

# Boundary conditions
phi_low=1      # Lower boundary phi
phi_left=100	 # Left boundary phi
phi_right=0	 # Right boundary phi
phi_up=0	    # Upper  boundary phi

x = np.linspace(delta/2, 1-delta/2, N)
y = np.linspace(delta/2, 1-delta/2, N)
## Define coordinates for all cell centres (for plotting)
[X,Y] = np.meshgrid(x,y);

def sort_result_matrix(A, N):
    results = np.ones((N,N))
#    for index, row in enumerate(results):
    i = 0
    for j, row in enumerate(results):
        for k, _ in enumerate(row):
            results[j, k] = A[i]
            i+=1
    return np.transpose(results)
    
## UDS Scheme
def uds_scheme(N, NN):
    Ads = -gamma
    Adw = -gamma
    Adn = -gamma
    Ade = -gamma
    Adp = -(Ads + Adw + Adn + Ade)
    Acs = -v*delta
    Acw = -u*delta
    Acn = 0
    Ace = 0
    Acp = - (Acs + Acw + Acn + Ace)
    
    # Internal nodes
    Ae = (Ade + Ace) * NN
    As = (Ads + Acs) * NN
    Aw = (Adw + Acw) * NN
    An = (Adn + Acn) * NN
    Ap = Adp + Acp  * NN
    # RHS vector
    Q = np.zeros(N**2)
    # Boundary conditions lower wall y = 0
    As[N-1:N*N:N] = Ads / 2 + Acs
    Ap[N-1:N*N:N] = 7/8 * Adp + Acp
    Q[N-1:N*N:N] = -As[N-1:N*N:N]*phi_low
    As[N-1:N*N:N] = 0
    # Boundary conditions right wall x=l
    Ae[N*N-N:N*N:1] = Ade / 2 + Ace
    Ap[N*N-N:N*N:1] = 7/8 * Adp + Acp
    Q[N*N-N+1:N*N:1] = -Ae[N*N-N+1:N*N:1]*phi_right
    Ae[N*N-N:N*N:1] = (Ade + Ace) * NN[N*N-N:N*N:1]
#    Ae[N*N-N:N*N:1] = 0
    # Boundary conditions upper wall y = l
    An[N-1:N*N:N] = Adn / 2 + Acn  
    Ap[N-1:N*N:N] = 7/8 * Adp + Acp
    Q[N-1:N*N:N] = -An[N-1:N*N:N]*phi_up
    An[N-1:N*N:N] = 0
    # Boundary conditions left wall x=0
    Aw[0:N:1] = Adw / 2 + Acw
    Ap[0:N:1] = 7/8 * Adp + Acp
    Q[0:N:1] = -Aw[0:N:1]*phi_left
    
    A = np.diag(Ap, 0) # Diagonal 
    A = A + np.diag(As[:-1], -1) + np.diag(An[:-1], 1) # North and South coefficients
    A = A + np.diag(Aw[N:], -N) + np.diag(Ae[N:], N) # West abd East coefficients
    solution = np.linalg.solve(A, Q)
    result = sort_result_matrix(solution, N)
    coefficients = [Ae, As, Aw, An, Ap]
    return result, A, Q

def cds_scheme(N, NN):
    Ads = -gamma
    Adw = -gamma
    Adn = -gamma
    Ade = -gamma
    Adp = - (Ads + Adw + Adn + Ade)
    Acs = -v*delta/2
    Acw = -u*delta/2
    Acn =  v*delta/2
    Ace =  u*delta/2
    Acp = - (Acs + Acw + Acn + Ace)
    # Internal nodes
    Ae = (Ade + Ace) * NN
    As = (Ads + Acs) * NN
    Aw = (Adw + Acw) * NN
    An = (Adn + Acn) * NN
    Ap = Adp + Acp  * NN
        # RHS vector
    Q = np.zeros(N**2)
    # Boundary conditions lower wall y = 0
    As[N-1:N*N:N] = Ads / 2 + Acs
    Ap[N-1:N*N:N] = 7/8 * Adp + Acp
    Q[N-1:N*N:N] = -As[N-1:N*N:N]*phi_low
    As[N-1:N*N:N] = 0
    # Boundary conditions right wall x=l
    Ae[N*N-N:N*N:1] = Ade / 2 + Ace
    Ap[N*N-N:N*N:1] = 7/8 * Adp + Acp
    Q[N*N-N+1:N*N:1] = -Ae[N*N-N+1:N*N:1]*phi_right
    Ae[N*N-N:N*N:1] = (Ade + Ace) * NN[N*N-N:N*N:1]
#    Ae[N*N-N:N*N:1] = 0
    # Boundary conditions upper wall y = l
    An[N-1:N*N:N] = Adn / 2 + Acn  
    Ap[N-1:N*N:N] = 7/8 * Adp + Acp
    Q[N-1:N*N:N] = -An[N-1:N*N:N]*phi_up
    An[N-1:N*N:N] = 0
    # Boundary conditions left wall x=0
    Aw[0:N:1] = Adw / 2 + Acw
    Ap[0:N:1] = 7/8 * Adp + Acp
    Q[0:N:1] = -Aw[0:N:1]*phi_left

    A = np.diag(Ap, 0) # Diagonal 
    A = A + np.diag(As[:-1], -1) + np.diag(An[:-1], 1) # North and South coefficients
    A = A + np.diag(Aw[N:], -N) + np.diag(Ae[N:], N) # West abd East coefficients
    solution = np.linalg.solve(A, Q)
    result = sort_result_matrix(solution, N)
    return result, A, Q

def lui_scheme():
    """
    TO BE IMPLEMENTED
    """
    # LUI coefficients
    Ads = -gamma
    Adw = -gamma
    Adn = -gamma
    Ade = -gamma
    Adww = 0
    Adss = 0
    Adp = - (Ads + Adw + Adn + Ade)
    Acs = -2*v*delta
    Acw = -2*u*delta
    Acn =  0
    Ace =  0
    Acww = u*delta/2
    Acss = v*delta/2
    Acp = - (Acs + Acw + Acn + Ace)
    # UDS coeff
    _, uds_solution, uds_coeff = uds_scheme()
    # Internal nodes
    Ae = ((Ade + Ace) - uds_coeff[0]) * NN
    As = ((Ads + Acs) - uds_coeff[1]) * NN
    Aw = ((Adw + Acw) - uds_coeff[2]) * NN
    An = ((Adn + Acn) - uds_coeff[3]) * NN
    Ap = ((Adp + Acp) - uds_coeff[4]) * NN
        # RHS vector
    Q = np.zeros(N**2)
    # Boundary conditions lower wall y = 0
    As[N-1:N*N:N] = Ads / 2 + Acs
    Ap[N-1:N*N:N] = 7/8 * Adp + Acp
    Q[N-1:N*N:N] = -As[N-1:N*N:N]*phi_low
    As[N-1:N*N:N] = 0
    # Boundary conditions right wall x=l
    Ae[N*N-N:N*N:1] = Ade / 2 + Ace
    Ap[N*N-N:N*N:1] = 7/8 * Adp + Acp
    Q[N*N-N+1:N*N:1] = -Ae[N*N-N+1:N*N:1]*phi_right
    Ae[N*N-N:N*N:1] = (Ade + Ace) * NN[N*N-N:N*N:1]
#    Ae[N*N-N:N*N:1] = 0
    # Boundary conditions upper wall y = l
    An[N-1:N*N:N] = Adn / 2 + Acn  
    Ap[N-1:N*N:N] = 7/8 * Adp + Acp
    Q[N-1:N*N:N] = -An[N-1:N*N:N]*phi_up
    An[N-1:N*N:N] = 0
    # Boundary conditions left wall x=0
    Aw[0:N:1] = Adw / 2 + Acw
    Ap[0:N:1] = 7/8 * Adp + Acp
    Q[0:N:1] = -Aw[0:N:1]*phi_left

        
# UDS    
#result_uds, _, _ = uds_scheme()
#plt.figure()
#plt.contourf(y, x, result_uds)
#plt.colorbar()
#plt.xlabel('X')
#plt.ylabel('Y')
#plt.title('UDS Scheme, $\Delta={}$'.format(delta))
#plt.savefig('uds_delta_50.png', bbox_inches='tight', dpi=400)
#
## CDS
#result_cds = cds_scheme()
#plt.figure()
#plt.contourf(y, x, result_cds)
#plt.colorbar()
#plt.xlabel('X')
#plt.ylabel('Y')
#plt.title('CDS Scheme, $\Delta={}$'.format(delta))
#plt.savefig('cds_delta_50.png', bbox_inches='tight', dpi=400)

## LUI
#lui_scheme()