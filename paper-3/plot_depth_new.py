import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import os
import support_functions as sf
import matplotlib.ticker as tick
import datetime

from matplotlib import rc 
from datetime import timedelta
from pandas.plotting import register_matplotlib_converters


PATH = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-water-level-bc/results"
DIVER_1 = "stornorrfors-divers/diver1_tegsbron.csv" # Tegsbron bästa
DIVER_2 = "stornorrfors-divers/diver2_tegsbron.csv" # Tegsbron alternativ
DIVER_3 = "stornorrfors-divers/diver3_smabatshamn.csv" # Småbåtshamnen
DIVER_4 = "stornorrfors-divers/diver4_sanddyna.csv" # Vid sanddynan som syns på kartan
DIVER_5 = "stornorrfors-divers/diver5_kyrkan.csv"
DIVER_6 = "stornorrfors-divers/diver6_grilludden.csv"
AIR_PRESSURE = "stornorrfors-divers/diver7_reference.csv"
GPS_STICK_LENGTH = 1.33

def main():
    rc('text', usetex=True)
    register_matplotlib_converters()
    try:
        diver_1_data = np.genfromtxt(DIVER_1, delimiter=',')[:,4]
        diver_2_data = np.genfromtxt(DIVER_3, delimiter=',')[:,4]
        diver_3_data = np.genfromtxt(DIVER_4, delimiter=',')[:,4]
        diver_4_data = np.genfromtxt(DIVER_5, delimiter=',')[:,4]
        diver_5_data = np.genfromtxt(DIVER_6, delimiter=',')[:-1,4]
        discharge_data = np.genfromtxt("stornorrfors-discharge-21.csv", delimiter=',')

    except FileNotFoundError:
        print("Water level measureuments file not found.")
        raise

    # depth = sf.read_simulation_data(PATH, f"water-depth")
    # level = sf.read_simulation_data(PATH, f"water-level-new-bc")
    level_n_001 = sf.read_simulation_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-0.01-water-level-bc/results", f"water-level")
    level_n_0011 = sf.read_simulation_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-0.011-water-level-bc/results", f"water-level")
    level_n_0012 = sf.read_simulation_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-0.012-water-level-bc/results", f"water-level")
    level_n_0013 = sf.read_simulation_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-0.013-water-level-bc/results", f"water-level")
    level_n_0014 = sf.read_simulation_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-0.014-water-level-bc/results", f"water-level")
    level_n_0015 = sf.read_simulation_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-0.015-water-level-bc/results", f"water-level")
    level_n_0016 = sf.read_simulation_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-0.016-water-level-bc/results", f"water-level")
    level_n_0017 = sf.read_simulation_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-0.017-water-level-bc/results", f"water-level")
    level_n_0018 = sf.read_simulation_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-0.018-water-level-bc/results", f"water-level")
    level_n_0019 = sf.read_simulation_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-0.019-water-level-bc/results", f"water-level")
    level_n_002 = sf.read_simulation_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-0.02-water-level-bc/results", f"water-level")


    # depth = depth[5:, :, :] # First hour is just steady state for initial condition
    # level = level[5:, :, :]
    level_n_001 = level_n_001[5:, :, :]
    level_n_0011 = level_n_0011[5:, :, :]
    level_n_0012 = level_n_0012[5:, :, :]
    level_n_0013 = level_n_0013[5:, :, :]
    level_n_0014 = level_n_0014[5:, :, :]
    level_n_0015 = level_n_0015[5:, :, :]
    level_n_0016 = level_n_0016[5:, :, :]
    level_n_0017 = level_n_0017[5:, :, :]
    level_n_0018 = level_n_0018[5:, :, :]
    level_n_0019 = level_n_0019[5:, :, :]
    level_n_002 = level_n_002[5:, :, :]

    validation_points = sf.return_validation_point_coordinates()
    elevation_gps = sf.stornorrfors_diver_elevations()

    first_day = datetime.datetime(2021, 6, 8, 16)
    last_day = datetime.datetime(2021, 6, 11, 6)
    
    measurement_time = [first_day + datetime.timedelta(minutes=0.5*x) for x in range(0, len(diver_1_data))] # Date Time, GMT+02:00
    simulation_time = [first_day + datetime.timedelta(minutes=10*x) for x in range(0, len(level_n_001[:, 0, 0]))]
    # for index, time in enumerate(simulation_time):
    #     print(index, time)
    color = plt.cm.coolwarm(np.linspace(0, 1, 11))
    fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5, 1, sharex=True)

    ax1.plot(simulation_time, level_n_001[:, validation_points["1"][0], validation_points["1"][1]], color=color[0])
    # ax1.plot(simulation_time, level_n_0011[:, validation_points["1"][0], validation_points["1"][1]], color=color[1])
    ax1.plot(simulation_time, level_n_0012[:, validation_points["1"][0], validation_points["1"][1]], color=color[2])
    # ax1.plot(simulation_time, level_n_0013[:, validation_points["1"][0], validation_points["1"][1]], color=color[3])
    ax1.plot(simulation_time, level_n_0014[:, validation_points["1"][0], validation_points["1"][1]], color=color[4])
    # ax1.plot(simulation_time, level_n_0015[:, validation_points["1"][0], validation_points["1"][1]], color=color[5])
    ax1.plot(simulation_time, level_n_0016[:, validation_points["1"][0], validation_points["1"][1]], color=color[6])
    # ax1.plot(simulation_time, level_n_0017[:, validation_points["1"][0], validation_points["1"][1]], color=color[7])
    ax1.plot(simulation_time, level_n_0018[:, validation_points["1"][0], validation_points["1"][1]], color=color[8])
    # ax1.plot(simulation_time, level_n_0019[:, validation_points["1"][0], validation_points["1"][1]], color=color[9])
    ax1.plot(simulation_time, level_n_002[:, validation_points["1"][0], validation_points["1"][1]], color=color[10])
    ax1.plot(measurement_time,  0.1 + elevation_gps["1"] - GPS_STICK_LENGTH + diver_5_data, 'r')
    ax1.plot(measurement_time,  -0.1 + elevation_gps["1"] - GPS_STICK_LENGTH + diver_5_data, 'r')
    ax1.plot(measurement_time,  elevation_gps["1"] - GPS_STICK_LENGTH + diver_5_data, 'k')
    ax1.set_facecolor('lightgray')
    ax1.grid(True)
 
    ax1.set_title("Grilludden", fontsize=10)


    ax2.plot(simulation_time, level_n_001[:, validation_points["2"][0], validation_points["2"][1]], color=color[0])
    # ax2.plot(simulation_time, level_n_0011[:, validation_points["2"][0], validation_points["2"][1]], color=color[1])
    ax2.plot(simulation_time, level_n_0012[:, validation_points["2"][0], validation_points["2"][1]], color=color[2])
    # ax2.plot(simulation_time, level_n_0013[:, validation_points["2"][0], validation_points["2"][1]], color=color[3])
    ax2.plot(simulation_time, level_n_0014[:, validation_points["2"][0], validation_points["2"][1]], color=color[4])
    # ax2.plot(simulation_time, level_n_0015[:, validation_points["2"][0], validation_points["2"][1]], color=color[5])
    ax2.plot(simulation_time, level_n_0016[:, validation_points["2"][0], validation_points["2"][1]], color=color[6])
    # ax2.plot(simulation_time, level_n_0017[:, validation_points["2"][0], validation_points["2"][1]], color=color[7])
    ax2.plot(simulation_time, level_n_0018[:, validation_points["2"][0], validation_points["2"][1]], color=color[8])
    # ax2.plot(simulation_time, level_n_0019[:, validation_points["2"][0], validation_points["2"][1]], color=color[9])
    ax2.plot(simulation_time, level_n_002[:, validation_points["2"][0], validation_points["2"][1]], color=color[10])
    ax2.plot(measurement_time,  0.1 + elevation_gps["2"] - GPS_STICK_LENGTH + diver_4_data, 'r')
    ax2.plot(measurement_time,  -0.1 + elevation_gps["2"] - GPS_STICK_LENGTH + diver_4_data, 'r')
    ax2.plot(measurement_time, elevation_gps["2"] - GPS_STICK_LENGTH + diver_4_data, 'k')
    ax2.grid(True)
    ax2.set_facecolor('lightgray')
    ax2.set_title("Kyrkan", fontsize=10)


    ax3.plot(simulation_time, level_n_001[:, validation_points["3"][0], validation_points["3"][1]], label="n=0.01", color=color[0])
    # ax3.plot(simulation_time, level_n_0011[:, validation_points["3"][0], validation_points["3"][1]], label="n=0.011", color=color[1])
    ax3.plot(simulation_time, level_n_0012[:, validation_points["3"][0], validation_points["3"][1]], label="n=0.012", color=color[2])
    # ax3.plot(simulation_time, level_n_0013[:, validation_points["3"][0], validation_points["3"][1]], label="n=0.013", color=color[3])
    ax3.plot(simulation_time, level_n_0014[:, validation_points["3"][0], validation_points["3"][1]], label="n=0.014", color=color[4])
    # ax3.plot(simulation_time, level_n_0015[:, validation_points["3"][0], validation_points["3"][1]], label="n=0.015", color=color[5])
    ax3.plot(simulation_time, level_n_0016[:, validation_points["3"][0], validation_points["3"][1]], label="n=0.016", color=color[6])
    # ax3.plot(simulation_time, level_n_0017[:, validation_points["3"][0], validation_points["3"][1]], label="n=0.017", color=color[7])
    ax3.plot(simulation_time, level_n_0018[:, validation_points["3"][0], validation_points["3"][1]], label="n=0.018", color=color[8])
    # ax3.plot(simulation_time, level_n_0019[:, validation_points["3"][0], validation_points["3"][1]], label="n=0.019", color=color[9])
    ax3.plot(simulation_time, level_n_002[:, validation_points["3"][0], validation_points["3"][1]], label="n=0.02", color=color[10])
    ax3.plot(measurement_time,  -0.1 + elevation_gps["3"] - GPS_STICK_LENGTH + diver_3_data, 'r')
    ax3.plot(measurement_time, elevation_gps["3"] - GPS_STICK_LENGTH + diver_3_data, 'k')
    ax3.plot(measurement_time,  0.1 + elevation_gps["3"] - GPS_STICK_LENGTH + diver_3_data, 'r')
    
    ax3.set_title("Sanddynan", fontsize=10)
    ax3.set_ylabel(r"MASL [$m$]", fontsize=14)
    ax3.grid(True)
    ax3.set_facecolor('lightgray')



    ax4.plot(simulation_time, level_n_001[:, validation_points["4"][0], validation_points["4"][1]], color=color[0])
    # ax4.plot(simulation_time, level_n_0011[:, validation_points["4"][0], validation_points["4"][1]], color=color[1])
    ax4.plot(simulation_time, level_n_0012[:, validation_points["4"][0], validation_points["4"][1]], color=color[2])
    # ax4.plot(simulation_time, level_n_0013[:, validation_points["4"][0], validation_points["4"][1]], color=color[3])
    ax4.plot(simulation_time, level_n_0014[:, validation_points["4"][0], validation_points["4"][1]], color=color[4])
    # ax4.plot(simulation_time, level_n_0015[:, validation_points["4"][0], validation_points["4"][1]], color=color[5])
    ax4.plot(simulation_time, level_n_0016[:, validation_points["4"][0], validation_points["4"][1]], color=color[6])
    # ax4.plot(simulation_time, level_n_0017[:, validation_points["4"][0], validation_points["4"][1]], color=color[7])
    ax4.plot(simulation_time, level_n_0018[:, validation_points["4"][0], validation_points["4"][1]], color=color[8])
    # ax4.plot(simulation_time, level_n_0019[:, validation_points["4"][0], validation_points["4"][1]], color=color[9])
    ax4.plot(simulation_time, level_n_002[:, validation_points["4"][0], validation_points["4"][1]], color=color[10])
    ax4.plot(measurement_time,  0.1 + elevation_gps["4"] - GPS_STICK_LENGTH + diver_2_data, 'r')
    ax4.plot(measurement_time,  -0.1 + elevation_gps["4"] - GPS_STICK_LENGTH + diver_2_data, 'r')
    ax4.plot(measurement_time, elevation_gps["4"] - GPS_STICK_LENGTH + diver_2_data, 'k')
    ax4.set_title("Hamnen", fontsize=10)
    ax4.grid(True)
    ax4.set_facecolor('lightgray')


    ax5.plot(simulation_time, level_n_001[:, validation_points["5"][0], validation_points["5"][1]], color=color[0])
    # ax5.plot(simulation_time, level_n_0011[:, validation_points["5"][0], validation_points["5"][1]], color=color[1])
    ax5.plot(simulation_time, level_n_0012[:, validation_points["5"][0], validation_points["5"][1]], color=color[2])
    # ax5.plot(simulation_time, level_n_0013[:, validation_points["5"][0], validation_points["5"][1]], color=color[3])
    ax5.plot(simulation_time, level_n_0014[:, validation_points["5"][0], validation_points["5"][1]], color=color[4])
    # ax5.plot(simulation_time, level_n_0015[:, validation_points["5"][0], validation_points["5"][1]], color=color[5])
    ax5.plot(simulation_time, level_n_0016[:, validation_points["5"][0], validation_points["5"][1]], color=color[6])
    # ax5.plot(simulation_time, level_n_0017[:, validation_points["5"][0], validation_points["5"][1]], color=color[7])
    ax5.plot(simulation_time, level_n_0018[:, validation_points["5"][0], validation_points["5"][1]], color=color[8])
    # ax5.plot(simulation_time, level_n_0019[:, validation_points["5"][0], validation_points["5"][1]], color=color[9])
    ax5.plot(simulation_time, level_n_002[:, validation_points["5"][0], validation_points["5"][1]], color=color[10])
    ax5.plot(measurement_time, elevation_gps["5"] - GPS_STICK_LENGTH + diver_1_data, 'k')
    ax5.plot(measurement_time,  0.1 + elevation_gps["5"] - GPS_STICK_LENGTH + diver_1_data, 'r')
    ax5.plot(measurement_time,  -0.1 + elevation_gps["5"] - GPS_STICK_LENGTH + diver_1_data, 'r')
    ax5.grid(True)
    ax5.set_title("Tegsbron", fontsize=10)
    ax5.set_xlabel(r"Date $[min]$", fontsize=14)
    ax5.set_facecolor('lightgray')

    ax3.legend([r"$n=0.01$", r"$n=0.012$", r"$n=0.014$",r"$n=0.016$",r"$n=0.018$",r"$n=0.02$", r"0.1 $m$ uncertainty", r"Pressure logger"], fontsize=10, loc='center left', bbox_to_anchor=(1, 0.5))
    fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.5)

    # fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5, 1)
    # ax1.plot(simulation_time, level1[:, validation_points["1"][0], validation_points["1"][1]])
    # ax1.plot(measurement_time, elevation_gps["1"] - GPS_STICK_LENGTH +  diver_5_data)
    # ax1.set_title("Grilludden")

    # ax2.plot(simulation_time, level1[:, validation_points["2"][0], validation_points["2"][1]])
    # ax2.plot(measurement_time, elevation_gps["2"] - GPS_STICK_LENGTH + diver_4_data)
    # ax2.set_title("Kyrkan")

    # ax3.plot(simulation_time, level1[:, validation_points["3"][0], validation_points["3"][1]])
    # ax3.plot(measurement_time, elevation_gps["3"] - GPS_STICK_LENGTH + diver_3_data)
    # ax3.set_title("Sanddynan")

    # ax4.plot(simulation_time, level1[:, validation_points["4"][0], validation_points["4"][1]])
    # ax4.plot(measurement_time, elevation_gps["4"] - GPS_STICK_LENGTH + diver_2_data)
    # ax4.set_title("Hamnen")

    # ax5.plot(simulation_time, level1[:, validation_points["5"][0], validation_points["5"][1]])
    # ax5.plot(measurement_time, elevation_gps["5"] - GPS_STICK_LENGTH + diver_1_data)
    # ax5.set_title("Tegsbron")
    # plt.figure()
    # plt.plot(simulation_time, level[:, validation_points["1"][0], validation_points["1"][1]])
    # plt.plot(measurement_time, diver_5_data)
    # plt.title("Grilludden")

    # plt.figure()
    # plt.plot(simulation_time, level[:, validation_points["2"][0], validation_points["2"][1]])
    # plt.plot(measurement_time, elevation_gps["2"] - GPS_STICK_LENGTH + diver_4_data)
    # plt.title("Kyrkan")

    # plt.figure()
    # plt.plot(simulation_time, level[:, validation_points["3"][0], validation_points["3"][1]])
    # plt.plot(measurement_time, elevation_gps["3"] - GPS_STICK_LENGTH + diver_3_data)
    # plt.title("Sanddynan")

    # plt.figure()
    # plt.plot(simulation_time, level[:, validation_points["4"][0], validation_points["4"][1]])
    # plt.plot(measurement_time, elevation_gps["4"] - GPS_STICK_LENGTH + diver_2_data)
    # plt.title("Hamnen")

    # plt.figure()
    # plt.plot(simulation_time, level[:, validation_points["5"][0], validation_points["5"][1]])
    # plt.plot(measurement_time, elevation_gps["5"] - GPS_STICK_LENGTH + diver_1_data)
    # plt.title("Tegsbron")
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig("depth-para-sweep.png", dpi=600, bbox_inches='tight', transparent=False)
    plt.show()


if __name__ == "__main__":
    main()