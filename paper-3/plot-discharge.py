import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import datetime
discharge_20 = "stornorrfors-discharge-20.csv"
discharge_19 = "stornorrfors-discharge-19.csv"


def main():
    
    sample_time = 100 # in minutes

    first_of_june = 217440
    last_of_june = first_of_june + 1440*30

    year = 20
    if year == 20:
        data = np.genfromtxt(discharge_20, delimiter=",")
        data = data[first_of_june: last_of_june]
   
        spillway = data[:,1]
        discharge = data[:, 2]
        first_day = datetime.datetime(2020, 6, 1, 0)
        dates = [first_day + datetime.timedelta(minutes=x) for x in range(0, len(discharge))] # Date Time, GMT+02:00
 
        plt.figure()
        plt.plot(dates[::sample_time], discharge[::sample_time])
        plt.plot(dates[::sample_time], spillway[::sample_time])
        plt.xlabel('Date')
        plt.ylabel('Discharge [m^3/s]')
        plt.title("2020")
        plt.legend(["Turbine", "Spillway"])

    year = 19
    if year == 19:
        data = np.genfromtxt(discharge_19, delimiter=",")
        data = data[first_of_june: last_of_june]

        discharge = data[:, 2]
        spillway = data[:,1]
        first_day = datetime.datetime(2019, 6, 1, 0)
        dates = [first_day + datetime.timedelta(minutes=x) for x in range(0, len(discharge))] # Date Time, GMT+02:00
        plt.figure()
        plt.plot(dates[::sample_time], discharge[::sample_time])
        plt.plot(dates[::sample_time], spillway[::sample_time])
        plt.xlabel('Date')
        plt.ylabel('Discharge [m^3/s]')
        plt.title("2019")
        plt.legend(["Turbine", "Spillway"])
    plt.show()

if __name__ == '__main__':
    main()