import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import os
import support_functions as sf
from matplotlib import rc 

PATH = "downstream-initial-data"

def main():
    X, Y, _ = sf.read_coordinate_data(PATH, "depth_averaged_velocity")
    print(X.shape)
    print(f"Max X: {np.max(X)}")
    print(f"Min X: {np.min(X)}")
    print(f"Max Y: {np.max(Y)}")
    print(f"Min Y: {np.min(Y)}")
    


if __name__ == "__main__":
    main()