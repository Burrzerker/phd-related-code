import os
import collections
import re
import shutil
import cv2 as cv
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rc
from scipy.io import loadmat as loadmat
from scipy.io import savemat as savemat
from scipy.optimize import curve_fit
from osgeo import gdal

def read_background_image(path):
    """
    Reads .tif background map for contour plots
    :param path: path to .tif file
    :returns: np.ndarray that contains image data
    """
    background = gdal.Open(path)
    background_array = background.ReadAsArray()
    shape = background_array.shape
    
    flipped_channels = np.ndarray((shape[1], shape[2], shape[0]))
    flipped_channels[:, :, 0] = background_array[0, :, :]
    flipped_channels[:, :, 1] = background_array[1, :, :]
    flipped_channels[:, :, 2] = background_array[2, :, :]
    flipped_channels[:, :, 3] = background_array[3, :, :]
    return flipped_channels.astype(np.uint8)


def process_dewatering_matrix(data, mask):
    processed_matrix = np.empty(shape=data[0, :, :].shape)
    processed_matrix[:] = np.NaN
    print(processed_matrix.shape)
    _, mask_3d = np.broadcast_arrays(data, mask)
    masked_data = np.ma.array(data, mask=mask_3d)
    masked_data = masked_data.filled(fill_value=np.NaN)
    
    compute_dewatering_rate(masked_data[:, 2075, 130])
    # compute_dewatering_rate(masked_data[:, 2075, 90])
    
    
    # for (time, x_index, y_index), value in np.ndenumerate(masked_data):
    #     if time is not 0:
    #         break
        
    #     if not np.isnan(value):
    #         try:
    #            processed_matrix[x_index, y_index] = compute_dewatering_rate(masked_data[:, x_index, y_index])
    #         #    print(x_index, y_index)
    #         except TypeError:
    #             print(x_index, y_index)
    #             raise TypeError
    #         # print(x_index, y_index, value)
    
    # savemat('cm_dewatering_rate_hours_1.mat', {'dewatering_rate': processed_matrix})
    # print("Done!")

    plt.show()


def compute_dewatering_rate(data):
    rc('text', usetex=True)
    max_data = np.nanmax(data)
    min_data = np.nanmin(data)

    data = data[~np.isnan(data)]

    t = np.linspace(0, len(data), len(data))

    if data[-1] > data[0] or len(data) < 4:
        # decrease_time = np.NaN
        print("Invalid entry.")
        return np.NaN
    else:
        try:
            c, cov = curve_fit(predicted_function, t, data)
        except RuntimeError:
            return np.NaN
        fitted_curve = predicted_function(t, c[0], c[1], c[2], c[3])

        try:
            first_change = np.where(np.diff(fitted_curve) != 0)[0][0]
        except IndexError:
            return np.NaN
        last_wetted_time = len(fitted_curve)
        decrease_time = (last_wetted_time - first_change) # In hours
        water_level_change = (max_data - min_data) * 100 # In centimeters
        font_size = 16
        plt.figure()
        plt.annotate(
            "", xy=(first_change, max_data), xycoords="data", xytext=(last_wetted_time, max_data),
            textcoords="data", arrowprops=dict(
            arrowstyle="<->", connectionstyle="arc3", color='r', lw=2),
        )
        plt.text(last_wetted_time - (last_wetted_time - first_change)/1.5, 0.99*max_data, "t={} minutes".format(decrease_time), color='r', fontsize=font_size)
        plt.vlines(first_change, min_data, max_data)
        plt.vlines(last_wetted_time, min_data, max_data)
        plt.plot(t, fitted_curve)
        plt.plot(t, data, 'x')
        plt.ylabel("WSE [$MASL$]", fontsize=font_size)
        plt.xlabel("t [min]", fontsize=font_size)
        plt.legend(["Curve fit", "Simulated values"], loc="lower left", fontsize=14)
        mng = plt.get_current_fig_manager()
        mng.full_screen_toggle()
        plt.savefig("curve_fit_example_2.png", dpi=600, bbox_inches='tight', transparent=True)
    return water_level_change / decrease_time    


def predicted_function(t, max_data, theta, tau, min_data):
    n = len(t)
    S = np.empty(n)

    for i in range(n):
        if t[i] < theta:
            S[i] = max_data
        else:
            S[i] = (max_data-min_data) * ((np.exp(-(t[i]-theta)/tau))) + min_data
    return S


def normalize_data_decrease(data):
    return (data - data[-1]) / (data[0] - data[-1]) 

def normalize_data_increase(data):
    print(data[0])
    print(data[-1])
    return (data - data[0]) / (data[-1] - data[0]) 

def generate_png_for_image_processing(data, X, Y):

    # data[data < 10**-10] = np.nan # For 50-21 case
    # data[~np.isnan(data)] = 1

    plt.contourf(X, Y, data, [0.001, 1])
    # print(f"max Y: {np.amax(Y)}")
    # print(f"max X: {np.amax(X)}")
    # print(f"min Y: {np.amin(Y)}")
    # print(f"min X: {np.amin(X)}")
    plt.hlines(np.amax(Y), np.amin(X), np.amax(X))
    plt.hlines(np.amin(Y), np.amin(X), np.amax(X))
    plt.vlines(np.amax(X), np.amin(Y), np.amax(Y))
    plt.vlines(np.amin(X), np.amin(Y), np.amax(Y))
    plt.axis('off')

    # plt.show()
    plt.savefig("temp/temp.png", bbox_inches='tight', dpi=400)
    plt.close()

def compute_area(data, X, Y):
    area_of_square = (np.amax(Y) - np.amin(Y)) * ((np.amax(X) - np.amin(X)))
    try:
        shutil.rmtree("temp")
    except:
        pass
    os.mkdir("temp")
    generate_png_for_image_processing(data, X, Y)
    img = cv.imread("temp/temp.png", 0)
    img = img[60:-200, 300:-60]
    total_number_of_pixels = img.shape[0] * img.shape[1]
    number_of_active_pixels = 0
    for row in img:
        for number in row:
            if number != 255:
                number_of_active_pixels += 1 

    return area_of_square * number_of_active_pixels / total_number_of_pixels

def return_simulation_cases(path):
    """
    Returns the path to folders containing .mat file exported from Delft3D
    :param path: path to folder containing subfolders with data
    :return: list contianing the path to the simulations in numerical order 
    """
    list_of_simulations = []
    for dir in os.listdir(path):
        list_of_simulations.append("{}/{}".format(path, dir))
    list_of_simulations.sort(key=natural_keys)
    return list_of_simulations

def read_coordinate_data(simulation, variable="water_level"):
    """
    Returns the X and Y matrices from Delft
    :param Simulation: Path to a .mat file exported from quickplot in Delft3D
    :returns: X and Y matrices
    """
    data = loadmat("{}/{}".format(simulation, variable))
    return data['data']['X'][0][0][0:-1, 0:-1], data['data']['Y'][0][0][0:-1, 0:-1], data['data']['Val'][0][0]

def read_simulation_data(simulation, variable="water_level"):
    """
    Return simulation data, water_level is default other options are
    depth_averaged_velocity and water_depth
    :params simulation:  
    """
    data = loadmat("{}/{}.mat".format(simulation, variable))
    return data['data']['Val'][0][0]

def hysteresis_points_of_interest(variable):
    """
    Find the start of change and end of change for some variable
    :param variable: variable under change, i.e. water level
    :return: indices where start and end in change happens
    """
    threshold = 0.001
    for index, value in enumerate(variable):
        try:
            if abs(variable[index+1] - value) > threshold:
                index_start = index
                break
        except IndexError:
            pass
    for index, value in enumerate(variable):    
        try:
            if abs(value - variable[index-1]) < threshold and index > index_start:
                index_end = index
                break
        except IndexError:
            pass
    return index_start, index_end

def validation_points_cycle(data):
    """
    Crops the validation data so that a increase-decrease cycle is achieved
    :param data: numpy ndarray containing validation data
    :returns data: numpy ndarray with one cycle  
    """

    point_1 = np.ndarray((243))
    point_2 = np.ndarray((243))
    point_3 = np.ndarray((243))
    point_4 = np.ndarray((243))
    point_5 = np.ndarray((243))
    point_6 = np.ndarray((243))
    point_7 = np.ndarray((243))
    point_8 = np.ndarray((243))

    point_1[:100] = data[65775:65775 + 100, 0]
    point_1[100:] = data[67800:67800 + 143, 0]
    point_2[:100] = data[65782:65782 + 100, 1]
    point_2[100:] = data[67800:67800 + 143, 1]
    point_3[:100] = data[65785:65785 + 100, 2]
    point_3[100:] = data[67800:67800 + 143, 2]
    point_4[:100] = data[65793:65793 + 100, 3]
    point_4[100:] = data[67800:67800 + 143, 3]
    point_5[:100] = data[65800:65800 + 100, 4]
    point_5[100:] = data[67800:67800 + 143, 4]
    point_6[:120] = data[65810:65810 + 120, 5]
    point_6[120:] = data[67820:67820 + 123, 5]
    point_7[:100] = data[65827:65827 + 100, 6]
    point_7[100:] = data[67830:67830 + 143 , 6]
    point_8[:100] = data[65845:65845 + 100, 7]
    point_8[100:] = data[67860:67860 + 143, 7]
    # print(data[:67800:67800+193, 0])
    points = np.ndarray((243,8))
    points[:, 0] = point_1
    points[:, 1] = point_2
    points[:, 2] = point_3
    points[:, 3] = point_4
    points[:, 4] = point_5
    points[:, 5] = point_6
    points[:, 6] = point_7
    points[:, 7] = point_8
    return points

def return_validation_data(path):
    """
    Returns time series validation data from .mat file
    Removes the points outside of the domain
    :param path: path to .mat file
    :returns: matrix containing time series in each point
    """
    data = loadmat(path)
    data = data['Diverdata']
    diver_data = np.ndarray(shape=(72000, 8))
    diver_data[:,0] = data[:,3] + 34.2
    diver_data[:,1] = data[:,4] + 28.077
    diver_data[:,2] = data[:,5] + 27.575
    diver_data[:,3] = data[:,6] + 25.846
    diver_data[:,4] = data[:,7] + 22.886
    diver_data[:,5] = data[:,8] + 17.82
    diver_data[:,6] = data[:,9] + 14.816
    diver_data[:,7] = data[:,10] + 12.429
    return diver_data 


def return_validation_point_coordinates():
    """
    Returns the mesh coordinates for validation points
    :return: dict containing validation points
    """
    
    validation_points = { 
        '1': (338, 82),      # Grilludden (338, 82)
        '2': (1700, 80),    # Kyrkan (1760, 115)
        '3': (2087, 80),    # Sandbanken (2077, 90)
        '4': (3161, 93),     # Småbåtshamnen
        '5': (4191, 25),     # Tegsbron
    }
    return validation_points

def validation_points_gps():
    validation_points = { 
        '1': (7088227.0, 752590.75),    # Grilludden
        '2': (7088251.5, 754626.31),    # Kyrkan
        '3': (7088133.5, 755166.69),    # Sandbanken
        '4': (7088333.0, 756865.00),    # Småbåtshamnen
        '5': (7087987.5, 758343.94)     # Tegsbron
    }
    return validation_points

def stornorrfors_diver_elevations():
    validation_points = { 
        '1': 0.968,    # Grilludden
        '2': 0.379,     # Kyrkan
        '3': 0.313,    # Sandbanken
        '4': -0.103,   # Småbåtshamnen
        '5': -0.11     # Tegsbron
    }
    return validation_points

def natural_keys(text):
    """
    sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    """
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

def atoi(text):
    return int(text) if text.isdigit() else text