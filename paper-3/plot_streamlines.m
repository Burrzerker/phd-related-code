data_x_vel = load('/home/anton/phd-related-code/paper-3/downstream-initial-data-second/velocity_x.mat');
data_y_vel = load('/home/anton/phd-related-code/paper-3/downstream-initial-data-second/velocity_y.mat');
X = data_x_vel.data.X;
Y = data_x_vel.data.Y;
X = X(1:end-1, 1:end-1);
Y = Y(1:end-1, 1:end-1);
velocity_x = data_x_vel.data.Val;
velocity_y = data_y_vel.data.Val;
stream_data = stream2(X, Y, velocity_x, velocity_y);
streamline(stream_data)
%streamline(X, Y, velocity_x, velocity_y)
%streamline(velocity_x, velocity_y)
