import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import datetime
from datetime import timedelta
import os
from matplotlib import rc 
from matplotlib.colors import ListedColormap
from scipy.io import loadmat
from pandas.plotting import register_matplotlib_converters

PATH = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/results_sweep"

def return_data(path):
    data = loadmat(path)
    X = data['data']['X'][0][0][0:-1, 0:-1]
    Y = data['data']['Y'][0][0][0:-1, 0:-1]
    read_data = data['data']['Val'][0][0]

    return read_data, X, Y

def main():
    rc('text', usetex=True)
    
    bathymetry, X, Y = return_data(f"{PATH}/initial_bed_level.mat")
    depth_100, _, _ = return_data(f"{PATH}/level_100.mat")
    depth_200, _, _ = return_data(f"{PATH}/level_200.mat")
    depth_300, _, _ = return_data(f"{PATH}/level_300.mat")
    depth_400, _, _ = return_data(f"{PATH}/level_400.mat")
    depth_500, _, _ = return_data(f"{PATH}/level_500.mat")
    depth_600, _, _ = return_data(f"{PATH}/level_600.mat")
    depth_700, _, _ = return_data(f"{PATH}/level_700.mat")
    depth_800, _, _ = return_data(f"{PATH}/level_800.mat")
    depth_900, _, _ = return_data(f"{PATH}/level_900.mat")
    depth_1000, _, _ = return_data(f"{PATH}/level_1000.mat")
    
    trim = 50
    plt.figure()
    plt.plot(depth_100[trim, :])
    plt.plot(depth_200[trim, :])
    plt.plot(depth_300[trim, :])
    plt.plot(depth_400[trim, :])
    plt.plot(depth_500[trim, :])
    plt.plot(depth_600[trim, :])
    plt.plot(depth_700[trim, :])
    plt.plot(depth_800[trim, :])
    plt.plot(depth_900[trim, :])
    plt.plot(depth_1000[trim, :])
    plt.plot(bathymetry[-2, :], 'black')
    plt.xlabel("Spanwise node number")
    plt.ylabel("$[MASL]$")
    plt.legend(["$Q=100~m^3/s$", "$Q=200~m^3/s$", "$Q=300~m^3/s$", "$Q=400~m^3/s$", "$Q=500~m^3/s$",
        "$Q=600~m^3/s$", "$Q=700~m^3/s$", "$Q=800~m^3/s$", "$Q=900~m^3/s$", "$Q=1000~m^3/s$", "Bathymetry"
    ])
    plt.show()



if __name__ == "__main__":
    main()