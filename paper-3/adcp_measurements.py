import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import os
import math
import pandas as pd
import support_functions as sf
import matplotlib as mpl
from matplotlib import rc 
import matplotlib.ticker as tick
import matplotlib.cm as cm
from matplotlib.colors import Normalize
from sweref99 import projections

PATH_005 = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-0.05-water-level-bc/results"
PATH_00125 = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-0.014-water-level-bc/results"
BACKGROUND_IMAGE = "background_high_res.tif"
ADCP_KYRKAN = "kyrkan_adcp_1.txt"
SIMULATION_INDEX = 122 # Corresponds to 12:20 9th of June
def main():
    rc('text', usetex=True)

    background = sf.read_background_image(BACKGROUND_IMAGE)
    X, Y, _ = sf.read_coordinate_data(PATH_00125, f"magnitude")
    x_vel_sim = sf.read_simulation_data(PATH_00125, f"x-vel")
    y_vel_sim = sf.read_simulation_data(PATH_00125, f"y-vel")
    depth_sim = sf.read_simulation_data(PATH_005, f"water-depth")
    water_level_sim = sf.read_simulation_data(PATH_005, f"water-level")
    magnitude_vel_sim_005 = sf.read_simulation_data(PATH_005, f"magnitude")
    magnitude_vel_sim_00125 = sf.read_simulation_data(PATH_00125, f"magnitude")

    tm = projections.make_transverse_mercator("SWEREF_99_TM")

    measurements = read_adcp_measurements(ADCP_KYRKAN)
    u_measured, v_measured, w_measured, depth, lat, lon, total_depth, asd = parse_mearuements(measurements)
    # print(depth[-1, :].shape)
    # print(total_depth)
    northing, easting = project_spatial_coordinates(lat, lon)


    magnitude = np.sqrt(u_measured**2+v_measured**2+w_measured**2)
    u_average = averaging(u_measured, depth)
    v_average = averaging(v_measured, depth)
    w_average = averaging(w_measured, depth)
    magnitude_average = averaging(magnitude, depth)

    depth_averaged_velocity = np.sqrt(u_average**2 + v_average**2 + w_average**2)

    fig, (ax1, ax2, ax3) = plt.subplots(3,1)
    ax1.imshow(background, extent=[np.nanmin(X), np.nanmax(X), np.nanmin(Y), np.nanmax(Y)])
    ax1.set_xticks([], [])
    ax1.set_yticks([], [])
    c = ax1.contourf(X, Y, water_level_sim[SIMULATION_INDEX, :, :], levels=np.linspace(-1.4, 2, 100) , cmap="coolwarm") #
    fig.colorbar(c, ax=ax1, aspect=50, orientation="horizontal", label=r"MASL [$m$]", format='%.1f')
    ax2.imshow(background, extent=[np.nanmin(X), np.nanmax(X), np.nanmin(Y), np.nanmax(Y)])
    ax2.set_xticks([], [])
    ax2.set_yticks([], [])
    c = ax2.contourf(X, Y, depth_sim[SIMULATION_INDEX, :, :], levels=np.linspace(0.01, np.nanmax(depth_sim[SIMULATION_INDEX, :, :]), 100) , cmap="coolwarm") #
    fig.colorbar(c, ax=ax2, aspect=50, orientation="horizontal", label=r"Depth [$m$]", format='%.1f')
    ax3.imshow(background, extent=[np.nanmin(X), np.nanmax(X), np.nanmin(Y), np.nanmax(Y)])
    ax3.set_xticks([], [])
    ax3.set_yticks([], [])
    c = ax3.contourf(X, Y, magnitude_vel_sim_005[SIMULATION_INDEX, :, :], levels=np.linspace(0.01, np.nanmax(magnitude_vel_sim_00125[SIMULATION_INDEX, :, :]), 100) , cmap="coolwarm") 
    fig.colorbar(c, ax=ax3, aspect=50, orientation="horizontal", label=r"Depth Averaged Velocity [$m/s$]", format='%.1f')
    fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.5)
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("simulation_vel_depth_elevation_n_005.png", dpi=600, bbox_inches='tight', transparent=False)



    fig, (ax1, ax2, ax3) = plt.subplots(3,1)
    ax1.imshow(background, extent=[np.nanmin(X), np.nanmax(X), np.nanmin(Y), np.nanmax(Y)])
    ax1.set_xticks([], [])
    ax1.set_yticks([], [])

    c = ax1.contourf(X, Y, y_vel_sim[SIMULATION_INDEX, :, :], levels=np.linspace(np.nanmin(y_vel_sim[-1, :, :]), np.nanmax(y_vel_sim[-1, :, :]), 100), cmap="coolwarm")
    # ax1.set_xlim(754360, 754856)
    # ax1.set_ylim(7088090, 7088290)
    ax2.imshow(background, extent=[np.nanmin(X), np.nanmax(X), np.nanmin(Y), np.nanmax(Y)])
    ax2.set_xticks([], [])
    ax2.set_yticks([], [])

    c = ax2.contourf(X, Y, x_vel_sim[SIMULATION_INDEX, :, :], levels=np.linspace(np.nanmin(x_vel_sim[-1, :, :]), np.nanmax(x_vel_sim[-1, :, :]), 100) , cmap="coolwarm")   
    # ax2.set_xlim(754360, 754856)
    # ax2.set_ylim(7088090, 7088290)
    ax3.imshow(background, extent=[np.nanmin(X), np.nanmax(X), np.nanmin(Y), np.nanmax(Y)])
    ax3.set_xticks([], [])
    ax3.set_yticks([], [])
    c = ax3.contourf(X, Y, magnitude_vel_sim_00125[-1, :, :], levels=np.linspace(np.nanmin(magnitude_vel_sim_00125[-1, :, :]), np.nanmax(magnitude_vel_sim_00125[-1, :, :]), 100) , cmap="coolwarm") 
    # ax3.set_xlim(754360, 754856)
    # ax3.set_ylim(7088090, 7088290)

    fig, axes = plt.subplots(2,1)
    ax1, ax2 = axes
    ax1.imshow(background, extent=[np.nanmin(X), np.nanmax(X), np.nanmin(Y), np.nanmax(Y)])  
    ax1.set_xlim(754360, 754856)
    ax1.set_ylim(7088090, 7088290)
    c = ax1.contourf(X, Y, magnitude_vel_sim_00125[SIMULATION_INDEX, :, :], levels=np.linspace(0.001, np.nanmax(depth_averaged_velocity), 100), extend="max", cmap="coolwarm") 
    ax1.quiver(easting, northing, u_average, v_average, depth_averaged_velocity, cmap="coolwarm")
    ax1.set_xticks([])
    ax1.set_yticks([])
    ax1.set_title(r"$n=0.014$")
    
    ax2.imshow(background, extent=[np.nanmin(X), np.nanmax(X), np.nanmin(Y), np.nanmax(Y)])  
    ax2.set_xlim(754360, 754856)
    ax2.set_ylim(7088090, 7088290)
    c = ax2.contourf(X, Y, magnitude_vel_sim_005[SIMULATION_INDEX, :, :], levels=np.linspace(0.001, np.nanmax(depth_averaged_velocity), 100), extend="max", cmap="coolwarm") 
    ax2.quiver(easting, northing, u_average, v_average, depth_averaged_velocity, cmap="coolwarm")
    ax2.set_xticks([])
    ax2.set_yticks([])
    ax2.set_title(r"$n=0.05$")

    plt.figure()
    plt.plot(depth_averaged_velocity)

    plt.figure()
    plt.plot(u_measured[1651,:], depth[1651,:])
    plt.show()

def averaging(vel, depth):
    vel = np.ma.masked_array(vel,np.isnan(vel))
    depth = np.ma.masked_array(depth,np.isnan(depth))
    return np.trapz(vel, depth, axis=1) / np.max(depth, axis=1)

def project_spatial_coordinates(x, y):
    tm = projections.make_transverse_mercator("SWEREF_99_TM")
    northing = np.ndarray(len(x))
    easting = np.ndarray(len(y))
    for index, item in enumerate(x):
        northing[index], easting[index] = tm.geodetic_to_grid(x[index], y[index])
    return northing, easting

def read_adcp_measurements(path):
    """
    Reads the ascii data from adcp measurements and puts it in a pandas dataframe
    """
    return pd.read_csv(path, sep=",", names=list(range(115)), index_col=False)

def parse_mearuements(data):
    """
    
    """

    data = data.to_numpy()
    x_coord = np.ndarray(int(len(data)))
    y_coord = np.ndarray(int(len(data)))
    column_vel = np.ndarray(int(len(data)))
    u_vel = np.ndarray((int(len(data)), int(max(data[:,5]))))
    v_vel = np.ndarray((int(len(data)), int(max(data[:,5]))))
    z_vel = np.ndarray((int(len(data)), int(max(data[:,5]))))
    depth = np.ndarray((int(len(data)), int(max(data[:,5]))))
    total_depth = data[:, 1]

    for index in range(len(data)):
        number_of_cells = int(data[index, 5])
        start_index = 7
        try:
            u_vel[index,:number_of_cells] = data[index, start_index:start_index+number_of_cells]
            v_vel[index,:number_of_cells] = data[index, start_index+number_of_cells:start_index+number_of_cells*2]
            z_vel[index,:number_of_cells] = data[index, start_index+number_of_cells*2:start_index+number_of_cells*3]
            depth[index,:number_of_cells] = data[index, start_index+number_of_cells*3:start_index+number_of_cells*4]
            x_coord[index] = data[index, 2]
            y_coord[index] = data[index, 3]
            column_vel[index] = data[index, 4]

            u_vel[index, number_of_cells:] = np.nan
            v_vel[index, number_of_cells:] = np.nan
            z_vel[index, number_of_cells:] = np.nan
            depth[index, number_of_cells:] = np.nan
        except ValueError:
            pass

    # Remove -32768 occurences
    u_vel[u_vel==-32768] = np.nan
    v_vel[v_vel==-32768] = np.nan
    z_vel[z_vel==-32768] = np.nan
    depth[depth==-32768] = np.nan
    column_vel[column_vel==-32768] = np.nan

    # Remove bad data 1111 and 2354 raises valueerror, weird coordinates on 2353
    u_vel = np.delete(u_vel, (1111), axis=0)
    v_vel = np.delete(v_vel, (1111), axis=0)
    z_vel = np.delete(z_vel, (1111), axis=0)
    depth = np.delete(depth, (1111), axis=0)
    x_coord = np.delete(x_coord, (1111), axis=0)
    y_coord = np.delete(y_coord, (1111), axis=0)
    total_depth = np.delete(total_depth, (1111), axis=0)
    column_vel = np.delete(column_vel, (1111), axis=0)

    u_vel = np.delete(u_vel, (2353), axis=0)
    v_vel = np.delete(v_vel, (2353), axis=0)
    z_vel = np.delete(z_vel, (2353), axis=0)
    depth = np.delete(depth, (2353), axis=0)
    x_coord = np.delete(x_coord, (2353), axis=0)
    y_coord = np.delete(y_coord, (2353), axis=0)
    total_depth = np.delete(total_depth, (2353), axis=0)
    column_vel = np.delete(column_vel, (2353), axis=0)

    u_vel = np.delete(u_vel, (2354), axis=0)
    v_vel = np.delete(v_vel, (2354), axis=0)
    z_vel = np.delete(z_vel, (2354), axis=0)
    depth = np.delete(depth, (2354), axis=0)
    x_coord = np.delete(x_coord, (2354), axis=0)
    y_coord = np.delete(y_coord, (2354), axis=0)
    total_depth = np.delete(total_depth, (2354), axis=0)
    column_vel = np.delete(column_vel, (2354), axis=0)

    return u_vel, v_vel, z_vel, depth, x_coord, y_coord, total_depth, column_vel


if __name__ == "__main__":
    main()