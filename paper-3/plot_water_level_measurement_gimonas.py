# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import datetime
from datetime import timedelta
import os
from matplotlib import rc 
from matplotlib.colors import ListedColormap
from scipy.io import loadmat
from pandas.plotting import register_matplotlib_converters


def main():
    rc('text', usetex=True)
    register_matplotlib_converters()
    try:
        measured_data = np.genfromtxt("diver-data/ID02_5okt_Gimonas.csv", delimiter=',')
        measured_air_pressure = np.genfromtxt("diver-data/ID17_5okt_obbola_luft.csv", delimiter=',')
        holmsund_data = np.genfromtxt("holmsund_sea_water_level_may_oct_20.csv", delimiter=';')
    except FileNotFoundError:
        print("Water level measureuments file not found.")
        raise
    sea_level = holmsund_data[:,1]
    gimonas_ref = (-0.608)


    measurement_index = measured_data[:,0]
    first_day = datetime.datetime(2020, 5, 15, 8)
    measurement_time = [first_day + datetime.timedelta(minutes=10*x) for x in range(0, len(measurement_index))] # Date Time, GMT+02:00
    holmsund_time = [first_day + datetime.timedelta(minutes=1*x) for x in range(0, len(sea_level))]

    measurement_pressure = measured_data[:,2] # Abs Pres, kPa
    measurement_temperature = measured_data[:,3] # Temp, °C
    air_pressure = measured_air_pressure[:-3,2]

    trim = 35
    measurement_index = measurement_index[trim:-trim]
    measurement_time = measurement_time[trim:-trim]
    measurement_pressure = measurement_pressure[trim:-trim]
    measurement_temperature = measurement_temperature[trim:-trim]
    air_pressure = air_pressure[trim:-trim]

    # plt.figure()
    # plt.plot(measurement_time, air_pressure, 'rx')
    # plt.plot(measurement_time, measurement_pressure, 'bd')
    # plt.plot(measurement_time, measurement_pressure - air_pressure, 'o')
    # plt.legend(['Atmospheric pressure $[kPa]$', "Measured pressure $[kPa]$", "Water pressure $[kPa]$"])
    # plt.xlabel('Date')
    # plt.ylabel('Pressure $[kPa]$')

    # plt.figure()
    # plt.plot(measurement_time, (measurement_pressure - air_pressure) / (9.81*0.998))
    # plt.xlabel('Date')
    # plt.ylabel('Measured depth $[m]$')

    fig, (ax1, ax2) = plt.subplots(2,1, sharex=True)
    ax1.set_title("Gimonas measured water level")
    ax1.plot(measurement_time, gimonas_ref + (measurement_pressure - air_pressure) / (9.81*0.998))
    ax1.set_ylabel('Measured WSE $[MASL]$')
    ax2.set_title("Sea level Holmsund 15 May - 5 October")
    ax2.plot(holmsund_time, sea_level / 100)
    ax2.set_ylabel('Sea level $[MASL]$')
    ax2.set_xlabel('Date')

    plt.figure()
    plt.plot(measurement_time, gimonas_ref + (measurement_pressure - air_pressure) / (9.81*0.998))
    plt.plot(holmsund_time, sea_level / 100)
    plt.xlabel("Date")
    plt.ylabel("WSE")
    plt.legend(["Gimonas", "Holmsund"])

    # fig, (ax1, ax2) = plt.subplots(2,1, sharex=True)

    # # plt.figure()
    # ax1.plot(measurement_time, measurement_pressure)
    # # plt.xlabel('Date')
    # ax1.set_ylabel("Absolute pressure $[kPa]$")

    # # plt.figure()
    # ax2.plot(measurement_time, measurement_temperature)
    # ax2.set_xlabel('Date')
    # ax2.set_ylabel("Water temperature $[^\circ C]$")
    plt.show()


if __name__ == "__main__":
    main()