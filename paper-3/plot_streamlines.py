import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import os
import support_functions as sf
from matplotlib import rc 
import matplotlib.ticker as tick
from scipy.interpolate import griddata
PATH = "downstream-initial-data-second"
BACKGROUND_IMAGE = "background.tif"


def main():
    background = sf.read_background_image(BACKGROUND_IMAGE)
    X, Y, _ = sf.read_coordinate_data(PATH, "depth_averaged_velocity")
    velocity_x = sf.read_simulation_data(PATH, 'velocity_x')
    velocity_y = sf.read_simulation_data(PATH, 'velocity_y')


    fig, ax = plt.subplots(1,1)
    
    def streams(ax,xx,yy,u,v,base_map=False):
        x = np.linspace(xx.min(), xx.max(), 50)
        y = np.linspace(yy.min(), yy.max(), 50)

        xi, yi = np.meshgrid(x,y)

        #then, interpolate your data onto this grid:

        px = xx.flatten()
        py = yy.flatten()
        pu = u.flatten()
        pv = v.flatten()
        pspeed = speed.flatten()

        gu = griddata(zip(px,py), pu, (xi,yi))
        gv = griddata(zip(px,py), pv, (xi,yi))
        gspeed = griddata(zip(px,py), pspeed, (xi,yi))

        lw = 6*gspeed/np.nanmax(gspeed)
        #now, you can use x, y, gu, gv and gspeed in streamplot:

        if base_map:
            xx,yy = ax(xx,yy)
            xi,yi = ax(xi,yi)

        ax.contour(xx,yy,speed, colors='k', alpha=0.4)
        ax.plot(xx,yy,'-k',alpha=0.3)
        ax.plot(xx.T,yy.T,'-k',alpha=0.3)
        ax.plot(xi,yi,'-b',alpha=0.1)
        ax.plot(xi.T,yi.T,'-b',alpha=0.1)
        c = ax.streamplot(x,y,gu,gv, density=2,
            linewidth=lw, color=gspeed, cmap=plt.cm.jet)
    streams(ax, X, Y, velocity_x, velocity_y)
    # ax.imshow(background, extent=[np.nanmin(X), np.nanmax(X), np.nanmin(Y), np.nanmax(Y)])
    # ax.set_xticks([], [])
    # ax.set_yticks([], [])
    # ax.quiver([X, Y], velocity_x, velocity_y, color='r')    
    # c = ax.contourf(X, Y, velocity, levels=np.linspace(0.0001, np.nanmax(velocity), 50))
    # fig.colorbar(c, orientation='horizontal', aspect=50, format=tick.FormatStrFormatter('%.2f')).set_label(label=r"Depth Averaged Velocity $[m/s]$")

    # fig.colorbar(c,)
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("contours_q_1000.png", dpi=600, bbox_inches='tight', transparent=True)
    plt.show()
    


if __name__ == "__main__":
    main()