# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import datetime
import support_functions as sf
from datetime import timedelta
import os
from matplotlib import rc 
from matplotlib.colors import ListedColormap
from scipy.io import loadmat
from pandas.plotting import register_matplotlib_converters

DIVER_1 = "stornorrfors-divers/diver1_tegsbron.csv" # Tegsbron bästa
DIVER_2 = "stornorrfors-divers/diver2_tegsbron.csv" # Tegsbron alternativ
DIVER_3 = "stornorrfors-divers/diver3_smabatshamn.csv" # Småbåtshamnen
DIVER_4 = "stornorrfors-divers/diver4_sanddyna.csv" # Vid sanddynan som syns på kartan
DIVER_5 = "stornorrfors-divers/diver5_kyrkan.csv"
DIVER_6 = "stornorrfors-divers/diver6_grilludden.csv"
AIR_PRESSURE = "stornorrfors-divers/diver7_reference.csv"

GPS_ELEVATION = np.array([-0.11, -0.103, 0.313, 0.279, 0.968]) - 0.1#[0.1, 0.1, 0.1, 0.1, 0.1]
GPS_STICK_LENGTH = 1.33
def main():
    rc('text', usetex=True)
    elevation_gps = sf.stornorrfors_diver_elevations()

    register_matplotlib_converters()
    try:
        diver_1_data = np.genfromtxt(DIVER_1, delimiter=',')[:,4]
        diver_2_data = np.genfromtxt(DIVER_3, delimiter=',')[:,4]
        diver_3_data = np.genfromtxt(DIVER_4, delimiter=',')[:,4]
        diver_4_data = np.genfromtxt(DIVER_5, delimiter=',')[:,4]
        diver_5_data = np.genfromtxt(DIVER_6, delimiter=',')[:-1,4]

        holmsund_data = np.genfromtxt("holmsund-june-2021.csv", delimiter=';')
        discharge_data = np.genfromtxt("stornorrfors-discharge-21.csv", delimiter=',')
    except FileNotFoundError:
        print("Water level measureuments file not found.")
        raise
    sea_level = 0.01 * holmsund_data[:,1] # Data is in centimetres

    first_day = datetime.datetime(2021, 6, 8, 16)
    last_day = datetime.datetime(2021, 6, 11, 6)
    
    measurement_time = [first_day + datetime.timedelta(minutes=0.5*x) for x in range(0, len(diver_1_data))] # Date Time, GMT+02:00
    holmsund_time = pd.date_range(first_day, last_day, periods=len(sea_level)).tolist()
    discharge_time = [first_day + datetime.timedelta(minutes=x) for x in range(0, len(discharge_data[:,1]))]

    discharge = discharge_data[:,1] + discharge_data[:,2]

    fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5,1, sharex=True)
    ax5.plot(measurement_time, elevation_gps["5"] - GPS_STICK_LENGTH + diver_1_data, 'k')
    ax5.set_title("Tegsbron", fontsize=10)
    ax5.set_facecolor('lightgray')
    ax5.grid(True)
    ax4.plot(measurement_time, elevation_gps["4"] - GPS_STICK_LENGTH + diver_2_data, 'k')
    ax4.set_title("Hamnen", fontsize=10)
    ax4.set_facecolor('lightgray')
    ax4.grid(True)
  
    ax3.plot(measurement_time, elevation_gps["3"] - GPS_STICK_LENGTH + diver_3_data, 'k')
    ax3.set_title("Sanddynan")
    ax3.set_ylabel(r"MASL [$m$]", fontsize=14)
    ax3.yaxis.set_label_coords(-.1, .5)
    ax3.set_facecolor('lightgray')
    ax3.grid(True)

    ax2.plot(measurement_time, elevation_gps["2"] - GPS_STICK_LENGTH + diver_4_data, 'k')
    ax2.set_title("Kyrkan", fontsize=10)
    ax2.set_facecolor('lightgray')
    ax2.grid(True)
    
    ax1.plot(measurement_time, elevation_gps["1"] - GPS_STICK_LENGTH + diver_5_data, 'k')
    ax1.set_title("Grilludden", fontsize=10)
    ax1.set_facecolor('lightgray')
    ax1.grid(True)

    fig, (ax1, ax2) = plt.subplots(2,1, sharex=True)
    ax2.plot(holmsund_time, sea_level, 'k')
    ax2.set_ylabel("MASL $[m]$", fontsize=10)
    ax2.set_title("Holmsund Sea Level", fontsize=10)
    ax2.set_facecolor('lightgray')
    ax2.grid(True)
    ax1.plot(discharge_time, discharge, 'k')
    ax1.set_ylabel("Discharge $[m^3/s]$", fontsize=10)
    ax1.set_title("Stornorrfors Operating Conditions", fontsize=10)
    ax1.set_facecolor('lightgray')
    ax1.grid(True)
    plt.xlabel("Date $[min]$", fontsize=14)
    
    # fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(10,5))
    
    # # plt.plot(discharge_time, discharge_data[:,2])
    # # plt.plot(discharge_time, discharge_data[:,1])
    # ax.plot(discharge_time, discharge, 'k')
    # ax.grid(True)
    # ax.set_facecolor("lightgray")
    # # plt.legend(["Turbin", "Spill", "Totalt"])
    # ax.set_xlabel("Date", fontsize=16)
    # ax.set_ylabel("Discharge $[m^3/s]$", fontsize=16)
    # fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.5)
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    fig.savefig("operating-conditions.png", dpi=600, bbox_inches='tight', transparent=False)

    # plt.figure()
    # plt.plot(measurement_time,  (GPS_ELEVATION[0] + diver_1_data))
    # plt.show()


if __name__ == "__main__":
    main()