import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import os
import support_functions as sf
from matplotlib import rc 
import matplotlib.ticker as tick
from matplotlib.lines import Line2D
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar
import matplotlib.font_manager as fm

# PATH = "downstream-initial-data-second"
PATH = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/stornorrfors-downstream/simulations/fine-mesh-n-0.0125-water-level-bc/results"
BACKGROUND_IMAGE = "background_high_res.tif"


def main():
    rc('text', usetex=True)

    background = sf.read_background_image(BACKGROUND_IMAGE)
    X, Y, _ = sf.read_coordinate_data(PATH, f"magnitude")
    velocity = sf.read_simulation_data(PATH, f"magnitude")
    depth = sf.read_simulation_data(PATH, f"water-depth")
    bathymetry = sf.read_simulation_data(PATH, f"bathymetry")
    fontprops = fm.FontProperties(size=8)

    x_min = 754369
    x_max = 754856
    y_min = 7088090
    y_max = 7088290

    validation_points = sf.validation_points_gps()
    vel_plot = velocity[150, :, :]
    depth_plot = depth[150, :, :]

    plt.xticks([], [])
    plt.yticks([], [])
    plt.imshow(background, extent=[np.nanmin(X), np.nanmax(X), np.nanmin(Y), np.nanmax(Y)])
    
    c = plt.contourf(X, Y, bathymetry, levels=np.linspace(np.nanmin(bathymetry), np.nanmax(bathymetry), 100), cmap="coolwarm")
    cb = plt.colorbar(c, aspect=50, orientation="horizontal", format='%.1f')
    cb.set_label(label=r"MASL [$m$]", fontsize=12)
    plt1 = plt.scatter(validation_points["1"][1], validation_points["1"][0], c='black')
    plt.scatter(validation_points["2"][1], validation_points["2"][0], c='black')
    plt.scatter(validation_points["3"][1], validation_points["3"][0], c='black')
    plt.scatter(validation_points["4"][1], validation_points["4"][0], c='black')
    plt.scatter(validation_points["5"][1], validation_points["5"][0], c='black')
    ax = plt.gca()
    line_1 = Line2D([x_min, x_min], [y_min,y_max], color="r")
    line_2 = Line2D([x_max, x_max], [y_min,y_max], color="r")
    line_3 = Line2D([x_min, x_max], [y_min,y_min], color="r")
    line_4 = Line2D([x_min, x_max], [y_max,y_max], color="r")
    scalebar = AnchoredSizeBar(ax.transData,
                    1000, '1000 m', 'lower left', 
                    pad=0.5,
                    color='black',
                    frameon=True,
                    size_vertical=2,
                    fontproperties=fontprops,
                    )
    
    ax.add_line(line_1)
    ax.add_line(line_2)
    ax.add_line(line_3)
    ax.add_line(line_4)
    ax.add_artist(scalebar)
    plt.legend([plt1, line_1], ["Pressure loggers", "Validation area"], fontsize=10, loc='lower right', bbox_to_anchor=(1,1))
    plt.tight_layout()
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig("bathymetry.png", dpi=600, bbox_inches='tight', transparent=False)


    fig, axes = plt.subplots(2,1)
    (ax1, ax2) = axes
    ax1.imshow(background, extent=[np.nanmin(X), np.nanmax(X), np.nanmin(Y), np.nanmax(Y)])
    ax1.set_xticks([], [])
    ax1.set_yticks([], [])

    c = ax1.contourf(X, Y, vel_plot, levels=np.linspace(0.0001, 5, 50))
    fig.colorbar(c, orientation='horizontal', aspect=50, ax=ax1, format=tick.FormatStrFormatter('%.2f')).set_label(label=r"Depth Averaged Velocity $[m/s]$")
    ax1.scatter(validation_points["1"][1], validation_points["1"][0], c='red')
    ax1.scatter(validation_points["2"][1], validation_points["2"][0], c='red')
    ax1.scatter(validation_points["3"][1], validation_points["3"][0], c='red')
    ax1.scatter(validation_points["4"][1], validation_points["4"][0], c='red')
    ax1.scatter(validation_points["5"][1], validation_points["5"][0], c='red')


    ax2.imshow(background, extent=[np.nanmin(X), np.nanmax(X), np.nanmin(Y), np.nanmax(Y)])
    ax2.set_xticks([], [])
    ax2.set_yticks([], [])
    c = ax2.contourf(X, Y, depth_plot, levels=np.linspace(np.nanmin(depth_plot), np.nanmax(depth_plot), 50))
    fig.colorbar(c, orientation='horizontal', aspect=50, ax=ax2, format=tick.FormatStrFormatter('%.2f')).set_label(label=r"Water Depth $[m]$")
    
    ax2.scatter(validation_points["1"][1], validation_points["1"][0], c='red')
    ax2.scatter(validation_points["2"][1], validation_points["2"][0], c='red')
    ax2.scatter(validation_points["3"][1], validation_points["3"][0], c='red')
    ax2.scatter(validation_points["4"][1], validation_points["4"][0], c='red')
    ax2.scatter(validation_points["5"][1], validation_points["5"][0], c='red')

    # fig.colorbar(c,)
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("contours_q_900.png", dpi=600, bbox_inches='tight', transparent=False)
    # plt.show()
    


if __name__ == "__main__":
    main()