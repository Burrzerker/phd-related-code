import os
import scipy.io
import numpy as np
import matplotlib.image as mpimg
from matplotlib import pyplot as plt
import matplotlib.cm as cm
from animation_stornorrfors import animate_stornorrfors


PATH_TO_DATA_FOLDER = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/data"
BACKGROUND_IMAGE = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/geo_ref_image.png"
PATH_TO_AREA_FILE = "area.mat"

def area_contour(contour):
    x=contour.vertices[:,0]
    y=contour.vertices[:,1]
    area=0.5*np.sum(y[:-1]*np.diff(x) - x[:-1]*np.diff(y))
    area=np.abs(area)
    print(area)
    return area

def find_simulations(path):
    list_of_simulations = []
    for dir in os.listdir(path):
        list_of_simulations.append("{}/{}".format(path, dir))
    return list_of_simulations


def read_simulation_data(simulation):
    print("Reading simulation case: {}".format(simulation.split('/')[-1]))
    print(os.path.isfile("{}/water_level.mat".format(simulation)))
    data = scipy.io.loadmat("{}/water_level.mat".format(simulation))
    return data['data']['Val'][0][0], data['data']['X'][0][0], data['data']['Y'][0][0]


def normalize(data):
    data = np.transpose(data)
    results = np.ndarray(shape=data.shape)
    for index, row in enumerate(data):
        local_min = np.nanmin(row)
        local_max = np.nanmax(row)
        results[index, :] = (row - local_min) / (local_max - local_min)
    return np.transpose(results)
 
def main():       
    simulations = find_simulations(PATH_TO_DATA_FOLDER)
    data, X, Y = read_simulation_data(simulations[4])
    transient_area_1= scipy.io.loadmat('area.mat')['area_list']
    transient_area_2= scipy.io.loadmat('area_15.mat')['area_list']
    transient_area_3= scipy.io.loadmat('area_30.mat')['area_list']
    water_level_start = data[36,:,:]
    water_level_stop = data[42, :, :]
    water_level_temp = data[28, :, :]
    line = 100
    timesteps=181
    #area_list = transient_area(data)
    data = data[:timesteps, :, line]
    normalized_data = normalize(data)
    
    water_level_start[~np.isnan(water_level_start)] = 1
    water_level_stop[~np.isnan(water_level_stop)] = 1
    water_level_temp[~np.isnan(water_level_temp)] = 1
    x = X[:-1,:-1]
    y = Y[:-1,:-1]
    T = np.linspace(0, 181, 181)
    index = np.linspace(0, 2077,2077)
    x_temp = X[:-1,0]
    y_temp = Y[0,:-1]
    t, index_plot = np.meshgrid(T,index)
    index_plot = np.transpose(index_plot)
    t = np.transpose(t)
    plot_variable = np.nan_to_num(normalized_data)
    
    plot_variable_gradient = np.gradient(plot_variable)
    
    print("ADS")
#fig, ax = plt.subplots()
#contour = ax.contourf(index_plot[:timesteps, :], t[:timesteps, :], plot_variable, levels=50, cmap='jet')
##contour = ax.contourf(index_plot[:timesteps, :], t[:timesteps, :],velocity[:timesteps,: ,line], levels=100)
#bar = fig.colorbar(contour, orientation='horizontal', format='%.1f', pad=-0.4, shrink=0.75)
#bar.set_label(r'$\frac{WSE - WSE_{t=\infty}}{ WSE_{t=\infty} - WSE_{t=0}}$')
#plt.axis('scaled')
#plt.xlabel('Streamwise coordinate X [m]')
#plt.ylabel('Time in minutes')
#plt.title('Transient Response After a Change in Discharge From 250 $m^3/s$ to 21 $m^3/s$')
#plt.rcParams.update({'font.size': 16})
#mng = plt.get_current_fig_manager()
#mng.full_screen_toggle()
#plt.savefig("normalized_water_level_contour_30.png", dpi=600, bbox_inches='tight')

#fig = plt.figure()
#plt.plot(T, area_list)

#fig, ax = plt.subplots()
#contour = ax.contourf(index_plot[:timesteps, :], t[:timesteps, :], plot_variable_gradient[0], levels=np.linspace(-0.5, 0, 100), cmap='jet')
#bar = fig.colorbar(contour, orientation='horizontal', format='%.1f')
#bar.set_label('Gradient of Normalized WSE')
#plt.axis('scaled')
#plt.rcParams.update({'font.size': 22})

#fig, ax = plt.subplots()
#contour = ax.contourf(index_plot[:timesteps, :], t[:timesteps, :], plot_variable_gradient[1], levels=np.linspace(-1, 1, 50))
#bar = fig.colorbar(contour, orientation='horizontal', format='%.1f')
#bar.set_label('Gradient of Depth averaged velocity')
#plt.axis('scaled')

#plt.figure()
#line1, = plt.plot(T, transient_area_1 / 10**6)
#line2, = plt.plot(T, transient_area_2 /10**6)
#line3, = plt.plot(T, transient_area_3 /10**6)
#plt.xlabel('Time in minutes since change in discharge')
#plt.ylabel('Area of water surface in $km^2$')
#plt.legend([line1, line2, line3], ['5 minutes', '15 minutes', '30 minutes6'])
#plt.title('Change in Area of Water Surface as a Function of Time')
#mng = plt.get_current_fig_manager()
#mng.full_screen_toggle()
#plt.savefig("transient_area.png", dpi=600, bbox_inches='tight')
#animate_stornorrfors(x,y,velocity)

plt.show()