#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 10 10:58:08 2019

@author: anton
"""
import numpy as np
import scipy.io
import matplotlib.pyplot as plt

from delft_conference import read_simulation_data
PATH = "initial_bed_level"
#print(os.path.isfile(PATH))
#data = pd.read_csv(PATH)
#X = data.iloc[:, 0]
#Y = data.iloc[:, 1]
#elev = data.iloc[:, 2]
#elev.reshape()
#print(elev)
_, X, Y = read_simulation_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/data/250-21-5")
data = scipy.io.loadmat(PATH)['data']
val = -1*data['Val'][0][0]
#X = data['X'][0][0]
#Y = data['Y'][0][0]
x = X[:-2, :-2]
y = Y[:-2, :-2]
z = val[:-1, :-1]

#x = np.linspace(min(X[0,:]), max(X[0,:]), 2076)
#y = np.linspace(min(Y[:,0]), max(Y[:,0]), 216)
#y_plot, x_plot = np.meshgrid(y, x)

plt.figure()
plt.contourf(x, y, z)
plt.axis('equal')
plt.colorbar(orientation='horizontal', shrink=0.75, format='%.2f', aspect=25, pad=0.05).set_label(label='Manning coefficient',size=10,weight='normal')#, label='test', shrink=0.5, fontsize=24)
plt.axis('off')
#plt.title('Manning coefficient distribution')
#mng = plt.get_current_fig_manager()
#mng.full_screen_toggle()
#plt.savefig("manning.png", dpi=600, bbox_inches='tight')
plt.show()