#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 14:50:26 2019

@author: anton
"""
import scipy.io
import matplotlib.pyplot as plt
import numpy as np

transient_matrix = scipy.io.loadmat('area_matrix')['area_list']

#transient_area_0= scipy.io.loadmat('area_1.mat')['area_list']
#transient_area_1= scipy.io.loadmat('area.mat')['area_list']
#transient_area_2= scipy.io.loadmat('area_15.mat')['area_list']
#transient_area_3= scipy.io.loadmat('area_30.mat')['area_list']
#transient_area_4= scipy.io.loadmat('area_45.mat')['area_list']
#transient_area_5= scipy.io.loadmat('area_60.mat')['area_list']
#transient_area_calibration = scipy.io.loadmat('area_1_calibration.mat')['area_list']
T = np.linspace(0, 241, 241)
size = 4

plt.figure()
line0, = plt.plot(T, transient_matrix[:,0] / 10**6, 'v', markersize=size)
line1, = plt.plot(T, transient_matrix[:,1] / 10**6, '^', markersize=size)
line2, = plt.plot(T, transient_matrix[:,2] /10**6, '<', markersize=size)
line3, = plt.plot(T, transient_matrix[:,3] /10**6, 's', markersize=size)
line4, = plt.plot(T, transient_matrix[:,4] /10**6, '+', markersize=size)
line5, = plt.plot(T, transient_matrix[:,5] /10**6, 'o', markersize=size)
line6, = plt.plot(T, transient_matrix[:,6] /10**6, 'x', markersize=size)
line7, = plt.plot(T, transient_matrix[:,7] /10**6, 'd', markersize=size)
plt.xlabel('Time in minutes since change in discharge')
plt.ylabel('Wetted area in $km^2$')
plt.legend([line0, line1, line2, line3, line4, line5, line6, line7], ['1 minute', '5 minutes', '15 minutes', '30 minutes', '35 minutes', '40 minutes', '45 minutes', '60 minutes'])
plt.title('Change in Wetted Area as a Function of Time')
# mng = plt.get_current_fig_manager()
# mng.full_screen_toggle()
# plt.savefig('/home/anton/phd-related-text/conference-delft/paper/1-column-A4-Full Paper/figures/transient_area_new.png', dpi=600, bbox_inches='tight')


plt.figure()
line0, = plt.plot(T, transient_matrix[:,0] / 10**6, 'v', markersize=size)
line1, = plt.plot(T, transient_matrix[:,1] / 10**6, '^', markersize=size)
line2, = plt.plot(T, transient_matrix[:,2] /10**6, '<', markersize=size)
line3, = plt.plot(T, transient_matrix[:,3] /10**6, 's', markersize=size)
line4, = plt.plot(T, transient_matrix[:,4] /10**6, '+', markersize=size)
line5, = plt.plot(T, transient_matrix[:,5] /10**6, 'o', markersize=size)

plt.figure()

line4, = plt.plot(T, transient_matrix[:,4] /10**6, '+', markersize=size)
line5, = plt.plot(T, transient_matrix[:,5] /10**6, 'o', markersize=size)
line6, = plt.plot(T, transient_matrix[:,6] /10**6, 'x', markersize=size)
line7, = plt.plot(T, transient_matrix[:,7] /10**6, 'd', markersize=size)
#plt.figure()
#line1, = plt.plot(T, transient_area_0 /10**6, 'o', markersize=4)
#line2, = plt.plot(T, transient_area_calibration /10**6, 'x', markersize=4)
#plt.xlabel('Time in minutes since change in discharge')
#plt.ylabel('Wetted area in $km^2$')
#plt.legend([line1, line2], ['Uniform Manning', 'Manning distribution'])
#plt.title('Change in Wetted Area as a Function of Time')
#mng = plt.get_current_fig_manager()
#mng.full_screen_toggle()
#plt.savefig('/home/anton/phd-related-text/conference-delft/paper/1-column-A4-Full Paper/figures/comparison_transient_area.png', dpi=600, bbox_inches='tight')

plt.show()