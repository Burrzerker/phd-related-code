#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 28 12:25:24 2019

@author: anton
"""
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np

BACKGROUND_IMAGE = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/geo_ref_image.png"

def animate_stornorrfors(x, y, data):
    img = mpimg.imread(BACKGROUND_IMAGE)

    fig,ax = plt.subplots()
    cs = ax.contourf(x, y, data[0,:,:], levels=np.linspace(0.0005, 5, 100))
    bar = fig.colorbar(cs, orientation='vertical', format='%.1f')
    bar.set_label('Magnitude of Depth Averaged Velocity $[m/s]$')
    crop_img = crop_background_image(img)
    def animate(i):
       print(i) 
       ax.clear()
       ax.imshow(crop_img, extent=[np.min(x), np.max(x), np.min(y), np.max(y)], origin='upper', aspect='auto')
       ax.contourf(x, y, data[i,:,:], levels=np.linspace(0.0005, 5, 100))
       ax.set_title('${}$ minutes after change from $250~m^3/s$ to $21~m^3/s$'.format(i)) 
       plt.xticks([])
       plt.yticks([])
  
    interval = 0.1#in seconds     
    print("ASD")
    ani = animation.FuncAnimation(fig, animate, 241, interval=interval, blit=False)
#    Writer = animation.writers['ffmpeg']
#    writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=6000)
#    ani.save('250_21_viridis.mp4', writer=writer, dpi=300)
    plt.show()
    
def crop_background_image(img):
    crop = img[340:1415, 955:1985,:]
    return crop