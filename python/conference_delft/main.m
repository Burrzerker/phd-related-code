clear 
data1 = load('/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/christmas_simulations/data/250-21-1/water_level.mat');
data2 = load('/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/christmas_simulations/data/250-21-5/water_level.mat');
data3 = load('/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/christmas_simulations/data/250-21-15/water_level.mat');
data4 = load('/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/christmas_simulations/data/250-21-30/water_level.mat');
data5 = load('/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/christmas_simulations/data/250-21-35/water_level.mat');
data6 = load('/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/christmas_simulations/data/250-21-40/water_level.mat');
data7 = load('/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/christmas_simulations/data/250-21-45/water_level.mat');
data8 = load('/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/christmas_simulations/data/250-21-60/water_level.mat');

disp("loaded data")
X = data1.data.X;
Y = data1.data.Y;
X_new = X(1:end-1,1:end-1) -X(1,1);
Y_new = Y(1:end-1,1:end-1) - Y(1,1);
variable1 = data1.data.Val;
area_list1 = zeros(241,1);

for i = 1:241
    temp = variable1(i,:,:);
    temp = squeeze(temp);
    [C, h] = contourf(X_new, Y_new, temp, 500);
    area_list1(i) = polyarea(C(1,2:end), C(2,2:end));
    i
end

variable2 = data2.data.Val;
area_list2 = zeros(241,1);
for i = 1:241
    temp = variable2(i,:,:);
    temp = squeeze(temp);
    [C, h] = contourf(X_new, Y_new, temp, 500);
    area_list2(i) = polyarea(C(1,2:end), C(2,2:end));
    i
end

variable3 = data3.data.Val;
area_list3 = zeros(241,1);
for i = 1:241
    temp = variable3(i,:,:);
    temp = squeeze(temp);
    [C, h] = contourf(X_new, Y_new, temp, 500);
    area_list3(i) = polyarea(C(1,2:end), C(2,2:end));
    i
end

variable4 = data4.data.Val;
area_list4 = zeros(241,1);
for i = 1:241
    temp = variable4(i,:,:);
    temp = squeeze(temp);
    [C, h] = contourf(X_new, Y_new, temp, 500);
    area_list4(i) = polyarea(C(1,2:end), C(2,2:end));
    i
end

variable5 = data5.data.Val;
area_list5 = zeros(241,1);
for i = 1:241
    temp = variable5(i,:,:);
    temp = squeeze(temp);
    [C, h] = contourf(X_new, Y_new, temp, 500);
    area_list5(i) = polyarea(C(1,2:end), C(2,2:end));
    i
end

variable6 = data6.data.Val;
area_list6 = zeros(241,1);
for i = 1:241
    temp = variable6(i,:,:);
    temp = squeeze(temp);
    [C, h] = contourf(X_new, Y_new, temp, 500);
    area_list6(i) = polyarea(C(1,2:end), C(2,2:end));
    i
end

variable7 = data7.data.Val;
area_list7 = zeros(241,1);
for i = 1:241
    temp = variable7(i,:,:);
    temp = squeeze(temp);
    [C, h] = contourf(X_new, Y_new, temp, 500);
    area_list7(i) = polyarea(C(1,2:end), C(2,2:end));
    i
end

variable8 = data8.data.Val;
area_list8 = zeros(241,1);
for i = 1:241
    temp = variable8(i,:,:);
    temp = squeeze(temp);
    [C, h] = contourf(X_new, Y_new, temp, 500);
    area_list8(i) = polyarea(C(1,2:end), C(2,2:end));
    i
end

area_list = zeros(241,8);
area_list(:,1) = area_list1;
area_list(:,2) = area_list2;
area_list(:,3) = area_list3;
area_list(:,4) = area_list4;
area_list(:,5) = area_list5;
area_list(:,6) = area_list6;
area_list(:,7) = area_list7;
area_list(:,8) = area_list8;