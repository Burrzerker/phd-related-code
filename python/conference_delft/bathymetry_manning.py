#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 12 09:43:54 2019

@author: anton
"""

import numpy as np
import scipy.io
import matplotlib.pyplot as plt
from matplotlib import ticker

from delft_conference import read_simulation_data
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams.update({'font.size': 12})
PATH_MANNING = "initial_bed_level"
PATH_BATHYMETRY = "bathymetry"
_, X, Y = read_simulation_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/data/250-21-5")
data = scipy.io.loadmat(PATH_MANNING)['data']
val = -1*data['Val'][0][0]

bath = scipy.io.loadmat(PATH_BATHYMETRY)['data']
bathymetry = bath['Val'][0][0]
bathymetry = bathymetry[:-1, :-1]

x = X[:-2, :-2]
y = Y[:-2, :-2]
z = val[:-1, :-1]

fig, axes = plt.subplots(1,2)
ax1, ax2 = axes
c1 = ax1.contourf(x, y, z)
ax1.axis('equal')
ax1.axis('off')
c2 = ax2.contourf(x, y, bathymetry)
ax2.axis('equal')
ax2.axis('off')
#cbar = plt.colorbar(c1, ax=ax1,orientation='horizontal', shrink=1, format='%.2f', aspect=25, pad=0.05).set_label(label='Manning coefficient',size=10,weight='normal')
fig.colorbar(c1, ax=ax1,orientation='horizontal', shrink=1, aspect=25, pad=0.05,
             ticks=[0.03, 0.05, 0.07, 0.09, 0.11], format='%.2f').set_label(label=r'Manning coefficient [s/m$^{1/3}$]',size=10,weight='normal')
#cbar.ax1.locator_params(nbins=5)
#tick_locator = ticker.MaxNLocator(nbins=5)
#cbar.locator = tick_locator
#cbar.update_ticks()
fig.colorbar(c2, ax=ax2,orientation='horizontal', shrink=1, format='%.0f', aspect=25, pad=0.05).set_label(label=r'Bathymetry [MASL]',size=10,weight='normal')
mng = plt.get_current_fig_manager()
mng.full_screen_toggle()
plt.savefig('test.png', dpi=600, bbox_inches='tight')
#plt.figure()
#plt.contourf(x, y, z)
#plt.axis('equal')
#plt.colorbar(orientation='horizontal', shrink=0.75, format='%.2f', aspect=25, pad=0.05).set_label(label='Manning coefficient',size=10,weight='normal')#, label='test', shrink=0.5, fontsize=24)
#plt.axis('off')
#
#
#plt.figure()
#plt.contourf(x, y, bathymetry)
#plt.axis('equal')
#plt.colorbar(orientation='horizontal', shrink=0.75, format='%.2f', aspect=25, pad=0.05).set_label(label='Manning coefficient',size=10,weight='normal')#, label='test', shrink=0.5, fontsize=24)
#plt.axis('off')
#plt.title('Manning coefficient distribution')
#mng = plt.get_current_fig_manager()
#mng.full_screen_toggle()
#plt.savefig("manning.png", dpi=600, bbox_inches='tight')
plt.show()