import datetime
import os
import requests
import subprocess
import yaml

TELEGRAM_TOKEN = "694726905:AAF6QEp3SMH8Uz2XMP_8bn8BJxtjQn8xAFI"
CHAT_ID = "79909198"
TELEGRAM_URL = "https://api.telegram.org/bot{}/sendMessage?chat_id={}&parse_mode=Markdown".format(TELEGRAM_TOKEN, CHAT_ID)
PATH_TO_SIMULATIONS_CONFIG_FILE = "config_files/simulations.yaml"
PATH_TO_DELFT_EXECUTION_PARALLELL_EXECUTION_SCRIPT = "/home/anton/phd-related-code/bash/run_delft3d_script_parallel.sh"
NUMBER_OF_CORES = 24


def main():
    cases = get_simulation_cases(PATH_TO_SIMULATIONS_CONFIG_FILE)

    for case in cases:
        try:
            run_simulation(case)
        except KeyboardInterrupt:
            print("Simulations manually interrupted!")
            sys.exit()
    notify_telegram_bot()


def get_simulation_cases(path_to_config):
    with open(path_to_config, 'r') as config_file:
        read_data = yaml.safe_load(config_file)
    cases = read_data['cases'].values()
    return cases


def run_simulation(case):
    print("RUNNING CASE: {}".format(case))
    try:
        os.system("{0} {1} {2}".format(PATH_TO_DELFT_EXECUTION_PARALLELL_EXECUTION_SCRIPT, case, NUMBER_OF_CORES))
    except:
        pass


def notify_telegram_bot():
    current_time = datetime.datetime.now().strftime('%Y%m%d - %H: %M')
    header = {'Content-type': 'application/json'}

    payload = "*Simulation Finished {}*".format(current_time)

    url = "{}&text={}".format(TELEGRAM_URL, payload)
    r = requests.post(url, headers=header, json=payload)


if __name__=='__main__':
    main()
