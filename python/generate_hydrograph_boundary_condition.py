# -*- coding: utf-8 -*-
import numpy as np
import os


PATH_TO_DISCHARGE_DATA = "/home/anton/phd-related-code/paper-3/stornorrfors-discharge-21.csv"
DIVER_1 = "/home/anton/phd-related-code/paper-3/stornorrfors-divers/diver1_tegsbron.csv"
BOUNDARY_CONDITIONS_FILE = "boundary_conditions.bct"
GPS_STICK_LENGTH = 1.33
ELEVATION_TEGSBRON = -0.11 

def main():
    discharge_data = np.genfromtxt(PATH_TO_DISCHARGE_DATA, delimiter=',')
    discharge = discharge_data[:,1] + discharge_data[:,2]
    water_level = np.genfromtxt(DIVER_1, delimiter=',')[:,4]

    try:
        os.remove(BOUNDARY_CONDITIONS_FILE)
    except FileNotFoundError:
        pass
    start_minute = 960
    with open(BOUNDARY_CONDITIONS_FILE, 'w') as opened_file:
        opened_file.write(first_string(len(discharge)))
        for index, item in enumerate(discharge):
            opened_file.write(f"  {start_minute + index}  {item}  {item}\n")
        opened_file.write(water_level_boundary(len(discharge)))
        for index, item in enumerate(water_level[::2]):
            water_level = -0.1 + ELEVATION_TEGSBRON - GPS_STICK_LENGTH + item
            opened_file.write(f"  {start_minute + index}  {water_level}  {water_level}\n")        
        # opened_file.write(second_string())


def first_string(records_in_table):
    return f"""table-name           'Boundary Section : 1'
contents             'Uniform             '
location             'Inlet               '
time-function        'non-equidistant'
reference-time       20210608
time-unit            'minutes'
interpolation        'linear'
parameter            'time                '                     unit '[min]'
parameter            'total discharge (t)  end A'               unit '[m3/s]'
parameter            'total discharge (t)  end B'               unit '[m3/s]'
records-in-table     {records_in_table}
"""

def second_string():
    return """table-name           'Boundary Section : 2'
contents             'Uniform             '
location             'Outlet              '
time-function        'non-equidistant'
reference-time       20210608
time-unit            'minutes'
interpolation        'linear'
parameter            'time                '                     unit '[min]'
parameter            'neumann         (n)  end A'               unit '[-]'
parameter            'neumann         (n)  end B'               unit '[-]'
records-in-table     2
960 -0.01 -0.01
4680 -0.01 -0.01
"""

def water_level_boundary(records_in_table):
    return f"""table-name           'Boundary Section : 2'
contents             'Uniform             '
location             'Outlet              '
time-function        'non-equidistant'
reference-time       20210608
time-unit            'minutes'
interpolation        'linear'
parameter            'time                '                     unit '[min]'
parameter            'water elevation (z)  end A'               unit '[m]'
parameter            'water elevation (z)  end B'               unit '[m]'
records-in-table     {records_in_table}
"""

if __name__ == "__main__":
    main()
