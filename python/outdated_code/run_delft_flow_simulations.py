"""
Script that runs all Deflt3D-FLOW simulations in the current folder.
"""

import argparse
import datetime
import json
import os
import pprint
import subprocess
import sys
import requests

from dataclasses import dataclass

from multiprocessing import Pool

# Path to the FLOW executable.
PATH_TO_EXECUTABLE = "C:\\PROGRA~1\\Deltares\\Delft3D 4.03.01\\win64\\flow2d3d\\bin\\d_hydro.exe"
TELEGRAM_TOKEN = "694726905:AAF6QEp3SMH8Uz2XMP_8bn8BJxtjQn8xAFI"
CHAT_ID = "79909198"
TELEGRAM_URL = "https://api.telegram.org/bot{}/sendMessage?chat_id={}&parse_mode=Markdown".format(TELEGRAM_TOKEN, CHAT_ID)

@dataclass
class SimulationResults:
    """
    DataClass for simulation results
    """
    failed_simulations = 0
    successful_simulations = 0
    list_of_failed_simulations = []
    list_of_successful_simulations = []

    def add_failed_simulation(self, case):
        """
        Add simulation result to class.
        :param case: Simulation case that was run.
        """
        self.failed_simulations += 1
        self.list_of_failed_simulations.append(case)

    def add_successful_simulation(self, case):
        """
        Add simulation result to class.
        :param case: Simulation case that was run.
        """
        self.successful_simulations += 1
        self.list_of_successful_simulations.append(case)


def main():
    """
    Main method
    """
    args = get_arguments()
    # Create an instance of SimulationResults class
    global results
    results = SimulationResults()

    list_of_simulations = find_simulation_cases(args.path)

    pool = Pool(args.number_of_processes)

    try:
        pool.map(run_delft3d, list_of_simulations)
    except KeyboardInterrupt:
        raise

    post_http_query()

def find_simulation_cases(dirpath):
    """
    Recursively find all simulation cases in the simulation folder.
    :param dirpath: Path to directory containing simulation directories.
    :return list_of_simulations: List containing the path to .xml files representing a simulation case
    """
    list_of_simulations = []
    for dirpath, dir_names, file_names in os.walk(dirpath):
        for f in file_names:
            if f.endswith('config_flow2d3d.xml'):
                list_of_simulations.append(os.path.join(dirpath, f))

    print("FOUND {} CASES!".format(len(list_of_simulations)))
    for number, case in enumerate(list_of_simulations):
        print("Case {0}, Path: {1}".format(number, case))

    return list_of_simulations


def get_arguments():
    """
    Parse CLI arguments.
    :return: Namespace containing CLI arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-p', '--path',
         help='Absolute path to directory containing Delft3D Flow simulations.',
         default=os.path.dirname(os.path.abspath(__file__))
    )
    parser.add_argument(
        '-n', '--number_of_processes',
        help='Number of processes to spawn.',
        type=int, default=2
    )
    return parser.parse_args()


def run_delft3d(case):
    """
    Execute the Delft3D-FLOW executable with correct simulation case
    :param case: Path to .xml describing a simulation setup.
    :param results: SimulationResults object that should be populated with results
    """
    # Each simulation has to be run from it's own directory.
    simulation_dir, xml_file = os.path.split(case)
    current_dir = os.getcwd()
    try:
        os.chdir(simulation_dir)
    except FileNotFoundError:
        print("The directory containing simulations does not exist!")
        sys.exit(1)
    print('-' * 50)
    print("SIMULATION STARTED, RUNNING IN DIRECTORY: {}".format(simulation_dir))
    print('-' * 50)
    try:
        subprocess.run("{} {}".format(PATH_TO_EXECUTABLE, xml_file), check=True)
        print("temp")
    except subprocess.CalledProcessError:
        pass

    print('-' * 50)
    print("SIMULATION ENDED")
    print('-' * 50)
    os.chdir(current_dir)


def post_http_query():
    current_time = datetime.datetime.now().strftime('%Y%m%d - %H: %M')
    header = {'Content-type': 'application/json'}

    payload = "*Simulation Finished {}*".format(current_time)

    url = "{}&text={}".format(TELEGRAM_URL, payload)
    r = requests.post(url, headers=header, json=payload)


if __name__ == '__main__':
    main()
