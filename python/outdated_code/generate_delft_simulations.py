"""
Generates different simulation cases for Delft3D-FLOW

-------------
CHANGELOG
-------------

Version 1.0:
    - Generate n number of simulations with Neumann conditions, input is:
        * n, the number of simulations that should be generated
        * range, the range of gradients on the water level at the downstream boundary
    - Version 1.0 uses a template of a manually created Delft3D simulation to generate new cases.

To be implemented:
    - Be able to change initial value from command line
    - Be able to give time series as input
"""

import argparse
import numpy as np
import os
import shutil


def main():
    """
    Main method.
    """
    args = get_arguments()
    set_global_variables(args)

    list_of_gradients = np.linspace(args.range[0], args.range[1], args.number)
    for case in list_of_gradients:
        generate_case(case)


def set_global_variables(args):
    """
    Set global variables
    :param args: Parsed CLI arguments.
    """
    global simulation_folder_name
    global template_simulation_folder
    simulation_folder_name = args.name
    template_simulation_folder = args.template


def generate_case(case):
    """
    Generates new cases
    :param case: Gradient of water level at outlet boundary for this case
    """
    case_name = copy_and_rename_simulation_case(case)
    current_dir = os.getcwd()
    os.chdir(case_name)
    change_boundary_condition(case)
    os.chdir(current_dir)


def change_boundary_condition(case):
    """
    Opens template.bct file and changes table containing time series values for water level gradient
    :param case: Gradient of water level at outlet boundary for this case
    """
    with open('template.bct', 'r+') as bc_file:
        contents = bc_file.readlines()

    temp1 = '{} {}  {}'.format(contents[-2][:16], case, case)
    temp2 = '{} {}  {}'.format(contents[-1][:16], case, case)

    contents = contents[:-2]
    contents.append(temp1 + '\n')
    contents.append(temp2)

    with open('template.bct', 'w') as bc_file:
        for line in contents:
            bc_file.write(line)


def copy_and_rename_simulation_case(case):
    """
    Copies and renames the template folder
    :param case: Gradient of water level at outlet boundary for this case, used to identify which case has been created
    """
    case_name = '{}_{}'.format(simulation_folder_name, case)
    try:
        shutil.copytree(template_simulation_folder, case_name)
    except FileExistsError:
        print('A case already exists with this gradient!')
        pass

    return case_name


def get_arguments():
    """
    Parse CLI arguments.
    :return: Namespace containing CLI arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-n', '--number', type=int,
        help='Number of simulations that should be generated.'
    )
    parser.add_argument(
        '-r', '--range', nargs=2, type=float,
        help='Range of water gradients that should be investigated.'
    )
    parser.add_argument(
        '-t', '--template',
        help='Path to template simulation folder.'
    )
    parser.add_argument(
        '--name', default='template',
        help='Optional name for simulation folders.'
    )
    return parser.parse_args()


if __name__ == '__main__':
    main()
