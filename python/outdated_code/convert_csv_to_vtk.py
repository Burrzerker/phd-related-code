"""
WORK IN PROGRES
.vtk files are required in order to post-process in Paraview!!
File that converts .csv to .vtk file
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas

from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata


PATH_TO_BATHYMETRY_FILE = 'E:\\Code\\Python\\phd-related-code\\clean_bathymetry_data.csv'

def main():
    topography = get_bathymetry_data()
    elevation, easting, northing = setup_grid_for_plot(topography) # Interpolated topography values
    plot_data(elevation, easting, northing)

    plt.contour(X_grid, Y_grid, Z_grid)
    plt.show()


def setup_grid_for_plot(topography):
    matrix = topography.values

    n = 5 # Sampling frequency
    easting = matrix[:,1][::n]
    northing = matrix[:,2][::n]
    elevation = matrix[:,3][::n]

    length = len(easting)

    points = np.ndarray(shape=(length, 2), dtype=float, order='F')
    points[:, 0] = easting
    points[:, 1] = northing

    X, Y = np.meshgrid(easting, northing)
    interpolated_elevation = griddata(points, elevation, (X, Y), method='linear')
    return interpolated_elevation, X, Y


def get_bathymetry_data():
    return pandas.read_csv(PATH_TO_BATHYMETRY_FILE)


if __name__ == '__main__':
    main()
