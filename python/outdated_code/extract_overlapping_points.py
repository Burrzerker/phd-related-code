import argparse
import os
import pandas

import numpy as np


def main():
    args = get_arguments()
    set_global_variables(args)
    first_file, second_file = read_geometry_data()
    extract_coordinates_from_files(first_file, second_file)


def extract_coordinates_from_files(first_file, second_file):
    """
    A function that extracts the highest X-value from one file and the highest Y-value from the other.
    These coordinates represents the corners of a square defining the overlap region.
    :param first_file: Corresponds to Old River Bed geometry
    :param second_file: Corresponds to Tunnelviken geometry
    """
    max_x_coordinate, min_y_coordinate = old_river_bed_coordinates(first_file)
    min_x_coordinate, max_y_coordinate = tunnelviken_coordinates(second_file)
    print("Coordinates old river bed: X: {}, Y: {}".format(max_x_coordinate, min_y_coordinate))
    print("Coordinates tunnelviken: X: {}, Y: {}".format(min_x_coordinate, max_y_coordinate))

    local_min_x_coordinate = extract_tunnelviken_overlap_region(max_x_coordinate, min_y_coordinate, second_file)
    extract_old_river_overlap_region(local_min_x_coordinate, max_y_coordinate, first_file)


def extract_old_river_overlap_region(min_x_coordinate, max_y_coordinate, old_river):
    """
    Create .csv file that countains the overlap geometry for old river
    :param min_x_coordinate: X coordinate of region
    :param max_y_coordinate: Y coordiante of region
    :param old_river: DataFrame with bathymetry
    """
    x_coordinates = old_river[old_river.columns[0]]
    y_coordinates = old_river[old_river.columns[1]]
    depth = old_river[old_river.columns[2]]

    filtered_x_coordinates = []
    filtered_y_coordinates = []
    filtered_depth = []

    for index, number in enumerate(y_coordinates):
        if y_coordinates[index] < max_y_coordinate and x_coordinates[index] > min_x_coordinate:
            filtered_x_coordinates.append(x_coordinates[index])
            filtered_y_coordinates.append(y_coordinates[index])
            filtered_depth.append(depth[index])
    output = np.ndarray(shape=(len(filtered_depth),3))
    output[:,0] = filtered_x_coordinates
    output[:,1] = filtered_y_coordinates
    output[:,2] = filtered_depth
    output_csv = pandas.DataFrame(output)
    with open('old_river_overlap.csv', 'w') as f:
        output_csv.to_csv(f, header=['X', 'Y', 'Depth'], index=None)

def extract_tunnelviken_overlap_region(max_x_coordinate, min_y_coordinate, tunnelviken):
    """
    Create .csv file that countains the overlap geometry for tunnelviken
    :param max_x_coordinate: X coordinate of region
    :param min_y_coordinate: Y coordiante of region
    :param tunnelviken: DataFrame with bathymetry
    """
    x_coordinates = tunnelviken[tunnelviken.columns[0]]
    y_coordinates = tunnelviken[tunnelviken.columns[1]]
    depth = tunnelviken[tunnelviken.columns[2]]

    filtered_x_coordinates = []
    filtered_y_coordinates = []
    filtered_depth = []

    for index, number in enumerate(y_coordinates):
        if y_coordinates[index] > min_y_coordinate and x_coordinates[index] < max_x_coordinate:
            filtered_x_coordinates.append(x_coordinates[index])
            filtered_y_coordinates.append(y_coordinates[index])
            filtered_depth.append(depth[index])

    local_min_x = min(filtered_x_coordinates)
    print("Max X value: {}, Local min X: {}".format(max_x_coordinate, local_min_x))

    output = np.ndarray(shape=(len(filtered_depth),3))
    output[:,0] = filtered_x_coordinates
    output[:,1] = filtered_y_coordinates
    output[:,2] = filtered_depth
    output_csv = pandas.DataFrame(output)
    with open('tunnelviken_overlap.csv', 'w') as f:
        output_csv.to_csv(f, header=['X', 'Y', 'Depth'], index=None)
    return local_min_x



def tunnelviken_coordinates(tunnelviken):
    """
    Extracts the coordinates of the data point with min X value and max Y value
    :param old_river_bed: Namespace containing bathymetric data
    :return min_x_coordinate:
    :return max_y_coordinate:
    """
    x_coordinates = tunnelviken[tunnelviken.columns[0]]
    y_coordinates = tunnelviken[tunnelviken.columns[1]]
    max_y_coordinate = max(y_coordinates)

    list_of_x_coordinates = []

    for index, number in enumerate(y_coordinates) :
        if number == max_y_coordinate:
            list_of_x_coordinates.append(x_coordinates[index])

    min_x_coordinate = min(list_of_x_coordinates)

    return min_x_coordinate, max_y_coordinate


def old_river_bed_coordinates(old_river_bed):
    """
    Extracts the coordiantes of the data point with max X value and min Y value
    :param old_river_bed: Namespace containing bathymetric data
    :return max_x_coordinate:
    :return min_y_coordinate:
    """
    x_coordinates = old_river_bed[old_river_bed.columns[0]]
    y_coordinates = old_river_bed[old_river_bed.columns[1]]
    max_x_coordinate = max(x_coordinates)

    list_of_y_coordinates = []

    for index, number in enumerate(x_coordinates) :
        if number == max_x_coordinate:
            list_of_y_coordinates.append(y_coordinates[index])

    min_y_coordinate = min(list_of_y_coordinates)

    return max_x_coordinate, min_y_coordinate

def read_geometry_data():
    """
    Reads two .csv files into pandas DataFrame type
    :return: first_file: first .csv file as DataFrame
    :return: second_file: second .csv file as DataFrame
    """

    with open(first_overlapping_file, 'r'):
        first_file = pandas.read_csv(first_overlapping_file)

    with open(second_overlapping_file, 'r'):
        second_file = pandas.read_csv(second_overlapping_file)
    return first_file, second_file

def set_global_variables(args):
    """
    Set global variables
    :param args: Parsed CLI arguments.
    """
    global first_overlapping_file
    global second_overlapping_file

    first_overlapping_file = args.path[0]
    second_overlapping_file = args.path[1]


def get_arguments():
    """
    Parse CLI arguments.
    :return: Namespace containing CLI arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-p', '--path', nargs=2, type=str,
         help='Absolute path to two .csv files containing overlapping sample data.',
    )
    return parser.parse_args()


if __name__ == '__main__':
    main()
