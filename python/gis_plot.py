import numpy as np
import matplotlib.pyplot as plt

VERTICES = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/padjelanta-gis/vertices.csv"
ELEVATION = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/padjelanta-gis/elevation.csv"
STUGOR_METER = np.array([1524, 14534, 36447, 48913, 58729, 76606, 86349, 100845, 118613]) / 1000 # meter från båten, Akkastugorna, gisuris, låddejåhkå
STUGOR_NAMN = ["Akkastugorna", "Gisuris", "Låddejåhkå", "Arasluokta", "Staloluokta", "Duottar", "Darreluoppal", "Såmmarlappstugan", "Njunjessstugan"]

def main():
    vertices = np.genfromtxt(VERTICES, delimiter=',')
    elevation = np.genfromtxt(ELEVATION)

    length = vertices[:, 4] / 1000

    plt.figure()
    plt.plot(length, elevation)
    plt.xlabel("Distance [km]")
    plt.ylabel("Elevation [m]")
    
    for xc in STUGOR_METER:
        plt.axvline(x=xc, color="r", linestyle="dashed")
    plt.text(STUGOR_METER[0] - 5, 1000, STUGOR_NAMN[0])
    plt.text(STUGOR_METER[1] - 1, 1000, STUGOR_NAMN[1])
    plt.text(STUGOR_METER[2] - 5, 1000, STUGOR_NAMN[2])
    plt.text(STUGOR_METER[3] - 5, 1000, STUGOR_NAMN[3])
    plt.text(STUGOR_METER[4] - 5, 1000, STUGOR_NAMN[4])
    plt.text(STUGOR_METER[5] - 5, 1000, STUGOR_NAMN[5])
    plt.text(STUGOR_METER[6] - 5, 1000, STUGOR_NAMN[6])
    plt.text(STUGOR_METER[7] - 5, 1000, STUGOR_NAMN[7])
    plt.text(STUGOR_METER[8] - 5, 1000, STUGOR_NAMN[8])
    plt.show()

if __name__ == "__main__":
    main()