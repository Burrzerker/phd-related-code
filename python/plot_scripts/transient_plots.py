import os
import numpy as np
import scipy.io as sio
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.animation import ArtistAnimation
from scipy.optimize import curve_fit


PATH_TO_MAT_FILE = 'E:\\Stornorrfors Data\\Validation Study\\Case_1\\temp\\manning_file_50_21_50_new\\water_level_no_hdf_8.mat'
PATH_TO_MEASURED_DATA = 'E:\\Stornorrfors Data\\Old River Bed\\Divers\\Diverdata.mat'
validation_points = {
    '1': (1199, 35),
    '2': (1099, 26),
    '3': (1039, 5),
    '4': (920, 9),
    '5': (826, 10),
    '6': (650, 22),
    '7': (400, 15),
    '8': (172, 12),
}

hfont = {'family':'Arial'}

def main():
    X, Y, time_series_water_level = read_data(PATH_TO_MAT_FILE)


    measured_water_level = read_validation_data(PATH_TO_MEASURED_DATA)
    normalized_validation_water_levels = plot_validation_transient(measured_water_level)
    normalized_simulated_water_levels = plot_transient_comparison(time_series_water_level)


    curve_fit_validation_data(normalized_validation_water_levels[0,:])

    compare_validation_and_simulation(normalized_simulated_water_levels, normalized_validation_water_levels)

    # plot_validation_point(time_series_water_level)
    # plot_contours(X, Y, time_series_water_level)
    # compare_gradients(normalized_validation_water_levels, normalized_simulated_water_levels)
    plt.show()
    # nr_of_timesteps = len(time_series_water_level[:,0,0])
    # animate_contour(X, Y, time_series_water_level, nr_of_timesteps)

def curve_fit_validation_data(data):
    t = np.linspace(0, 300, 300)
    time = 100
    y = func(t, 1, 0.1, 0)
    popt, pcov = curve_fit(func, t, data)
    fig = plt.figure()
    scaling = (35.453 - 35.0522)
    plt.plot(t[:time], data[:time], 'rx', label='data')
    plt.plot(t[:time], func(t[:time],*popt), 'b-', label='fit')
    plt.legend()
    plt.xlabel('Time in minutes since change in discharge')
    plt.ylabel('Exponential fit')


def func(x, a, b, c):
    return a * np.exp(-b * x) + c


def compare_gradients(validation_data, simulation_data):
    t = np.linspace(0, 300, 300)

    for index, _ in enumerate(validation_data[:,0]):
        fig = plt.figure()
        N = 20
        validation_gradient = np.gradient(validation_data[index,:], t)
        simulation_gradient = np.gradient(simulation_data[index,:], t)

        validation_gradient_sliding_average = np.convolve(validation_gradient, np.ones((N,))/N, mode='valid')
        simulation_gradient_sliding_average = np.convolve(simulation_gradient, np.ones((N,))/N, mode='valid')

        plt.plot(validation_gradient_sliding_average, label='Validation gradient, point={}'.format(index + 1))
        plt.plot(simulation_gradient_sliding_average, label='simulation gradient, point={}'.format(index + 1))
        max_gradient_validaition_index = np.argmin(validation_gradient_sliding_average)
        max_gradient_simulation_index = np.argmin(simulation_gradient_sliding_average)

        print("Max gradient validation: {}, at index: {}".format(min(validation_gradient_sliding_average), max_gradient_validaition_index))
        print("Max gradient simulation: {}, at index: {}".format(min(simulation_gradient_sliding_average), max_gradient_simulation_index))

def compare_validation_and_simulation(simulation, validation):
    minutes = len(validation[0,:])
    time = np.linspace(0, minutes, minutes)

    fig, axarr = plt.subplots(4, sharex='col', sharey='row')
    fig.suptitle('Transient response comparison points 1-4')
    l1, = axarr[0].plot(time, simulation[0,:], label="Simulation")
    l2, = axarr[0].plot(time, validation[0,:], label="Validation")
    axarr[0].set_title('Point 1', size='6', y=0.95)
    axarr[0].legend([l1, l2], ["Simulation", "Validation"], bbox_to_anchor=(1, 1), loc=1, borderaxespad=0.)
    axarr[1].plot(time, simulation[1,:], label='Simulation point {}'.format(2))
    axarr[1].plot(time, validation[1,:], label='Validation point {}'.format(2))
    axarr[1].set_title('Point 2', size='6', y=0.95)
    axarr[2].plot(time, simulation[2,:], label='Simulation point {}'.format(3))
    axarr[2].plot(time, validation[2,:], label='Validation point {}'.format(3))
    axarr[2].set_title('Point 3', size='6', y=0.95)
    axarr[3].plot(time, simulation[3,:], label='Simulation point {}'.format(4))
    axarr[3].plot(time, validation[3,:], label='Validation point {}'.format(4))
    axarr[3].set_title('Point 4', size='6', y=0.95)
    fig.text(0.5, 0.04, 'Time in minutes since change in discharge', **hfont, ha='center')
    fig.text(0.04, 0.5, r'$\frac{WSE - WSE_{t=\infty}}{ WSE_{t=\infty} - WSE_{t=0}}$', **hfont, va='center', rotation='vertical')
    plt.xticks(range(0, 300, 20))
    # plt.savefig('transient_response_1_4.png', dpi=400, bbox_inches='tight', pad_inches=0)


    fig, axarr = plt.subplots(4, sharex='col', sharey='row')
    fig.suptitle('Transient response comparison points 5-8')
    l1, = axarr[0].plot(time, simulation[4,:], label='Simulation point {}'.format(5))
    l2, = axarr[0].plot(time, validation[4,:], label='Validation point {}'.format(5))
    axarr[0].set_title('Point 5', size='6', y=0.95)
    axarr[0].legend([l1, l2], ["Simulation", "Validation"], bbox_to_anchor=(1, 1), loc=1, borderaxespad=0.)
    axarr[1].plot(time, simulation[5,:], label='Simulation point {}'.format(6))
    axarr[1].plot(time, validation[5,:], label='Validation point {}'.format(6))
    axarr[1].set_title('Point 6', size='6',  y=0.95)
    axarr[2].plot(time, simulation[6,:], label='Simulation point {}'.format(7))
    axarr[2].plot(time, validation[6,:], label='Validation point {}'.format(7))
    axarr[2].set_title('Point 7', size='6', y=0.95)
    axarr[3].plot(time, simulation[7,:], label='Simulation point {}'.format(8))
    axarr[3].plot(time, validation[7,:], label='Validation point {}'.format(8))
    axarr[3].set_title('Point 8', size='6', y=0.95)
    fig.text(0.5, 0.04, 'Time in minutes since change in discharge', **hfont, ha='center')
    fig.text(0.04, 0.5, r'$\frac{WSE - WSE_{t=\infty}}{ WSE_{t=\infty} - WSE_{t=0}}$', **hfont, va='center', rotation='vertical')
    plt.xticks(range(0, 300, 20))
    # plt.savefig('transient_response_5_8.png', dpi=400, bbox_inches='tight', pad_inches=0)
    ref_vector = np.linspace(0, 300, 300)
    print("SIMULATION")
    for index, _ in enumerate(simulation[:,0]):
        print("Point {}".format(index + 1))
        for ref, _ in enumerate(ref_vector):
            if 0.97 <= simulation[index, ref]:
                start = ref
            if simulation[index, ref] <= 0.04:
                stop = ref
                break

        print("Start: {}".format(start))
        print("Stop: {}".format(stop))
        print("Difference {}".format(stop - start))
            # print(simulation[index, ref])
        print("=" * 50)
    print("VALIDATION")
    for index, _ in enumerate(validation[:,0]):
        print("Point {}".format(index + 1))
        for ref, _ in enumerate(ref_vector):
            if 0.97 <= validation[index, ref]:
                start = ref
            if validation[index, ref] <= 0.04:
                stop = ref
                break

        print("Start: {}".format(start))
        print("Stop: {}".format(stop))
        print("Difference {}".format(stop - start))
            # print(simulation[index, ref])
        print("=" * 50)




def plot_validation_transient(water_level):
    number_of_measurements = 72000 # 72000 minutes
    start = 21725
    minutes = 300
    # start=0
    # minutes = number_of_measurements
    normalized_water_levels = np.ndarray(shape=(8, minutes))
    time = np.linspace(0, minutes, minutes)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.yaxis.label.set_size(16)
    for index, _ in enumerate(water_level[0,:]):
        normalized_water_level = (water_level[start:start+minutes, index] - water_level[start + minutes, index]) / (water_level[start, index] - water_level[start + minutes, index])
        normalized_water_levels[index, :]= normalized_water_level
        plt.plot(time, normalized_water_level, label='Point {}'.format(index+1))
    ax.legend()
    plt.xlabel('Time in minutes since change in discharge')
    plt.ylabel(r'$\frac{WSE - WSE_{t=\infty}}{ WSE_{t=\infty} - WSE_{t=0}}$', **hfont)
    plt.title('Transient response measurments')
    # plt.savefig('transient_response_validation.png', dpi=400, bbox_inches='tight', pad_inches=0)
    return normalized_water_levels


def plot_contours(X, Y, time_series_water_level):
    times = np.linspace(0,300, 5)
    for time in times:
        time = int(time)
        plot_contour(X, Y, time_series_water_level, time)


def plot_validation_point(time_series_water_level):
    function = time_series_water_level[:, validation_points['3'][0], validation_points['3'][1]]
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    plt.plot(function)


def plot_transient_comparison(time_series_water_level):
    time = 300 # 300 minutes

    normalized_water_levels = np.ndarray(shape=(8, time))
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    mpl.rc('font',family='Arial')
    for index in range(1, 9):
        temp = time_series_water_level[time, validation_points['{}'.format(index)][0], validation_points['{}'.format(index)][1]]
        data = time_series_water_level[:time, validation_points['{}'.format(index)][0], validation_points['{}'.format(index)][1]] - temp
        temp2 = data / data[0]
        normalized_water_levels[index-1,:] = temp2
        ax.plot(np.linspace(0, time, time), temp2, label='Point {}'.format(index))
    ax.legend()
    ax.yaxis.label.set_size(16)
    plt.xlabel('Time in minutes since change in discharge')
    plt.ylabel(r'$\frac{WSE - WSE_{t=\infty}}{ WSE_{t=\infty} - WSE_{t=0}}$', **hfont)
    plt.title('Transient response simulation')
    plt.savefig('transient_response_simulation.png', dpi=400, bbox_inches='tight', pad_inches=0)
    return normalized_water_levels

def plot_contour(X, Y, contour, timestep):
    fig = plt.figure()
    cont = plt.contourf(X, Y, contour[timestep, :, :], cmap=cm.jet, levels=25)
    plt.xlabel('Latitude SWEREF 99 [m]')
    plt.ylabel(r'Water Elevation MASL $[m]$')
    plt.title('Water Elevation, timestep: {} '.format(timestep))
    plt.axes().set_aspect('equal')
    cbar = plt.colorbar(cont)
    cbar.ax.set_ylabel('Elevation, MASL')



def animate_contour(X, Y, contour, timesteps):
    fig = plt.figure(figsize=(6.1,5),facecolor='w')
    ims = []
    levs = np.linspace(15,35,100)
    for i in range(timesteps):
        im = plt.contourf(X,Y,contour[i, :, :],levs,cmap=cm.jet,vmax=6,vmin=0)
        ims.append(im.collections)
        if i % 10 == 0:
            print(i)
    cbar = plt.colorbar(im)
    cbar.set_clim(0,6)
    cbar.set_ticks(np.linspace(0,6,7))
    cbar.set_label('Elevation [m/s]')
    plt.xlim(0,1)
    plt.ylim(0,1)
    plt.xlabel('x [m]')
    plt.ylabel('y [m]')
    plt.axes().set_aspect('equal')
    ani = ArtistAnimation(fig,ims,interval=200,repeat=True)

def read_data(path):
    data = sio.loadmat(path)
    X = data['data']['X'][0][0][:-1, :-1]
    Y = data['data']['Y'][0][0][:-1, :-1]
    time_series_water_level = data['data']['Val'][0][0]
    return X, Y, time_series_water_level

def transpose_diver_matrix(matrix):
    diver_data = np.ndarray(shape=(72000, 8))
    diver_data[:,0] = matrix[:,3]
    diver_data[:,1] = matrix[:,4]
    diver_data[:,2] = matrix[:,5]
    diver_data[:,3] = matrix[:,6]
    diver_data[:,4] = matrix[:,7]
    diver_data[:,5] = matrix[:,8]
    diver_data[:,6] = matrix[:,9]
    diver_data[:,7] = matrix[:,10]
    return diver_data


def read_validation_data(path):
    data = sio.loadmat(PATH_TO_MEASURED_DATA)
    diver_data = data['Diverdata']
    diver_data = transpose_diver_matrix(diver_data)

    measured_water_level_elevation = np.ndarray(shape=diver_data.shape)
    reference_elevations = [34.2, 28.077, 27.575, 25.846, 22.886, 17.82, 14.816, 12.429]
    for index, elevation in enumerate(reference_elevations):
        measured_water_level_elevation[:, index] = diver_data[:, index] + elevation
    return measured_water_level_elevation

if __name__ == '__main__':
    main()
