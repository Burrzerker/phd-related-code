import pandas
import matplotlib.pyplot as plt
import numpy as np
import os
import matplotlib.ticker as ticker
import matplotlib.cm as cm

PATH_TO_COMPARISON_DATA = "E:\\Stornorrfors Data\\Validation Study\\Case_1\\Data\\comparison_data.csv"
PATH_TO_DELFT_DATA_50Q = "E:\\Stornorrfors Data\\Validation Study\\Case_1\\Data\\50q_water_elevation.csv"
PATH_TO_DELFT_DATA_21Q = "E:\\Stornorrfors Data\\Validation Study\\Case_1\\Data\\21q_water_elevation.csv"

@ticker.FuncFormatter
def major_formatter(x, pos):
    return "%.4e" % x

def main():
    X, Y, water_bed, comparison_data = read_comparison_data(PATH_TO_COMPARISON_DATA)

    _, manning_matrix_50 = read_manning_data(PATH_TO_DELFT_DATA_50Q)
    _, manning_matrix_21 = read_manning_data(PATH_TO_DELFT_DATA_21Q)
    plot_comparison_data(X, Y, comparison_data, manning_matrix_50, 50)
    plot_comparison_data(X, Y, comparison_data, manning_matrix_21, 21)

    plt.show()

def plot_comparison_data(X, Y, data, matrix, case):

    temp = np.linspace(0,0,8)
    roughness_file_water_level = data['roughness_file_calibrated_{}'.format(case)]
    reference = data['water_elevation_q_{}'.format(case)]
    hec_reference = data['water_elevation_hec_q_{}'.format(case)]
    manning_list = ["0.01", "0.02", "0.03", "0.04", "0.05", "0.06", "0.07", "0.08", "0.09", "0.10", "0.11", "0.12"]
    fig = plt.figure()
    plt.rcParams['xtick.labelsize'] = 'xx-small'
    ax = fig.add_subplot(1, 1, 1)
    colors = cm.rainbow(np.linspace(0, 1, len(manning_list)))
    ax.xaxis.set_major_formatter(major_formatter)
    ax.scatter(Y, reference, label=r'$Q={} m^3/s$'.format(case), marker='s')
    for manning, c in zip(manning_list, colors):
        ax.scatter(Y, matrix['n={}'.format(manning)], label='n={}'.format(manning), color=c)
    ax.scatter(Y, hec_reference, marker='d', label='HEC-RAS, n=0.06')
    ax.scatter(Y, roughness_file_water_level, marker='p', label='Roughness File')
    ax.invert_xaxis()
    ax.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)

    plt.xlabel('Latitude SWEREF 99 [m]')
    plt.ylabel(r'Water Elevation MASL $[m]$')
    plt.title('Water Elevation for Q={} $m^3/s$'.format(case))

    # plt.savefig('water_level_roughness_file_{}.png'.format(case), dpi=400, bbox_inches='tight', pad_inches=0)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.xaxis.set_major_formatter(major_formatter)

    difference_measured = matrix.sub(reference, axis='rows')
    for manning, c in zip(manning_list, colors):
        ax.scatter(Y, difference_measured['n={}'.format(manning)], label='n={}'.format(manning), color=c)
    ax.plot(Y, temp, label='Measured data')
    ax.scatter(Y, hec_reference - reference, marker='d', label='HEC-RAS, n=0.06')
    ax.scatter(Y, roughness_file_water_level - reference, marker='p', label='Roughness File')
    ax.invert_xaxis()
    ax.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    plt.title('Water level difference for Q={} $m^3/s$'.format(case))
    plt.xlabel('Latitude SWEREF 99 [m]')
    plt.ylabel(r'$H_{sim} - H_{measured}$ [m]')
    # plt.savefig('water_level_diff_roughness_file_{}.png'.format(case), dpi=400, bbox_inches='tight', pad_inches=0)


def read_comparison_data(path_to_file):
    data = pandas.read_csv(path_to_file)
    X = data.iloc[:,0]
    Y = data.iloc[:,1]
    water_bed = data.iloc[:,2]
    comparison_data = data.iloc[:,3:]
    return X, Y, water_bed, comparison_data

def read_manning_data(path_to_file):
    data = pandas.read_csv(path_to_file)
    measured_data = data.iloc[:,2]
    manning_matrix = data.iloc[:,2:]
    return measured_data, manning_matrix


if __name__ == '__main__':
    main()
