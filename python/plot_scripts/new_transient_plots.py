import os
import scipy.io as sio
import matplotlib.pyplot as plt
import numpy as np
import yaml
import pandas as pd

# PATH_TO_CASE_FILES = "\\home\\anton\\phd-related-code\\python\\config_files\\simulations.yaml"
PATH_TO_CASE_FILES = "../config_files/simulations.yaml"
PATH_TO_CASE = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/250-21-Q/water_level.mat"
PATH_TO_MEASURED_DATA = "data/Diverdata.mat"

# Some original validation points are laid dry for some time steps
# real validation point 1 is 340, 28
# real validation point 4 is 674, 213
# real validation point 5 is 781, 190
validation_points = {
    '1': (340, 35),
    '2': (455, 150),
    '3': (526, 191),
    '4': (674, 100),
    '5': (781, 182),
    '6': (996, 95),
    '7': (1317, 180),
    '8': (1852, 175),
}

def main():
    cases = get_simulation_cases(PATH_TO_CASE_FILES)
    time_series_water_level_matrix, order_of_cases = read_water_level_data(cases)
    measured_water_level = read_validation_data(PATH_TO_MEASURED_DATA)

    # plot_water_level_in_validation_points(time_series_water_level_matrix, measured_water_level)
    plot_water_level_different_discharge(time_series_water_level_matrix, measured_water_level, order_of_cases)
    plt.show()


def plot_water_level_in_validation_points(data, measured_water_level):
    """
    Plot the water level in each validation point. Each validation point gets its own plot. 8 plots.
    """
    time = np.linspace(0, 241, 241)

    for index, _ in enumerate(data[0, :, 1]): # Number of validation points
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        for case, _ in enumerate(data[0, 1, :]): # Number of simulations
            time_series = data[:, index, case]
            ax.plot(time, time_series)
        plt.xlabel('Time in minutes since change in discharge')
        plt.ylabel(r'Water elevation MASL')
        ax.plot(time, measured_water_level[:, index], label='Measured data')
        plt.title(r'Transient response simulation point {}'.format(index + 1))
        plt.xlim(right=200)


def plot_water_level_different_discharge(data, measured_water_level, order_of_cases):
    """
    Plot the water lavel for each different discharge case in the validation points. 10 plots.
    """
    time = np.linspace(0, 241, 241)
    for index, _ in enumerate(data[0, 1, :]): # Number of simulations
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        for case, _ in enumerate(data[0, :, 1]): # Validation points
            time_series = data[:, case, index]
            normalized_time_series = (time_series - time_series[-1]) / (time_series[0] - time_series[-1])
            # if index==5 and case==3:
            #     print(time_series)
            #     print("=" * 50)
            #     print(normalized_time_series)
            if not case == 6 or case == 7: # Remove the two most downstream validation points
                ax.plot(time, normalized_time_series)
        plt.xlabel('Time in minutes since change in discharge')
        plt.ylabel(r'Water elevation MASL')
        # ax.plot(time, measured_water_level[:, index], label='Measured data')
        plt.title(r'Transient response simulation {}'.format(order_of_cases[index]))
        plt.xlim(right=200)

def get_simulation_cases(path_to_cases):
    with open(path_to_cases, 'r') as case_file:
        read_data = yaml.safe_load(case_file)
    temp = read_data['cases'].values()
    cases = []
    for case in temp:
        cases.append(case + "/water_level.mat")
    return cases

def read_water_level_data(cases):
    """
    Function that reads water level data time series and sorts it according to the eight validations points.
    There are 10 different simulations.

    :param cases: list of simulation cases
    :return time_series_water_level_matrix: 241x8x10 matrix containing time series with water levels
    """
    time_series_water_level_matrix = np.ndarray((241, 8, 10))
    order_of_cases = []
    for index, case in enumerate(cases):
        discharge = os.path.split(case)[0].split('/')[-1]
        order_of_cases.append(discharge)
        data = sio.loadmat(case)
        time_series_water_level = data['data']['Val'][0][0]
        temp_matrix = np.ndarray((241, 8))

        for point in validation_points:
            point = int(point)
            temp = time_series_water_level[:, validation_points["{}".format(point)][0], validation_points["{}".format(point)][1]]
            temp_matrix[:, point-1] = temp
        time_series_water_level_matrix[:, :, index] = temp_matrix

    X = data['data']['X'][0][0][:-1, :-1]
    Y = data['data']['Y'][0][0][:-1, :-1]
    return time_series_water_level_matrix, order_of_cases

def transpose_diver_matrix(matrix):
    diver_data = np.ndarray(shape=(72000, 8))
    diver_data[:,0] = matrix[:,3]
    diver_data[:,1] = matrix[:,4]
    diver_data[:,2] = matrix[:,5]
    diver_data[:,3] = matrix[:,6]
    diver_data[:,4] = matrix[:,7]
    diver_data[:,5] = matrix[:,8]
    diver_data[:,6] = matrix[:,9]
    diver_data[:,7] = matrix[:,10]
    return diver_data


def read_validation_data(path):
    data = sio.loadmat(PATH_TO_MEASURED_DATA)
    diver_data = data['Diverdata']
    diver_data = transpose_diver_matrix(diver_data)

    measured_water_level_elevation = np.ndarray(shape=diver_data.shape)
    reference_elevations = [34.2, 28.077, 27.575, 25.846, 22.886, 17.82, 14.816, 12.429]
    for index, elevation in enumerate(reference_elevations):
        measured_water_level_elevation[:, index] = diver_data[:, index] + elevation
    measured_water_level_elevation = measured_water_level_elevation[21725:21725+241]
    return measured_water_level_elevation

if __name__=='__main__':
    main()
