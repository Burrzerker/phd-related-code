# -*- coding: utf-8 -*-

import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

PATH_TO_EXAMPLE_OBSERVATIONS = "example_data/observations/"
PATH_TO_EXAMPLE_FIRST_ITERATION = "example_data/n_0.07_std_0.5/first_iteration/"
PATH_TO_EXAMPLE_LAST_ITERATION = "example_data/n_0.07_std_0.5/last_iteration/"

list_of_observations = os.listdir(PATH_TO_EXAMPLE_OBSERVATIONS)
list_of_first_iteration = os.listdir(PATH_TO_EXAMPLE_FIRST_ITERATION)
list_of_last_iteration = os.listdir(PATH_TO_EXAMPLE_LAST_ITERATION)

for item in list_of_first_iteration:
    print item

for item in list_of_last_iteration:
    print item

fig, axes = plt.subplots(5,1, sharex=True)
for index, item in enumerate(list_of_observations):
    observation_point = item.split('.')[0]
    print "Index: {}: Observation Point: M{}".format(index, observation_point)
 
    data = pd.read_csv("{}{}".format(PATH_TO_EXAMPLE_OBSERVATIONS, item))
    data_first = pd.read_csv("{}{}".format(PATH_TO_EXAMPLE_FIRST_ITERATION, item))
    data_last = pd.read_csv("{}{}".format(PATH_TO_EXAMPLE_LAST_ITERATION, item))
    
    axes[index].plot(data.iloc[:,1])
    axes[index].plot(data_first.iloc[:,1], 'r', markersize=1)
    axes[index].plot(data_last.iloc[:,1], 'p', markersize=1)
    axes[index].set_title("Observation Point: M{}".format(observation_point), fontsize=8)
axes[-1].legend(['Observation', 'First iteration', 'Last iteration'])
