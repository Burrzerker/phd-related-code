# This bash scripts installs delft3d and it's dependencies, based on the build_ubuntu1604 readme.


# ---------- Install aptitude dependencies--------------------------------------

sudo apt-get install autoconf subversion libtool flex g++ gfortran libstdc++6 byacc libexpat1-dev uuid-dev ruby make pkg-config -y

cd ~
# ----------Checkout Delft trunk repo ------------------------------------------
# username anton.burman
# password standard password
svn co https://svn.oss.deltares.nl/repos/delft3d/trunk delft3dtrunk

# ----------Install thrid party dependencies -----------------------------------

mkdir Downloads/libraries
chmod 777 -R Downloads/libraries
cd Downloads/libraries
##
# Install zlib
v=1.2.11
# wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/zlib-${v}.tar.gz
wget http://www.zlib.net/zlib-1.2.11.tar.gz
tar -xf zlib-1.2.11.tar.gz && cd zlib-1.2.11
./configure --prefix=/usr/local
#sudo make check install
sudo make install
cd ..

# Install HDF5
v=1.8.13
wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/hdf5-${v}.tar.gz
tar -xf hdf5-${v}.tar.gz && cd hdf5-${v}
echo "# HDF5 libraries for Delft3D"
export HDF5_DIR="/home/anton/Downloads/libraries/hdf5-${v}"
./configure --enable-shared --enable-hl --prefix=$HDF5_DIR
make -j 2 # 2 for number of procs to be used
sudo make install
cd ..

# Install Netcdf
v=4.4.1
wget http://www.unidata.ucar.edu/downloads/netcdf/ftp/netcdf-${v}.tar.gz
tar -xf netcdf-${v}.tar.gz && cd netcdf-${v}
echo "# NETCDF4 libraries for Delft3D"
export NETCDF4_DIR="/home/anton/Downloads/libraries/netcdf_4.4"
CPPFLAGS=-I$HDF5_DIR/include LDFLAGS=-L$HDF5_DIR/lib ./configure --enable-netcdf-4 --enable-shared --enable-dap --prefix=$NETCDF4_DIR
# make check
make
sudo make install
cd ..


export PATH=$PATH:$NETCDF4_DIR/bin
export LD_LIBRARY_PATH=$NETCDF4_DIR/lib:$LD_LIBRARY_PATH
export PKG_CONFIG_PATH=$NETCDF4_DIR/lib/pkgconfig:$PKG_CONFIG_PATH

# Install Netcdf-fortran
v=4.4.4
wget http://www.unidata.ucar.edu/downloads/netcdf/ftp/netcdf-fortran-${v}.tar.gz
tar -xf netcdf-fortran-${v}.tar.gz && cd netcdf-fortran-${v}
CPPFLAGS=-I$NETCDF4_DIR/include LDFLAGS=-L$NETCDF4_DIR/lib LD_LIBRARY_PATH=$NETCDF4_DIR/lib:$LD_LIBRARY_PATH ./configure --prefix=$NETCDF4_DIR
# make check
make
sudo make install
cd ..


# Install mpi
v=3.2
wget http://www.mpich.org/static/downloads/${v}/mpich-${v}.tar.gz
tar -xzf mpich-${v}.tar.gz
cd mpich-${v}
export MPICH2_3_2_DIR="/home/anton/Downloads/libraries/mpich"
./configure --prefix=$MPICH2_3_2_DIR
make
sudo make install
cd ..

export PATH=$PATH:$MPICH2_3_2_DIR/bin
export LD_LIBRARY_PATH=$MPICH2_3_2_DIR/lib:$LD_LIBRARY_PATH
export PKG_CONFIG_PATH=$MPICH2_3_2_DIR/lib/pkgconfig:$PKG_CONFIG_PATH
cd ~
sudo chmod 777 -R Downloads/libraries

# ---------- Install Delft trunk repo ------------------------------------------

cd /home/anton/delft3dtrunk/src
# Regarding the three argument error given when building
# https://oss.deltares.nl/web/delft3d/home/-/message_boards/message/1850291/maximized
# Copy the fixed file from my own code repo
cp /home/anton/phd-related-code/bash/oc.c /home/anton/delft3dtrunk/src/utils_lgpl/nefis/packages/nefis/src/oc.c -f
cp /home/anton/phd-related-code/bash/build_ubuntu1604.sh /home/anton/delft3dtrunk/src/build_ubuntu1604.sh -f

# Builds delft3d
./build_ubuntu1604.sh -gnu


# Copies MPI files after installation
cp -r ~/Downloads/libraries/mpich/lib/* ~/delft3dtrunk/src/bin/
cp -r ~/Downloads/libraries/mpich/bin/* ~/delft3dtrunk/src/bin/
cd ~/delft3dtrunk/bin/
chmod a+x *
