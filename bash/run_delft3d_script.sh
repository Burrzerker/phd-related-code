#!/bin/bash
# This script is used to run a Delft3D simulation with a single core.
# PATH_TO_CASE is the path to the folder containing the .xml file that defines the simulation
# DELFT_EXECUTABLE is the path to the run_dflow2d3d.sh script
# REMEMBER THAT YOU NEED TO CHANGE ALL PATHS FROM CASES DEFINED IN WINDOWS
# FROM \ TO / IN main.mdf.

PATH_TO_CASE=/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/validation_study/Case_1/temp/manning_file_50_21_50_new
DELFT_EXECTUABLE=/home/anton/delft3dtrunk/src/bin/run_dflow2d3d.sh

cd $PATH_TO_CASE
$DELFT_EXECTUABLE config_flow2d3d.xml
