#!/bin/bash
# This script is used to run a Delft3D simulation with a single core.
# PATH_TO_CASE is the path to the folder containing the .xml file that defines the simulation
# DELFT_EXECUTABLE is the path to the run_dflow2d3d.sh script
# REMEMBER THAT YOU NEED TO CHANGE ALL PATHS FROM CASES DEFINED IN WINDOWS
# FROM \ TO / IN main.mdf.

# $1 is the first command line argument, it should be the path to the folder containing the simulation case
# $2 is the second command line argument, it should be how many cores that you want to run
PATH_TO_CASE=$1
NUMBER_OF_CORES=$2

DELFT_EXECTUABLE=/home/anton/delft3dtrunk/src/bin/run_dflow2d3d_parallel.sh

cd $PATH_TO_CASE
$DELFT_EXECTUABLE $NUMBER_OF_CORES config_flow2d3d.xml
