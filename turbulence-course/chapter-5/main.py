import numpy as np
import pandas as pd
import scipy.optimize
from matplotlib import pyplot as plt
from matplotlib import rc 


def main():
    rc('text', usetex=True)

    # task_2()
    # task_3()
    # task_4_and_5()
    # task_6()
    # task_8()
    # task_9()
    task_10()


def task_10():
    data = pd.read_excel('Jet_measurements/Mean_velocity_profiles/centerline_mean.xls')
    data = np.array(data)
    u_x = np.array([3.1512, 3.1467, 3.1086, 2.9444, 2.7061, 2.3997, 2.1562, 1.6364, 1.4974, 1.4008, 1.2648, 1.1163, 0.9635, 0.8476]) 
    x = np.array([32, 57,82,107,132,157,182,257,282,307,332,382,432,482])*10**-3
    diameter = 25 * 10**-3
    nozzle_area = np.pi * (diameter/2)**2
    u_j = (5.5 / (60*60)) / nozzle_area
    u = np.array(data[:, 4])
    # for index, _ in enumerate(u):
    #     u[index] = np.float(data[index, 4])
    # u = data[:, 4]
    # x = np.array(data[:, 1]) * 10 ** -3
    # for item, _ in enumerate(u):
    # print(type(x[item]))
    test = scipy.optimize.curve_fit(lambda t, a,b: a*t**(-2/3)+b,  x[3:]/diameter,  u_x[3:]/u_j)
    plt.figure()
    plt.plot(x / diameter, u_x / u_j, 'o--')
    # plt.plot(x[3:]/diameter, coef[0]*x[3:]/diameter + coef[1])
    plt.plot(x[3:]/diameter, test[0][0]*(x[3:]/diameter)**(-2/3)+test[0][1])
    plt.plot(x[3:]/diameter, test[0][0]*(-2/3)*(x[3:]/diameter)**(-5/3))
    plt.xlabel(r"$x/d$")
    plt.ylabel(r"$u/u_j$")
    plt.legend([r"Measured $u/u_j$", r"$2.91x^{-2/3}-0.11$", "Derivative"])

def task_9():
    diameter = 25 * 10**-3
    nozzle_area = np.pi * (diameter/2)**2
    u_j = (5.5 / (60*60)) / nozzle_area
    r_12 = np.array([13, 15, 20, 45, 55])
    x_d = np.array([2, 4, 10, 15, 20]) / diameter

    coef_1 = np.polyfit(x_d, r_12, 1)
    print(f"Spread coefficient S: {coef_1[0]}")

    u_x = np.array([3.1512, 3.1467, 3.1086, 2.9444, 2.7061, 2.3997, 2.1562, 1.6364, 1.4974, 1.4008, 1.2648, 1.1163, 0.9635, 0.8476]) 
    x = np.array([32, 57,82,107,132,157,182,257,282,307,332,382,432,482])*10**-3

    coef = np.polyfit(x[4:]/diameter,u_j/u_x[4:], 1)

    print(f"Virtual origin: {coef[1]/diameter}")

    plt.figure()
    plt.plot(x/diameter, u_j/u_x, 'o')
    plt.xlim(0, max(x)/diameter)
    plt.plot(x[4:]/diameter, coef[0]*x[4:]/diameter + coef[1])
    plt.xlabel(r"$x/d$")
    plt.ylabel(r"$u_j/u$")
    plt.legend(["Measured data", "Curve fit"])

    plt.figure()
    plt.plot(x_d*diameter, r_12, 'o')
    plt.plot(x_d*diameter, coef_1[0] * x_d)
    plt.xlabel(r"$x/d$")
    plt.ylabel(r"$r_{1/2}$")
    plt.legend([r"Approximate $r_{1/2}$", "Curve fit"])

    print(f"Spreading angle: {np.degrees(np.arctan(0.06))} degrees")

def task_8():
    uu = np.ones(17)
    vv = np.ones(17)
    uv = np.ones(17)
    for index, item in enumerate(uu):
        data = np.genfromtxt(f"Jet_measurements/Profile5_coincidence/tRad5_{index+1}_coincidence.csv", delimiter=',', skip_header=3) # Profile 1 upper jet
        u = data[:,1]# / data[:, 10]
        # u = u[~np.isnan(u)]
        U = np.mean(u)
        v = data[:,4] #/ data[:, 9]
        # v = v[~np.isnan(v)]
        V = np.mean(v)
        uu[index] = np.mean((U - u) * (U - u))
        vv[index] = np.mean((V - v) * (V - v))
        uv[index] = np.mean((U - u) * (V - v))
    data = pd.read_excel('Jet_measurements/Mean_velocity_profiles/profile5_mean.xls')
    data = np.array(data)
    u0 = data[1, 4]
    # u = np.empty_like(data[:17, 1])
    # u[:17] = np.flip(data[:17, 6])
    # v = np.empty_like(data[:17, 7])
    # v[:17] = np.flip(data[:17, 7])
    x_5 = np.empty_like(data[:17, 1])
    x_5[:17] = np.flip(data[:17, 2])

    plt.figure()
    plt.plot(x_5 / x_5[5], np.flip(uu / (u0**2)), 'o--')
    plt.plot(x_5 / x_5[5], np.flip(vv / (u0**2)), 'o--')
    plt.plot(x_5 / x_5[5], np.flip(uv / (u0**2)), 'o--')
    plt.legend([r"$\langle u'^2\rangle$", r"$\langle v'^2\rangle$", r"$\langle u'v'\rangle$"])
    plt.xlabel(r"$r/r_{1/2}$")
    plt.ylabel(r"$\langle u_iu_j\rangle / U_{0}^2$")
    

def task_6():
    data_1 = pd.read_excel('Jet_measurements/Mean_velocity_profiles/profile1_mean.xls')
    data_1 = np.array(data_1)
    profile_1 = np.empty_like(data_1[:, 1])
    profile_1[:22] = np.flip(data_1[:22, 5])
    profile_1[22:] = data_1[22:,5]
    x_1 = np.empty_like(data_1[:, 1])
    x_1[:22] = np.flip(data_1[:22, 2])
    x_1[22:] = data_1[22:,2]

    data_2 = pd.read_excel('Jet_measurements/Mean_velocity_profiles/profile2_mean.xls')
    data_2 = np.array(data_2)
    profile_2 = np.empty_like(data_2[:, 1])
    profile_2[:16] = np.flip(data_2[:16, 5])
    profile_2[16:] = data_2[16:,5]
    x_2 = np.empty_like(data_2[:, 1])
    x_2[:16] = np.flip(data_2[:16, 2])
    x_2[16:] = data_2[16:,2]

    data_3 = pd.read_excel('Jet_measurements/Mean_velocity_profiles/profile3_mean.xls')
    data_3 = np.array(data_3)
    profile_3 = np.empty_like(data_3[:, 1])
    profile_3[:15] = np.flip(data_3[:15, 5])
    profile_3[15:] = data_3[15:,5]
    x_3 = np.empty_like(data_3[:, 1])
    x_3[:15] = np.flip(data_3[:15, 2])
    x_3[15:] = data_3[15:,2]

    data_4 = pd.read_excel('Jet_measurements/Mean_velocity_profiles/profile4_mean.xls')
    data_4 = np.array(data_4)
    profile_4 = np.empty_like(data_4[:, 1])
    profile_4[:20] = np.flip(data_4[:20, 5])
    profile_4[20:] = data_4[20:,5]

    x_4 = np.empty_like(data_4[:, 1])
    x_4[:20] = np.flip(data_4[:20, 2])
    x_4[20:] = data_4[20:,2]

    data_5 = pd.read_excel('Jet_measurements/Mean_velocity_profiles/profile5_mean.xls')
    data_5 = np.array(data_5)
    profile_5 = np.empty_like(data_5[:, 1])
    profile_5[:17] = np.flip(data_5[:17, 5])
    profile_5[17:] = data_5[17:,5]
    x_5 = np.empty_like(data_5[:, 1])
    x_5[:17] = np.flip(data_5[:17, 2])
    x_5[17:] = data_5[17:,2]

    fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5,1, sharex=True)
    ax1.plot(x_1, profile_1, 'o')
    ax2.plot(x_2, profile_2, 'o')
    ax3.plot(x_3, profile_3, 'o')
    ax4.plot(x_4, profile_4, 'o')
    ax5.plot(x_5, profile_5, 'o')
    fig.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
    plt.xlabel(r"$x$")
    #plt.ylabel(r"$u_{axial}$")


def task_4_and_5():
    data_1 = pd.read_excel('Jet_measurements/Mean_velocity_profiles/profile1_mean.xls')
    data_1 = np.array(data_1)
    profile_1 = np.empty_like(data_1[:, 1])
    profile_1[:22] = np.flip(data_1[:22, 4])
    profile_1[22:] = data_1[22:,4]
    x_1 = np.empty_like(data_1[:, 1])
    x_1[:22] = np.flip(data_1[:22, 2])
    x_1[22:] = data_1[22:,2]

    data_2 = pd.read_excel('Jet_measurements/Mean_velocity_profiles/profile2_mean.xls')
    data_2 = np.array(data_2)
    profile_2 = np.empty_like(data_2[:, 1])
    profile_2[:16] = np.flip(data_2[:16, 4])
    profile_2[16:] = data_2[16:,4]
    x_2 = np.empty_like(data_2[:, 1])
    x_2[:16] = np.flip(data_2[:16, 2])
    x_2[16:] = data_2[16:,2]

    data_3 = pd.read_excel('Jet_measurements/Mean_velocity_profiles/profile3_mean.xls')
    data_3 = np.array(data_3)
    profile_3 = np.empty_like(data_3[:, 1])
    profile_3[:15] = np.flip(data_3[:15, 4])
    profile_3[15:] = data_3[15:,4]
    x_3 = np.empty_like(data_3[:, 1])
    x_3[:15] = np.flip(data_3[:15, 2])
    x_3[15:] = data_3[15:,2]

    data_4 = pd.read_excel('Jet_measurements/Mean_velocity_profiles/profile4_mean.xls')
    data_4 = np.array(data_4)
    profile_4 = np.empty_like(data_4[:, 1])
    profile_4[:20] = np.flip(data_4[:20, 4])
    profile_4[20:] = data_4[20:,4]

    x_4 = np.empty_like(data_4[:, 1])
    x_4[:20] = np.flip(data_4[:20, 2])
    x_4[20:] = data_4[20:,2]

    data_5 = pd.read_excel('Jet_measurements/Mean_velocity_profiles/profile5_mean.xls')
    data_5 = np.array(data_5)
    profile_5 = np.empty_like(data_5[:, 1])
    profile_5[:17] = np.flip(data_5[:17, 4])
    profile_5[17:] = data_5[17:,4]
    x_5 = np.empty_like(data_5[:, 1])
    x_5[:17] = np.flip(data_5[:17, 2])
    x_5[17:] = data_5[17:,2]
    diameter = 25 * 10**-3

    fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(1,5, sharex=True, sharey=True)
    ax1.plot(profile_1, x_1, 'o')
    ax1.plot([0, max(profile_1)/2], [x_1[10], x_1[10]], 'r--')
    ax2.plot(profile_2, x_2, 'o')
    ax2.plot([0, max(profile_2)/2], [x_2[10], x_2[10]], 'r--')
    ax3.plot(profile_3, x_3, 'o')
    ax3.plot([0, max(profile_3)/2], [x_3[10], x_3[10]], 'r--')
    ax4.plot(profile_4, x_4, 'o')
    ax4.plot([0, max(profile_4)/2], [x_4[10], x_4[10]], 'r--')
    ax5.plot(profile_5, x_5, 'o', label=r"Velocity profile")
    ax5.plot([0, max(profile_5)/2], [x_5[5], x_5[5]], 'r--', label=r"$r_{1/2}$")
    ax1.set_xlim([0, 3.5])
    ax2.set_xlim([0, 3.5])
    ax3.set_xlim([0, 3.5])
    ax4.set_xlim([0, 3.5])
    ax5.set_xlim([0, 3.5])
    ax1.set_ylim([-100, 125])
    ax2.set_ylim([-100, 125])
    ax3.set_ylim([-100, 125])
    ax4.set_ylim([-100, 125])
    ax5.set_ylim([-100, 125])
    ax1.set_title("Profile 1")
    ax2.set_title("Profile 2")
    ax3.set_title("Profile 3")
    ax4.set_title("Profile 4")
    ax5.set_title("Profile 5")
    fig.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
    plt.xlabel("Mean axial velocity")
    plt.ylabel("Axial position")

    ax3.set_title("Profile 3")
    ax4.set_title("Profile 4")
    ax5.set_title("Profile 5")
    fig.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
    plt.xlabel("Mean axial velocity")
    plt.ylabel("Axial position")

    plt.figure()
    plt.plot(x_1 / x_1[10], profile_1 / profile_1[10], 'o--')
    plt.plot(x_2 / x_2[10], profile_2 / profile_2[10], 'o--')
    plt.plot(x_3 / x_3[10], profile_3 / profile_3[10], 'o--')
    plt.plot(x_4 / x_4[10], profile_4 / profile_4[10], 'o--')
    plt.plot(x_5 / x_5[5], profile_5 / profile_5[5], 'o--')
    plt.xlabel(r"$r/r_{1/2}$")
    plt.ylabel(r"$U/U_0$")
    plt.legend(["Profile 1", "Profile 2", "Profile 3", "Profile 4", "Profile 5"])
    
def task_3():
    data = np.genfromtxt("Jet_measurements/Profile1/tRad1_1.csv", delimiter=',', skip_header=3) # Profile 1 upper jet
    u1 = data[:,1] # Axial velocity
    u1 = u1[~np.isnan(u1)]
    u2 = data[:,4] # Radial velocity
    u2 = u2[~np.isnan(u2)]

    fig, (ax1, ax2) = plt.subplots(2,1)
    fig.suptitle("Profile 1")
    ax1.set_ylim([0, 7000])
    ax1.hist(u1, bins=100)
    ax1.set_xlabel("Axial velocity")
    ax1.set_ylabel("Count")
    ax2.set_ylim([0, 15000])
    ax2.hist(u2, bins=100)
    ax2.set_xlabel("Radial velocity")
    ax2.set_ylabel("Count")

    data = np.genfromtxt("Jet_measurements/Profile2/tRad2_1.csv", delimiter=',', skip_header=3) # Profile 1 upper jet
    u1 = data[:,1] # Axial velocity
    u1 = u1[~np.isnan(u1)]
    u2 = data[:,4] # Radial velocity
    u2 = u2[~np.isnan(u2)]

    fig, (ax1, ax2) = plt.subplots(2,1)
    fig.suptitle("Profile 2")
    ax1.set_ylim([0, 7000])
    ax1.hist(u1, bins=100)
    ax1.set_xlabel("Axial velocity")
    ax1.set_ylabel("Count")
    ax2.set_ylim([0, 15000])
    ax2.hist(u2, bins=100)
    ax2.set_xlabel("Radial velocity")
    ax2.set_ylabel("Count")

    data = np.genfromtxt("Jet_measurements/Profile3/tRad3_1.csv", delimiter=',', skip_header=3) # Profile 1 upper jet
    u1 = data[:,1] # Axial velocity
    u1 = u1[~np.isnan(u1)]
    u2 = data[:,4] # Radial velocity
    u2 = u2[~np.isnan(u2)]

    fig, (ax1, ax2) = plt.subplots(2,1)
    fig.suptitle("Profile 3")
    ax1.set_ylim([0, 7000])
    ax1.hist(u1, bins=100)
    ax1.set_xlabel("Axial velocity")
    ax1.set_ylabel("Count")
    ax2.set_ylim([0, 15000])
    ax2.hist(u2, bins=100)
    ax2.set_xlabel("Radial velocity")
    ax2.set_ylabel("Count")

    data = np.genfromtxt("Jet_measurements/Profile4/tRad4_1.csv", delimiter=',', skip_header=3) # Profile 1 upper jet
    u1 = data[:,1] # Axial velocity
    u1 = u1[~np.isnan(u1)]
    u2 = data[:,4] # Radial velocity
    u2 = u2[~np.isnan(u2)]

    fig, (ax1, ax2) = plt.subplots(2,1)
    fig.suptitle("Profile 4")
    ax1.set_ylim([0, 7000])
    ax1.hist(u1, bins=100)
    ax1.set_xlabel("Axial velocity")
    ax1.set_ylabel("Count")
    ax2.set_ylim([0, 15000])
    ax2.hist(u2, bins=100)
    ax2.set_xlabel("Radial velocity")
    ax2.set_ylabel("Count")

    data = np.genfromtxt("Jet_measurements/Profile5/tRad5_-1.csv", delimiter=',', skip_header=3) # Profile 1 upper jet
    u1 = data[:,1] # Axial velocity
    u1 = u1[~np.isnan(u1)]
    u2 = data[:,4] # Radial velocity
    u2 = u2[~np.isnan(u2)]
   
    fig, (ax1, ax2) = plt.subplots(2,1)
    fig.suptitle("Profile 5")
    ax1.set_ylim([0, 7000])
    ax1.hist(u1, bins=100)
    ax1.set_xlabel("Axial velocity")
    ax1.set_ylabel("Count")
    ax2.set_ylim([0, 15000])
    ax2.hist(u2, bins=100)
    ax2.set_xlabel("Radial velocity")
    ax2.set_ylabel("Count")


def task_2():
    viscosity = 1.0034 * 10 **-6
    diameter = 25 * 10**-3
    nozzle_area = np.pi * (diameter/2)**2
    u_j = (5.5 / (60*60)) / nozzle_area
    reynolds_number = u_j * diameter / viscosity 
    print(f"The Reynolds number is: {round(reynolds_number)}")


if __name__ == "__main__":
    main()
    plt.show()