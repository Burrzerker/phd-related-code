import os
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc
from scipy.io import savemat
from scipy.io import loadmat
from scipy import integrate
import matplotlib.ticker as ticker

PATH_TO_DATA = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/Turbulenskurs/data"
font_size = 14
d = 15 * 10 ** -3

def main():    
    rc('text', usetex=True)
    
    # generate_time_averaged_data(os.path.join(PATH_TO_DATA, "Vol_2_Run1_Rec1"))
    # time_average()
    U1, V1, U2, V2, X1, Y1, X2, Y2, r1, r2, x, _, _, _ = parse_data()
    # plot_velocity_fields(X1, X2, Y1, Y2, U1, U2, V1, V2)
    # momentum_flux(x, x, r1, r2, U1, U2)
    r_12_plane_1 = r_half(x)
    r_12_plane_2 = r_half(x + 30*d) # Plane 2 starts at x = 30*d according to lab manual
    self_similar_vel(r_12_plane_2, r2, U2, V2)
    # reynolds_stress(X1, Y1, X2, Y2, r2, r_12_plane_2, U2)
    # kinetic_energy(X2, Y2, r2, r_12_plane_2)
    plt.show()

def self_similar_vel(r_12_plane_2, r2, U2, V2):
    fig, (ax1, ax2) = plt.subplots(1,2, sharex=True)
    ax1.plot(r2/r_12_plane_2[0], U2[25:,0] / U2[25, 0], 'x')
    ax1.plot(r2/r_12_plane_2[25], U2[25:,25] / U2[25, 25], 'd')
    ax1.plot(r2/r_12_plane_2[50], U2[25:,50] / U2[25, 50], 'o')
    ax1.plot(r2/r_12_plane_2[75], U2[25:,75] / U2[25, 75], '+')
    
    ax1.set_xlabel(r"$r/r_{1/2}$", fontsize=font_size)
    ax1.set_ylabel(r"$\langle U \rangle / U_0$", fontsize=font_size)
    ax2.plot(r2/r_12_plane_2[0], V2[25:,0] / U2[25, 0], 'x')
    ax2.plot(r2/r_12_plane_2[25], V2[25:,25] / U2[25, 25], 'd')
    ax2.plot(r2/r_12_plane_2[50], V2[25:,50] / U2[25, 50], 'o')
    ax2.plot(r2/r_12_plane_2[75], V2[25:,75] / U2[25, 75], '+')
    ax2.set_ylabel(r"$\langle V \rangle / U_0$", fontsize=font_size)
    ax2.set_xlabel(r"$r/r_{1/2}$", fontsize=font_size)
    fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.5, hspace=None)
    ax1.legend([r"$x/D=30$", r"$x/D=32$", r"$x/D=34$", r"$x/D=36$"], fontsize=10)
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig("self-similar-vel.png", dpi=600, bbox_inches='tight', transparent=False)

def r_half(x):
    x_0 = -5*d/2
    # x_0 = -2.25*d
    S = 0.096 # Approximately, see pope p 101
    return S*(x-x_0)

def plot_velocity_fields(X1, X2, Y1, Y2, U1, U2, V1, V2):
    x_plot = np.linspace(0.5, 10, 11)
    # print(x_plot)
    D = 30 * np.ones(shape=X2.shape)
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2, sharex=False, sharey=False)
    cont_1 = ax1.contourf(X1 / 15, Y1, U1[40:, :], 100, cmap="coolwarm")
    ax1.set_title(r"Plane 1", fontsize=font_size)
    ax1.set_ylabel(r"$y~[mm]$", fontsize=font_size)
    ax1.set_xlabel(r"$x/D$", fontsize=font_size)
    # ax1.plot(x_plot, 53 + (x_plot / 5))
    cont_2 = ax2.contourf(X1/ 15, Y1, V1[40:, :], 100, cmap="coolwarm")
    ax2.set_title(r" Plane 1", fontsize=font_size)
    ax2.set_xlabel(r"$x/D$", fontsize=font_size)
    cont_3 = ax3.contourf(D + X2/ 15, Y2, U2[25:, :], 100, cmap="coolwarm")
    ax3.set_title(r"Plane 2", fontsize=font_size)
    ax3.set_ylabel(r"$y~[mm]$", fontsize=font_size)
    ax3.set_xlabel(r"$x/D$", fontsize=font_size)
    # ax3.plot([D[0,:]+ X2[0,:]/15, (D[0,:]+ X2[0,:]/15) / 5)
    # print((D[0,:]+ X2[0,:]/15) / 5)
    cont_4 = ax4.contourf(D+ X2/ 15, Y2, V2[25:, :], 100, cmap="coolwarm")
    ax4.set_title(r"Plane 2", fontsize=font_size)
    ax4.set_xlabel(r"$x/D$", fontsize=font_size)
    plt.colorbar(cont_1, ax=ax1, format="%.1e").set_label(label=r"$\langle u\rangle$", fontsize=font_size,weight='bold')
    plt.colorbar(cont_2, ax=ax2, format="%.1e").set_label(label=r"$\langle v\rangle$", fontsize=font_size,weight='bold')
    plt.colorbar(cont_3, ax=ax3, format="%.1e").set_label(label=r"$\langle u\rangle$", fontsize=font_size,weight='bold')
    plt.colorbar(cont_4, ax=ax4, format="%.1e").set_label(label=r"$\langle v\rangle$", fontsize=font_size,weight='bold')
    fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.5, hspace=0.5)

    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("time_averaged_velocity_fields.png", dpi=600, bbox_inches='tight', transparent=False)

def momentum_flux(x1, x2, r1, r2, U1, U2):
    momentum_flux_1 = np.ndarray(shape=x1.shape)
    momentum_flux_2 = np.ndarray(shape=x2.shape)
    rho = 0.998
    for index, _ in enumerate(x1):
        momentum_flux_1[index] = 2 * 3.14 * rho * integrate.trapz(r1 * (U1[40:,index]**2), r1)
        
    for index, _ in enumerate(x2):
        momentum_flux_2[index] = 2 * 3.14 * rho * integrate.trapz(r2 * (U2[25:,index]**2), r2)

    fig, (ax1, ax2) = plt.subplots(2,1, sharex=True)
    ax1.plot(momentum_flux_1[1:])
    ax1.plot(momentum_flux_2[1:])
    ax1.ticklabel_format(axis="y", style="sci", scilimits=(4,4))
    ax1.set_ylabel(r"$\dot{M}(x)$")
    ax2.plot(np.diff(momentum_flux_1[1:]))
    ax2.plot(np.diff(momentum_flux_2[1:]))
    ax2.set_xlabel(r"$x~[mm]$")
    ax2.set_ylabel(r"$diff(\dot{M}(x)$)")
    ax2.ticklabel_format(axis="y", style="sci", scilimits=(4,4))
    plt.legend([r"Plane 1", r"Plane 2"])
    # plt.savefig("momentum-flux.png", dpi=600, bbox_inches='tight', transparent=False)
    
def parse_data():
    # files_in_directory = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    # X, Y, dimensions = parse_spatial_dimensions(os.path.join(path, files_in_directory[0]))
    # U, V = parse_velocity(os.path.join(path, files_in_directory[0]), dimensions)
    X = loadmat("data/x.mat")["X"]
    Y = loadmat("data/y.mat")["Y"]
    U1 = loadmat("data/Vol_1_Run1_Rec1_U_avg.mat")["u"]
    V1 = loadmat("data/Vol_1_Run1_Rec1_V_avg.mat")["v"]
    U2 = loadmat("data/Vol_2_Run1_Rec1_U_avg.mat")["u"]
    V2 = loadmat("data/Vol_2_Run1_Rec1_V_avg.mat")["v"]
    Y1 = Y[40:, :]
    Y2 = Y[25:, :]
    X1 = X[40:, :]
    X2 = X[25:, :]

    r1 = (Y1[:,0] - Y1[0,0])  * 10**-3
    x1 = X1[0, :] * 10**-3
    r2 = (Y2[:,0] - Y2[0,0]) * 10**-3
    x2 = X2[0, :] * 10**-3
    # r1 = Y1[:,0] * 10**-3
    # r2 = Y2[:,0] * 10**-3
    
    return U1, V1, U2, V2, X1, Y1, X2, Y2, r1, r2, x1, x2, X, Y

def parse_spatial_dimensions(path):
    data = np.genfromtxt(path, delimiter=",", skip_header=9)
    x = data[:, 0]
    y = data[:, 1]
    X, Y = np.meshgrid(list(set(x)), list(set(y)))
    dimensions = X.shape
    return X, Y, dimensions
    
def parse_velocity(path, dimensions):

    data = np.genfromtxt(path, delimiter=",", skip_header=9)
    u = data[:, 2]
    v = data[:, 3]
    u = np.reshape(u, dimensions)
    v = np.reshape(v, dimensions)
    return u, v

def kinetic_energy(X, Y, r2, r_12_plane_2):
    U = loadmat("data/Vol_2_Run1_Rec1_U_avg.mat")["u"]
    V = loadmat("data/Vol_2_Run1_Rec1_V_avg.mat")["v"]
    u = loadmat("data/Vol_2_Run1_Rec1_U.mat")["u"]
    v = loadmat("data/Vol_2_Run1_Rec1_V.mat")["v"]
    
    u_fluc = U[25:, :, None] - u[25:, :, :]
    v_fluc = V[25:, :, None] - v[25:, :, :]
    k = (1/2) * (u_fluc**2 + v_fluc**2)
    k_mean = np.mean(k, axis=2)

    divergence = np.sqrt(U[25:, :]**2 + V[25:, :]**2) * (np.gradient(k_mean, axis=1) + np.gradient(k_mean, axis=0))

    plt.figure()
    plt.plot(r2/r_12_plane_2[0], -divergence[:, 0] / U[25, 0]**3, 'x--')
    plt.plot(r2/r_12_plane_2[25], -divergence[:, 25]/ U[25, 25]**3, 'd--')
    plt.plot(r2/r_12_plane_2[50], -divergence[:, 50]/ U[25, 50]**3, 'o--')
    plt.plot(r2/r_12_plane_2[75], -divergence[:, 75]/ U[25, 75]**3, '+--')
    plt.xlabel(r"$r/r_{1/2}$", fontsize=font_size)
    plt.ylabel(r"Mean flow convection $-\frac{\overline{D}k}{\overline{D}t}$", fontsize=font_size)
    plt.legend([r"$x/D=30$", r"$x/D=32$", r"$x/D=34$", r"$x/D=36$"], fontsize=font_size, bbox_to_anchor=(1, 1))
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("convection.png", dpi=600, bbox_inches='tight', transparent=False)
    # plt.figure()
    # plt.contourf(X, Y, k_mean, 100, cmap="coolwarm")
    # plt.ylabel(r"$y~[mm]$", fontsize=font_size)
    # plt.xlabel(r"$X~[mm]$", fontsize=font_size)

def reynolds_stress(X1, Y1, X2, Y2, r2, r_12_plane_2, U2):
    D = 30 * np.ones(shape=X2.shape)

    UV1 = loadmat("data/Vol_1_Run1_Rec1_UV_avg.mat")["uv"]
    UV2 = loadmat("data/Vol_2_Run1_Rec1_UV_avg.mat")["uv"]

    plt.figure()
    plt.plot(r2/r_12_plane_2[0], -np.gradient(U2[25:, 0], r2)*UV2[25:, 0] , 'x--')
    plt.plot(r2/r_12_plane_2[25], -np.gradient(U2[25:, 25], r2)*UV2[25:, 25], 'd--')
    plt.plot(r2/r_12_plane_2[50], -np.gradient(U2[25:, 50], r2)*UV2[25:, 50], 'o--')
    plt.plot(r2/r_12_plane_2[75], -np.gradient(U2[25:, 75], r2)*UV2[25:, 75], '+--')
    plt.xlabel(r"$r/r_{1/2}$", fontsize=font_size)
    plt.ylabel(r"Production $\mathcal{P}$", fontsize=font_size)
    plt.legend([r"$x/D=30$", r"$x/D=32$", r"$x/D=34$", r"$x/D=36$"], fontsize=font_size, bbox_to_anchor=(1, 1))
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("production.png", dpi=600, bbox_inches='tight', transparent=False)
    # fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2)
    # cont_1 = ax1.contourf(X1 / 15, Y1, UV1[40:, :], 100, cmap="coolwarm")
    # ax1.set_title(r"Plane 1", fontsize=font_size)
    # ax1.set_ylabel(r"$y~[mm]$", fontsize=font_size)
    # ax1.set_xlabel(r"$x/D$", fontsize=font_size)
    # cont_2 = ax2.contourf(D+ X2/ 15, Y2, UV2[25:, :], 100, cmap="coolwarm")
    # ax2.set_title(r"Plane 2", fontsize=font_size)
    # ax2.set_xlabel(r"$x/D$", fontsize=font_size)
    # plt.colorbar(cont_1, ax=ax1, format="%.3e",orientation="vertical").set_label(label=r"$\langle uv\rangle$", fontsize=font_size,weight='bold')
    # plt.colorbar(cont_2, ax=ax2, format="%.3e",orientation="vertical").set_label(label=r"$\langle uv\rangle$", fontsize=font_size,weight='bold')
    # fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.5, hspace=0.5)
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("reynolds-stress.png", dpi=600, bbox_inches='tight', transparent=False)

def generate_time_averaged_data(path):

    files_in_directory = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    data = np.genfromtxt(os.path.join(path, files_in_directory[0]), delimiter=",", skip_header=9)
    x = data[:, 0]
    y = data[:, 1]
    X, Y = np.meshgrid(list(set(x)), list(set(y)))
    # savemat("x.mat", {"X": X})
    # savemat("y.mat", {"Y": Y})
    dimensions = X.shape
    timesteps = len(files_in_directory)
    u = np.ndarray((dimensions[0], dimensions[1], timesteps))
    v = np.ndarray((dimensions[0], dimensions[1], timesteps))
    uv = np.ndarray((dimensions[0], dimensions[1], timesteps))
    for index, file in enumerate(files_in_directory):
        data = np.genfromtxt(os.path.join(path, file), delimiter=",", skip_header=9)
        u[:, :, index] = np.reshape(data[:, 2], dimensions)
        v[:, :, index] = np.reshape(data[:, 3], dimensions)
        uv[:, :, index] = u[:, :, index] * v[:, :, index]
        print(index)
    u_struct = {"u": u}
    v_struct = {"v": v}
    uv_struct = {"uv": uv}
    # savemat("Vol_1_Run1_Rec1_U.mat", u_struct)
    # savemat("Vol_1_Run1_Rec1_V.mat", v_struct)
    savemat("Vol_2_Run1_Rec1_UV.mat", uv_struct)

def time_average():
    uv1 = loadmat("data/Vol_1_Run1_Rec1_UV.mat")["uv"]
    uv2 = loadmat("data/Vol_2_Run1_Rec1_UV.mat")["uv"]
    uv1_avg = np.mean(uv1, axis=2)
    uv2_avg = np.mean(uv2, axis=2)
    savemat("Vol_1_Run1_Rec1_UV_avg.mat", {"uv": uv1_avg})
    savemat("Vol_2_Run1_Rec1_UV_avg.mat", {"uv": uv2_avg})
    # u = loadmat("Vol1_Run1_Rec1_allData_U.mat")["u"]
    # v = loadmat("Vol1_Run1_Rec1_allData_V.mat")["v"]
    # u_avg = np.mean(u, axis=2)
    # v_avg = np.mean(v, axis=2)
    # savemat("Vol1_Run1_Rec1_allData_U_avg.mat", {"u": u_avg})
    # savemat("Vol1_Run1_Rec1_allData_V_avg.mat", {"v": v_avg})

def fmt(x, pos):
    a, b = '{:.2e}'.format(x).split('e')
    b = int(b)
    return r'${} \times 10^{{{}}}$'.format(a, b)


if __name__ == "__main__":
    main()