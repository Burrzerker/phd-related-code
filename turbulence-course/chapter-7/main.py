import numpy as np
from scipy.io import loadmat
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt
from matplotlib import rc 
PATH = "Turbulence_course_Joel.mat"

nu = 1.16 * 10 **-6
rho = 1011
k = 0.41
B = 5.42
mu = 1.005 * 10 **-3
FONTSIZE=14

def main():
    rc('text', usetex=True)

    data = loadmat(PATH)
    u = data["U"][0]
    y = data["y"][0]
    y_displaced = y[1:] + 50*10**-6
    uv = data["uv"][0]

    gradient = np.gradient(u, y, edge_order=2) # du/dr 
    
    Tau_w1 = mu * gradient[0]
    u_tau1 = np.sqrt(Tau_w1 / rho)
    gradient_displaced_1 = np.gradient(u[1:], y_displaced, edge_order=2) # du/dr 
    Tau_w1_displaced = mu * gradient_displaced_1[0]
    u_tau1_displaced = np.sqrt(Tau_w1_displaced / rho)
    y_plus_1 = u_tau1 * y / nu 
    y_plus_1_displaced = u_tau1_displaced * y_displaced / nu 


    R = max(y) 
    r = R - y
    gradient_r = np.gradient(u, r, edge_order=2)
    A = np.pi*R**2
    u_m = np.mean(u)
    Re = 2 * R * u_m / nu
    f = 0.316 * Re **(-1/4)
    Tau_w2 = (1/8) * rho * f * (u_m**2)
    u_tau2 = np.sqrt(Tau_w2 / rho)
   
    Tau_w3, u_tau3 = gauss_newton(u_m, y, u)
    

    Tau_w3_displaced, u_tau3_displace = gauss_newton(u_m, y_displaced, u[1:])
    

    popt_1, _ = curve_fit(log_law, u_tau1*y[1:]/nu, u[1:]/u_tau1)
    popt_2, _ = curve_fit(log_law, u_tau2*y[1:]/nu, u[1:]/u_tau2)
    popt_3, _ = curve_fit(log_law, u_tau3*y[1:]/nu, u[1:]/u_tau3)

    fig, axs = plt.subplots(nrows=3, ncols=2, sharex=True, sharey=True)
    axs[0,0].semilogx(u_tau1*y[1:]/nu, (1/k)*np.log(u_tau1*y[1:]/nu) + B, 'k')
    axs[0,0].semilogx(u_tau1*y/nu, u/u_tau1, 'ro')
    axs[0,0].set_title(r"Gradient: $\tau_w={},~u_t={}$".format(round(Tau_w1, 2), round(u_tau1, 2)))
    axs[1,0].semilogx(u_tau2*y/nu, u/u_tau2, 'ro')
    axs[1,0].semilogx(u_tau2*y[1:]/nu, (1/k)*np.log(u_tau2*y[1:]/nu) + B, 'k')
    axs[1,0].set_title(r"Blasius: $\tau_w={},~u_t={}$".format(round(Tau_w2, 2), round(u_tau2, 2)))
    axs[1,0].set_ylabel(r"$U^{+}$", fontsize=FONTSIZE)
    axs[2,0].semilogx(u_tau3*y/nu, u/u_tau3, 'ro')
    axs[2,0].semilogx(u_tau3*y[1:]/nu, (1/k)*np.log(u_tau3*y[1:]/nu) + B, 'k')
    axs[2,0].set_title(r"Gauss-Newton: $\tau_w={},~u_t={}$".format(round(Tau_w3, 2), round(u_tau3, 2)))
    axs[2,0].set_xlabel(r"$y^{+}$", fontsize=FONTSIZE)

    axs[0, 1].semilogx(u_tau1*y/nu, u/u_tau1, 'ro')
    axs[0, 1].semilogx(u_tau1*y[1:]/nu, log_law(u_tau1*y[1:]/nu, *popt_1), 'k')
    axs[0, 1].set_title(r"Gradient: $\kappa={},~B={}$".format(round(popt_1[0], 2), round(popt_1[1], 2)))
    axs[1, 1].semilogx(u_tau2*y/nu, u/u_tau2, 'ro')
    axs[1, 1].semilogx(u_tau2*y[1:]/nu, log_law(u_tau2*y[1:]/nu, *popt_2), 'k')
    axs[1, 1].set_title(r"Blasius: $\kappa={},~B={}$".format(round(popt_2[0], 2), round(popt_2[1], 2)))
    axs[2, 1].semilogx(u_tau3*y/nu, u/u_tau3, 'ro')
    axs[2, 1].semilogx(u_tau3*y[1:]/nu, log_law(u_tau3*y[1:]/nu, *popt_3), 'k')
    axs[2, 1].set_title(r"Gauss-Newton: $\kappa={},~B={}$".format(round(popt_3[0], 2), round(popt_3[1], 2)))
    axs[2,1].set_xlabel(r"$y^{+}$", fontsize=FONTSIZE)
    
    fig.tight_layout()
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("fig_1.png", dpi=600, bbox_inches='tight', transparent=False)
    fig, axs = plt.subplots(nrows=3, ncols=1, sharex=True, sharey=True)
    axs[0].plot(r, -uv, 'ro')
    axs[0].plot(r, -nu*gradient_r -(Tau_w1*(r))/(rho*R), 'k') #)
    axs[0].legend([r"Measured $-\overline{u'v'}$", r"$-\overline{u'v'} = -\nu \frac{ \partial U}{\partial r} - \frac{\tau_w r}{R \rho}$"], fontsize=10)
    axs[0].ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    axs[0].set_title("Gradient", fontsize=FONTSIZE)
    axs[1].plot(r, -uv, 'ro')
    axs[1].plot(r, -nu*gradient_r -(Tau_w2*(r))/(rho*R), 'k') #)
    axs[1].set_ylabel(r"$-\overline{u'v'}$", fontsize=FONTSIZE)
    axs[1].set_title("Blasius", fontsize=FONTSIZE)
    axs[2].plot(r, -uv, 'ro')
    axs[2].plot(r, -nu*gradient_r -(Tau_w3*(r))/(rho*R), 'k') #)
    axs[2].set_xlabel(r"$r~[m]$", fontsize=FONTSIZE)
    axs[2].set_title("Gauss-Newton", fontsize=FONTSIZE)
    fig.tight_layout()
    # plt.figure()
    # plt.plot(r, -nu*gradient_r -(Tau_w1*(r))/(rho*R), 'k') #)
    # plt.plot(r, -uv, 'ro')
    # plt.legend([r"$-\overline{u'v'} = -\nu \frac{ \partial U}{\partial r} - \frac{\tau_w r}{R \rho}$", r"Measured $-\overline{u'v'}$"], fontsize=FONTSIZE)
    # plt.xlabel(r"$r~[m]$", fontsize=FONTSIZE)
    # plt.ylabel(r"$-\overline{u'v'}$", fontsize=FONTSIZE)
    # plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig("fig_2.png", dpi=600, transparent=False)
    # plt.show()
    print(f"Wall shear - gradient: {Tau_w1}")
    print(f"Wall shear - Blasius: {Tau_w2}")
    print(f"Wall shear - Gauss-Newton: {Tau_w3}") 
    print(f"Wall shear displaced- gradient: {Tau_w1_displaced}")
    print(f"Wall shear displaced - Gauss-Newton: {Tau_w3_displaced}")

def gauss_newton(u_m, y, u):
    u_tau_0 = u_m / 25 # Ungefär Re = 8 * 10^5 i fig 7.12
    u_tau_1 = 1
    residual = 1
    diff = 1
    while abs(diff) > 0.001:
        residual = u[1:] - (u_tau_0 /k) * np.log(y[1:] * u_tau_0 / nu) - B*u_tau_0
        jakobian = (-1/k) * np.log(y[1:] * u_tau_0 / nu) - 1/k - B
        u_tau_1 = u_tau_0 - (1 / np.matmul(np.transpose(jakobian),jakobian)) * np.matmul(np.transpose(jakobian), residual)
        diff = u_tau_1 - u_tau_0
        u_tau_0 = u_tau_1

    Tau_w3 = u_tau_1**2 * rho
    u_tau3 = u_tau_1
    return Tau_w3, u_tau3

def log_law(y_plus, k, B):
    return (1/k)*np.log(y_plus)+B


if __name__ == "__main__":
    main()