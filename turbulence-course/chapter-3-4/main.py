from scipy.io import loadmat as loadmat
from scipy import signal
from scipy import stats
from matplotlib import rc 

import matplotlib.pyplot as plt
import numpy as np

AUTO_CORRELATION = True

def autocor(x):
    '''manualy compute, non partial'''
    s = range(len(x))
    mean=np.mean(x)
    var=np.var(x)
    xp=x-mean
    corr=[1. if l==0 else np.sum(xp[l:]*xp[:-l])/(len(x)) / var for l in s]
    # corr = np.nan_to_num(corr)
    return np.array(corr)


def time_scale(x):
    index = 10
    return np.sum(autocor(x[:index])/ (np.var(x) * 75)) # This one is probably correct but plot is ugly
    # return np.sum(correlation(x[:index]) / (75))

def correlation(x):
    # a = np.correlate(x, x)
    # a = signal.correlate(x,x)
    result = np.correlate(x, x, mode='full')
    index = int(len(result)/2 + 1)
    # print(result[index])   
    return result[index:]
    # return a #/ np.max(a)

def length_scale(x):
    index = len(x) 
    return np.sum(autocor(x[:index]) * 6 * 10**-3 / np.var(x))


def time_averaging(x):
    return np.mean(x, 2)


def space_averaging(x):
    return np.mean(x, 0)

def main():
    rc('text', usetex=True)
    data = loadmat("PIV_data_fixed.mat")
    
    u1 = np.nan_to_num(data['u'])[:115, :125, :]
    u2 = np.nan_to_num(data['u_2'])[:115, :125, :]
    v1 = np.nan_to_num(data['v'])[:115, :125, :]
    v2 = np.nan_to_num(data['v_2'])[:115, :125]
    xg1 = np.nan_to_num(data['xg'])[:115, :125]
    xg2 = np.nan_to_num(data['xg_2'])[:115, :125]
    yg1 = np.nan_to_num(data['yg'])[:115, :125]
    yg2 = np.nan_to_num(data['yg_2'])[:115, :125]

    magnitude_1 = np.sqrt(np.multiply(u1, u1) + np.multiply(v1, v1))
    magnitude_2 = np.sqrt(np.multiply(u2, u2) + np.multiply(v2, v2))
    average_vel_1 = time_averaging(magnitude_1)
    average_u_1 = time_averaging(u1)
    average_v_1 = time_averaging(v1)
    point = (80, 80)
    u = u1[point[0], point[1], :]
    corr = autocor(u)

    fig = plt.figure(figsize=(10,8))
    grid = plt.GridSpec(3, 3, wspace=0.4, hspace=0.3)
    plot_1 = fig.add_subplot(grid[0,:])
    plot_2 = fig.add_subplot(grid[1,:])
    plot_3 = fig.add_subplot(grid[2,0])
    plot_4 = fig.add_subplot(grid[2,1])
    plot_5 = fig.add_subplot(grid[2,2])
    plot_1.plot(u - np.mean(u))
    plot_1.set_title(r"$u_{fluc}$")
    plot_1.set_ylabel(r"$m/s$")
    plot_1.set_xlabel(r"$t$")

    plot_2.plot(corr)
    plot_2.set_title("Auto correlation")
    plot_2.set_ylabel(r"$\rho (s)$")
    plot_2.set_xlabel(r"$s$")

    plot_3.imshow(average_vel_1.T, origin="lower", cmap="jet")
    plot_3.set_title(r"$\langle U\rangle$")
    plot_3.scatter(point[0], point[1], c='black')

    plot_4.imshow(average_u_1.T, origin="lower", cmap="jet")
    plot_4.scatter(point[0], point[1], c='black')
    plot_4.set_title(r"$\langle u \rangle$")

    plot_5.imshow(average_v_1.T, origin="lower", cmap="jet")
    plot_5.scatter(point[0], point[1], c='black')    
    plot_5.set_title(r"$\langle v \rangle$")

    point = (50, 40)
    u = u1[point[0], point[1], :]
    corr = autocor(u)

    fig = plt.figure(figsize=(10,8))
    grid = plt.GridSpec(3, 3, wspace=0.4, hspace=0.3)
    plot_1 = fig.add_subplot(grid[0,:])
    plot_2 = fig.add_subplot(grid[1,:])
    plot_3 = fig.add_subplot(grid[2,0])
    plot_4 = fig.add_subplot(grid[2,1])
    plot_5 = fig.add_subplot(grid[2,2])
    plot_1.plot(u - np.mean(u))
    plot_1.set_title(r"$u_{fluc}$")
    plot_1.set_ylabel(r"$m/s$")
    plot_1.set_xlabel(r"$t$")

    plot_2.plot(corr)
    plot_2.set_title("Auto correlation")
    plot_2.set_ylabel(r"$\rho (s)$")
    plot_2.set_xlabel(r"$s$")

    plot_3.imshow(average_vel_1.T, origin="lower", cmap="jet")
    plot_3.set_title(r"$\langle U\rangle$")
    plot_3.scatter(point[0], point[1], c='black')

    plot_4.imshow(average_u_1.T, origin="lower", cmap="jet")
    plot_4.scatter(point[0], point[1], c='black')
    plot_4.set_title(r"$\langle u \rangle$")

    plot_5.imshow(average_v_1.T, origin="lower", cmap="jet")
    plot_5.scatter(point[0], point[1], c='black')    
    plot_5.set_title(r"$\langle v \rangle$")

    fig, (ax1, ax2) = plt.subplots(2,1, figsize=(4,10))
    fig.suptitle("Double averaged velocity profiles")
    ax2.plot(space_averaging(time_averaging(magnitude_1)), yg1[0,:])
    ax2.set_ylabel(r"$y$")
    ax2.set_xlabel(r"$\langle \overline{U} \rangle$")
    ax2.set_title(r"Lower part")
    ax1.plot(space_averaging(time_averaging(magnitude_2)), yg2[0,:])
    ax1.set_ylabel(r"$y$")
    ax1.set_xlabel(r"$\langle \overline{U} \rangle$")
    ax1.set_title(r"Upper part")

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2, figsize=(10,8))
    fig.suptitle("Standard deviation")
    ax1.imshow(np.apply_along_axis(np.nanstd, 2, u1).T, origin="lower", cmap="jet")
    ax1.set_title("$u_1$")
    ax2.imshow(np.apply_along_axis(np.nanstd, 2, v1).T, origin="lower", cmap="jet")
    ax2.set_title("$v_1$")
    ax3.imshow(np.apply_along_axis(np.nanstd, 2, u2).T, origin="lower", cmap="jet")
    ax3.set_title("$u_2$")
    ax4.imshow(np.apply_along_axis(np.nanstd, 2, v2).T, origin="lower", cmap="jet")
    ax4.set_title("$v_2$")


    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2, figsize=(10,8))
    fig.suptitle("Kurtosis")
    ax1.imshow(np.apply_along_axis(stats.kurtosis, 2, u1).T, origin="lower", cmap="jet")
    ax1.set_title("$u_1$")
    ax2.imshow(np.apply_along_axis(stats.kurtosis, 2, v1).T, origin="lower", cmap="jet")
    ax2.set_title("$v_1$")
    ax3.imshow(np.apply_along_axis(stats.kurtosis, 2, u2).T, origin="lower", cmap="jet")
    ax3.set_title("$u_2$")
    ax4.imshow(np.apply_along_axis(stats.kurtosis, 2, v2).T, origin="lower", cmap="jet")
    ax4.set_title("$v_2$")


    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2, figsize=(10,8))
    fig.suptitle("Skewness")
    ax1.imshow(np.apply_along_axis(stats.skew, 2, u1).T, origin="lower", cmap="jet")
    ax1.set_title("$u_1$")
    ax2.imshow(np.apply_along_axis(stats.skew, 2, v1).T, origin="lower", cmap="jet")
    ax2.set_title("$v_1$")
    ax3.imshow(np.apply_along_axis(stats.skew, 2, u2).T, origin="lower", cmap="jet")
    ax3.set_title("$u_2$")
    ax4.imshow(np.apply_along_axis(stats.skew, 2, v2).T, origin="lower", cmap="jet")
    ax4.set_title("$v_2$")


    if AUTO_CORRELATION:
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2, figsize=(10,8))
        fig.suptitle("Integral timescale")
        ax1.imshow(np.apply_along_axis(time_scale, 2, u1).T, origin="lower", cmap="jet")
        ax1.set_title("$u_1$")

        ax2.imshow(np.apply_along_axis(time_scale, 2, v1).T, origin="lower", cmap="jet")
        ax2.set_title("$v_1$")

        ax3.imshow(np.apply_along_axis(time_scale, 2, u2).T, origin="lower", cmap="jet")
        ax3.set_title("$u_2$")

        ax4.imshow(np.apply_along_axis(time_scale, 2, v2).T, origin="lower", cmap="jet")
        ax4.set_title("$v_2$")

        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2, figsize=(10,8))
        fig.suptitle("Integral lengthscale")
        ax1.plot(np.mean(np.apply_along_axis(length_scale, 0, u1), axis=1), yg1[0,:])
        ax1.set_title("$u_1$")

        ax2.plot(np.mean(np.apply_along_axis(length_scale, 0, v1), axis=1), yg1[0,:])
        ax2.set_title("$v_1$")

        ax3.plot(np.mean(np.apply_along_axis(length_scale, 0, u2), axis=1), yg1[0,:])
        ax3.set_title("$u_2$")

        ax4.plot(np.mean(np.apply_along_axis(length_scale, 0, v2), axis=1), yg1[0,:])
        ax4.set_title("$v_2$")

    plt.show()


if __name__ == "__main__":
    main()