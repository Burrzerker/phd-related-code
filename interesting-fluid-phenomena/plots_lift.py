import numpy as np
import argparse
import matplotlib.pyplot as plt
import os
import pickle
import pandas as pd 
from matplotlib import rc 
from matplotlib.colors import ListedColormap
from scipy.io import loadmat

def main():
    rc('text', usetex=True)
    plt.rcParams["font.family"] = "Times New Roman"

    data_24 = pd.read_csv("lift-24.csv", sep=',') 
    data_20 = pd.read_csv("lift-20.csv", sep=',') 
    data_15 = pd.read_csv("lift-15.csv", sep=',') 
    data_5 = pd.read_csv("lift-5.csv", sep=',')
    time = data_5.iloc[1:, 0]
    
    plt.figure()
    plt.title("Lift x-component", fontsize=14)
    plt.plot(time, data_5.iloc[1:, 1], 'x')
    plt.plot(time, data_15.iloc[1:, 1], 'd')
    plt.plot(time, data_20.iloc[1:, 1], 's')
    plt.plot(time, data_24.iloc[1:, 1], '^')
    plt.ylabel("Lift $[N]$", fontsize=14)
    plt.xlabel("Time $[s]$", fontsize=14)
    plt.legend(['5 $m/s$', '15 $m/s$', '20 $m/s$', '24 $m/s$'])
    plt.savefig(f"x_lift.png", dpi=600, bbox_inches='tight', transparent=False)

    plt.figure()
    plt.title("Lift z-component", fontsize=14)
    plt.plot(time, data_5.iloc[1:, 2], 'x')
    plt.plot(time, data_15.iloc[1:, 2], 'd')
    plt.plot(time, data_20.iloc[1:, 2], 's')
    plt.plot(time, data_24.iloc[1:, 2], '^')
    plt.ylabel("Lift $[N]$", fontsize=14)
    plt.xlabel("Time $[s]$", fontsize=14)
    plt.legend(['5 $m/s$', '15 $m/s$', '20 $m/s$', '24 $m/s$'])
    plt.savefig(f"z_lift.png", dpi=600, bbox_inches='tight', transparent=False)

    plt.figure()
    plt.title("Lift y-component", fontsize=14)
    plt.plot(time, data_5.iloc[1:, 3], 'x')
    plt.plot(time, data_15.iloc[1:, 3], 'd')
    plt.plot(time, data_20.iloc[1:, 3], 's')
    plt.plot(time, data_24.iloc[1:, 3], '^')
    plt.ylabel("Lift $[N]$", fontsize=14)
    plt.xlabel("Time $[s]$", fontsize=14)
    plt.legend(['5 $m/s$', '15 $m/s$', '20 $m/s$', '24 $m/s$'])
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig(f"y_lift.png", dpi=600, bbox_inches='tight', transparent=False)
    # plt.show()

if __name__ == "__main__":
    main()