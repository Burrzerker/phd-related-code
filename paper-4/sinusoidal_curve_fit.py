import numpy as np
import scipy.optimize
import scipy.fftpack
import scipy.signal
from scipy.io import loadmat
from matplotlib import pyplot as plt
from matplotlib import rc
def main():
    rc('text', usetex=True)
    vel = loadmat("velocity")["velocity"][0][58:]
    level = loadmat("water_level")["level"][0][58:]
    t = np.linspace(0, 10*np.pi, len(level))
    # t_clausen = np.linspace(0, 300, 1000)
    t_real = np.linspace(0, len(level), len(level))
    plt.figure()
    plt.plot(level[30:])
    plt.plot(1.125 + 0.096*clausen(10, t))
    plt.plot(1.125 + 0.096*clausen(50, t))
    plt.plot(1.125 + 0.096*clausen(100, t))
    # plt.plot(np.gradient(level))
    # plt.plot(vel)
    vel_one_period = vel[30:90]
    level_one_period = level[34:93]
    # frequency = 48
    # omega = 0.015
    # offset = 10
    # t = np.linspace(0, 150, 1500)
    # noise = np.random.normal(0, 0.5, t.shape)
    # sine = np.sin(2 * np.pi *t / frequency)
    # dispersion = np.sin(2 * np.pi *t / frequency + omega*t)
    # signal = offset + dispersion + noise
    
    # popt, cov = scipy.optimize.curve_fit(predicted_function, t, signal, p0=[9.5, 50, 0.02])
    # offset, frequency, omega = popt 
    # print(popt)
    # curve_fit = predicted_function(t, offset, frequency, omega)
    # plt.figure()
    # # plt.plot(t, sine)
    # plt.plot(t, offset + dispersion)
    # plt.plot(t, curve_fit)
    # # plt.plot(t, signal, 'x')
    

    

    
    phi = np.linspace(0, 2*np.pi, 1000)
    phi_1 = np.linspace(0, 2*np.pi, len(level_one_period))
    clausen_100 = clausen(100, phi)
    # plt.figure()
    # plt.plot(power_series(10, phi), 'x')
    # for index in range(1, 100):
    #     if index % 10 == 0:
    #         plt.plot(power_series(index, phi))
    # test_series = power_series(100, phi)
    
    # plt.figure()
    # plt.plot(test_series)

    plt.figure()
    # plt.plot(t, level)
    plt.plot(phi_1 / (2*np.pi), level_one_period, 'x')
    plt.plot(phi/ (2*np.pi), 1.125 + 0.096*clausen_100)
    plt.plot(phi / (2*np.pi), 1.125 + 0.096*np.sin(phi))
    plt.plot(phi / (2*np.pi), 1.125 + 0.096*(4*clausen_100/6+2*np.sin(phi)/6))
    plt.plot(phi / (2*np.pi), 1.125 + 0.096*np.sin(phi + np.sin(phi)/3))

    plt.legend([r"Simulation", r"$Cl_2(\phi) = \sum _{n=1}^{100} sin(n \phi)/n^2$", r"$sin(\phi)$", r"$2Cl_2(\phi)/3+sin(\phi)/3$"])
    plt.xlabel(r"One period")
    plt.ylabel(r"Water level")
 
    plt.show()

def predicted_function(t, offset, frequency, omega):
    return offset + np.sin(2*np.pi*t / frequency + omega*t)

def clausen(n, phi):
    # phi = np.linspace(0, 10*np.pi, 1000)
    clausen = np.sin(phi)
    for index in range(n):
        index += 2
        clausen += np.sin(index*phi)/(index**2)
    return clausen

def power_series(n, phi):
    value = 0
    for index in range(1, n):
        value += np.sin(phi + np.sin(phi)/n)
    return value

if __name__ == "__main__":
    main()