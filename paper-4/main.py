import matplotlib.pyplot as plt
import support_functions as sf
import numpy as np
import scipy.optimize
from scipy.io import savemat

from matplotlib import rc 

PATH_TO_DATA ="/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/50-21-24-start-stop"
BACKGROUND_IMAGE = "../paper-2/background.tif"
def main():
    rc('text', usetex=True)
    plt.rcParams["font.family"] = "Times New Roman"

    X, Y, level = sf.read_coordinate_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/results/start-stop/24", 'water_depth')
    _, _, vel = sf.read_coordinate_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/results/start-stop/24", 'depth_averaged_velocity')
    background = sf.read_background_image(BACKGROUND_IMAGE)
    
    plot_level = level[:, 1200, 100]
    plot_velocity = vel[:, 1200, 100]
    savemat("water_level", {"level": plot_level})
    savemat("velocity", {"velocity": plot_velocity})
    plot_time = np.linspace(0, len(level[:, 0, 0]), len(level[:, 0, 0]))

    # fig, ax = plt.subplots(1, 1)
    # ax.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    # ax.contourf(X, Y, plot_level, levels=np.linspace(0, 2, 200), extend="both")
    # ax.set_xticks([], [])
    # ax.set_yticks([], [])

    test = level[57:, 1200, 100]
    test_time = np.linspace(0, len(level[57:, 0, 0]), len(level[57:, 0, 0]))
    p0=[1, 0.1, 24]
    # popt,cov = optimize.curve_fit(predicted_function, test_time, test)
    # print(popt)
    # y_offset, amplitude, frequency, omega, phase = popt
    # temp = predicted_function(test_time, y_offset, amplitude, frequency, omega, phase)
    # amplitude=0.1


    # popt,cov = scipy.optimize.curve_fit(test_function, test_time, test, p0=p0)
    asd = scipy.optimize.least_squares(test_function, x0=p0, args=(y_offset, amplitude, frequency))
    print(asd)
    y_offset, amplitude, frequency, = popt
    temp = test_function(test_time, y_offset, amplitude, frequency)

    plt.figure()
    plt.plot(test_time, test)
    plt.plot(test_time, temp)
    # plt.plot(test_time, 1.125 + 0.1*np.sin((test_time * 24  + 20/(2*np.pi))))
    plt.plot(test_time, 1.125 + 0.1*np.sin(np.pi*test_time / 24))
    # plt.plot(plot_time, 1.125 + 0.1*np.sin((plot_time / 2*np.pi*24 + plot_time*omega) + 20/(2*np.pi)))
    plt.legend(["Simulation", "Sine function"])
    plt.xlabel("Time in minutes")
    plt.ylabel("Depth [m]")

    # plt.figure()
    # # plt.plot(np.fft.fft(level[:40, 1200, 100]))
    # plt.plot(np.fft.fft(test).imag)
    # # plt.xlim([5, 50])
    # # plt.ylim([-1, 1])
    # # plt.plot(plot_time, 1.125 + 0.1*np.sin((plot_time / 2*np.pi*24) + 20/(2*np.pi)))
    # plt.legend(["Simulation", "Sine function"])
    # # plt.xlabel("Time in minutes")
    # plt.ylabel("Depth [m]")

    # plt.figure()
    # plt.plot(plot_time, vel[:, 1200, 100])
    # # plt.plot(plot_time, 1.125 + 0.1*np.sin((plot_time / 2*np.pi*24) + 20/(2*np.pi)))
    # plt.legend(["Simulation", "Sine function"])
    # plt.xlabel("Time in minutes")
    # plt.ylabel("Velocity [m/s]")
    plt.show()

def test_function(t, y_offset, amplitude, frequency):
    return y_offset + amplitude*np.sin(np.pi*t / (frequency))

def predicted_function(t, y_offset, amplitude, frequency, omega, phase):
    return y_offset + amplitude*np.cos((2*np.pi*t / (frequency) + t*omega) + phase)


if __name__ == "__main__":
    main()