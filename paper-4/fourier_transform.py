import numpy as np
from matplotlib import pyplot as plt
from scipy.io import loadmat
import scipy.fftpack

def main():
    vel = loadmat("velocity")["velocity"][0][58:]
    level = loadmat("water_level")["level"][0][58:]
    t = np.linspace(0, len(level), len(level))

    y = np.sin(2*np.pi*5*t) + np.sin(2*np.pi*10*t) + np.sin(2*np.pi*20*t)

    n = len(y)
    T = 1
    xf = scipy.fftpack.fftfreq(n, T)[:n//2]
    Y = np.fft.fft(level)

    Y_10 = np.copy(Y)
    np.put(Y_10, range(10, n), 0)
    ifft_10 = np.fft.ifft(Y_10).real

    Y_50 = np.copy(Y)
    np.put(Y_50, range(50, n), 0)
    ifft_50 = np.fft.ifft(Y_50).real
    
    Y_100 = np.copy(Y)
    np.put(Y_100, range(100, n), 0)
    ifft_100 = np.fft.ifft(Y_100).real
    
    Y_200 = np.copy(Y)
    np.put(Y_200, range(200, n), 0)
    ifft_200 = np.fft.ifft(Y_200).real
    
    Y_n = np.copy(Y)
    np.put(Y_n, range(n, n), 0)
    ifft_n = np.fft.ifft(Y_n).real
    
    plt.figure()
    plt.plot(xf, (2/n)*np.abs(Y_10[0:n//2]), 'x')
    plt.plot(xf, (2/n)*np.abs(Y_50[0:n//2]), 'd')
    plt.plot(xf, (2/n)*np.abs(Y_100[0:n//2]), 'o')
    plt.plot(xf, (2/n)*np.abs(Y_200[0:n//2]), '^')
    plt.plot(xf, (2/n)*np.abs(Y_n[0:n//2]), 'p')
    plt.legend(["$n=10$", "$n=50$", "$n=100$", "$n=len(t)$"])


    plt.figure()
    plt.plot(t, ifft_10, 'x')
    plt.plot(t, ifft_50, 'd')
    plt.plot(t, ifft_100, 'o')
    plt.plot(t, ifft_200, '^')
    plt.plot(t, ifft_n, 'p')
    plt.plot(t, level, label="Original dataset")
    plt.grid(linestyle='dashed')
    plt.legend(["$n=10$", "$n=50$", "$n=100$", "$n=200$", "$n=len(t)$", "Original dataset"])
    plt.show()

if __name__ == "__main__":
    main()