echo "Bash script that will create spawning habit area data from area_spawning_habitats.py script!"
# python dry_spawning_habitats.py --case 24 --species salmon
# python dry_spawning_habitats.py --case 24 --species trout
# python dry_spawning_habitats.py --case 24 --species grayling

# python dry_spawning_habitats.py --case 29 --species salmon
# python dry_spawning_habitats.py --case 29 --species trout
# python dry_spawning_habitats.py --case 29 --species grayling

# python dry_spawning_habitats.py --case 36 --species salmon
# python dry_spawning_habitats.py --case 36 --species trout
# python dry_spawning_habitats.py --case 36 --species grayling

# python dry_spawning_habitats.py --case 48 --species salmon
# python dry_spawning_habitats.py --case 48 --species trout
# python dry_spawning_habitats.py --case 48 --species grayling

# python dry_spawning_habitats.py --case 72 --species salmon
# python dry_spawning_habitats.py --case 72 --species trout
# python dry_spawning_habitats.py --case 72 --species grayling

# python dry_spawning_habitats.py --case 144 --species salmon
# python dry_spawning_habitats.py --case 144 --species trout
# python dry_spawning_habitats.py --case 144 --species grayling

# python area_spawning_habitats.py --case 24 --species salmon
# python area_spawning_habitats.py --case 24 --species trout
# python area_spawning_habitats.py --case 24 --species grayling

# python area_spawning_habitats.py --case 29 --species salmon
# python area_spawning_habitats.py --case 29 --species trout
# python area_spawning_habitats.py --case 29 --species grayling

# python area_spawning_habitats.py --case 36 --species salmon
# python area_spawning_habitats.py --case 36 --species trout
# python area_spawning_habitats.py --case 36 --species grayling

# python area_spawning_habitats.py --case 48 --species salmon
# python area_spawning_habitats.py --case 48 --species trout
# python area_spawning_habitats.py --case 48 --species grayling

# python area_spawning_habitats.py --case 72 --species salmon
# python area_spawning_habitats.py --case 72 --species trout
# python area_spawning_habitats.py --case 72 --species grayling

# python area_spawning_habitats.py --case 144 --species salmon
# python area_spawning_habitats.py --case 144 --species trout
# python area_spawning_habitats.py --case 144 --species grayling

python area_spawning_habitats.py --case 24 --species salmon
python area_spawning_habitats.py --case 24 --species trout
python area_spawning_habitats.py --case 24 --species grayling