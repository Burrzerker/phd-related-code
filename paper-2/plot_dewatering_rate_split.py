import numpy as np
import argparse
import matplotlib.pyplot as plt
import os
import pickle
import support_functions as sf
from matplotlib import rc 
from matplotlib.colors import ListedColormap
from scipy.io import loadmat

# PATH_TO_DATA = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/results"
# BACKGROUND_IMAGE = "../paper-1/geo_ref.tif"
BACKGROUND_IMAGE = "background.tif"

def parse_arguments():
    parser = argparse.ArgumentParser(description='Arguments for selecting cases and species.')
    parser.add_argument('--case', action='store', type=int, required=True)
    parser.add_argument('--level', action='store', type=int, required=True)

    args = parser.parse_args()
    return args

def main():
    args = parse_arguments()
    rc('text', usetex=True)
    X, Y, _ = sf.read_coordinate_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/results/start-stop/24", 'water_level')
    background = sf.read_background_image(BACKGROUND_IMAGE)

    dewatering_rate = loadmat(f"cm_dewatering_rate_hours_{args.case}.mat")['dewatering_rate']
    dewatering_rate = np.multiply(dewatering_rate, 1/60)
    
    custom_cmap = ListedColormap(["darkgreen", "limegreen", "yellow", "darkorange", "red"])

    levels = args.level
    if levels == 1:
        # Larval brown trout / grayling
        levels_cmap = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
        levels_ticks = [0.15, 0.25, 0.35, 0.45, 0.55, 0.65]
        text = "larval grayling/brown trout at daylight"
    if levels == 2:
        # Juvenile Grayling
        levels_cmap = [0, 1.0, 1.2, 2.0, 3.0, 3.2]
        levels_ticks = [0.5, 1.1, 1.6, 2.5, 3.1, 3.3]
        text = "juvenile grayling at daylight"
    if levels == 3:
        # Juvenile brown trout
        levels_cmap = [1, 1.5, 3, 4.5, 6, 7]
        levels_ticks = [1.25, 2.25, 3.75, 5.25, 6.5, 7.5]
        text = "juvenile brown trout at daylight"

    x_max = np.amax(X)
    y_max = np.amax(Y)
    x_min = np.amin(X)
    y_min = np.amin(Y)
    print(x_min + (x_max - x_min)/4)
    print(y_min + (y_max - y_min)/4)

if __name__ == "__main__":
    main()