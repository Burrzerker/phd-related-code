import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import os
import findiff
import support_functions as sf
from matplotlib import rc 
# from matplotlib.lines import Line2D
PATH_TO_DATA = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/results"
BACKGROUND_IMAGE = "../paper-1/geo_ref.tif"

def main():


    rc('text', usetex=True)
    X, Y, _ = sf.read_coordinate_data(PATH_TO_DATA + "/start-stop/24", 'water_level')
    level = sf.read_simulation_data(PATH_TO_DATA + "/50-21-30", 'water_level')
    background = sf.read_background_image(BACKGROUND_IMAGE)
    
    steady_state_50 = level[0]
    steady_state_50[~np.isnan(steady_state_50)] = 1
    steady_state_50[np.isnan(steady_state_50)] = 0
    steady_state_21 = level[-1]    
    steady_state_21[~np.isnan(steady_state_21)] = 1
    steady_state_21[np.isnan(steady_state_21)] = 0
    z = steady_state_50 - steady_state_21
    z[z==0] = np.nan
    
    level = sf.read_simulation_data(PATH_TO_DATA + "/50-21-30", 'water_level')
    
    mask = np.ma.masked_invalid(z).mask
    sf.process_dewatering_matrix(level, mask)


if __name__ == "__main__":
    main()