import numpy as np
import argparse
import matplotlib.pyplot as plt
import os
import pickle
import support_functions as sf
from matplotlib import rc 
from matplotlib.colors import ListedColormap
from scipy.io import loadmat

# PATH_TO_DATA = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/results"
# BACKGROUND_IMAGE = "../paper-1/geo_ref.tif"
BACKGROUND_IMAGE = "background.tif"

def parse_arguments():
    parser = argparse.ArgumentParser(description='Arguments for selecting cases and species.')
    parser.add_argument('--case', action='store', type=int, required=True)
    parser.add_argument('--level', action='store', type=int, required=True)

    args = parser.parse_args()
    return args

def main():
    args = parse_arguments()
    rc('text', usetex=True)
    plt.rcParams["font.family"] = "Times New Roman"

    X, Y, _ = sf.read_coordinate_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/results/start-stop/24", 'water_level')
    background = sf.read_background_image(BACKGROUND_IMAGE)

    dewatering_rate = loadmat(f"cm_dewatering_rate_hours_{args.case}.mat")['dewatering_rate']
    dewatering_rate = np.multiply(dewatering_rate, 1/60)
    
    custom_cmap = ListedColormap(["darkgreen", "limegreen", "yellow", "darkorange", "red"])

    levels = args.level
    if levels == 1:
        # Larval brown trout / grayling
        levels_cmap = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
        levels_ticks = [0.15, 0.25, 0.35, 0.45, 0.55, 0.65]
        text = "larval grayling/brown trout at daylight"
    if levels == 2:
        # Juvenile Grayling
        levels_cmap = [0, 1.0, 1.2, 2.0, 3.0, 3.2]
        levels_ticks = [0.5, 1.1, 1.6, 2.5, 3.1, 3.3]
        text = "juvenile grayling at daylight"
    if levels == 3:
        # Juvenile brown trout
        levels_cmap = [1, 1.5, 3, 4.5, 6, 7]
        levels_ticks = [1.25, 2.25, 3.75, 5.25, 6.5, 7.5]
        text = "juvenile brown trout at daylight"

    compute_area = False
    if compute_area:

        total_number_of_nodes = np.count_nonzero(~np.isnan(dewatering_rate))
        if levels == 1:
            number_of_very_good_nodes = (dewatering_rate <= 0.2).sum()
            
            good_mask = np.ma.masked_outside(dewatering_rate, 0.2, 0.3)
            number_of_good_nodes = total_number_of_nodes - np.ma.count_masked(good_mask)
                        
            moderate_mask = np.ma.masked_outside(dewatering_rate, 0.3, 0.4)
            number_of_moderate_nodes = total_number_of_nodes - np.ma.count_masked(moderate_mask)

            unsatisfactory_mask = np.ma.masked_outside(dewatering_rate, 0.4, 0.5)
            number_of_unsatisfactory_nodes = total_number_of_nodes - np.ma.count_masked(unsatisfactory_mask)

            number_of_bad_nodes = (dewatering_rate > 0.5).sum()
        
        if levels == 2:
            number_of_very_good_nodes = (dewatering_rate <= 1).sum()
            
            good_mask = np.ma.masked_outside(dewatering_rate, 1, 1.2)
            number_of_good_nodes = total_number_of_nodes - np.ma.count_masked(good_mask)
                        
            moderate_mask = np.ma.masked_outside(dewatering_rate, 1.2, 2)
            number_of_moderate_nodes = total_number_of_nodes - np.ma.count_masked(moderate_mask)

            unsatisfactory_mask = np.ma.masked_outside(dewatering_rate, 2, 3)
            number_of_unsatisfactory_nodes = total_number_of_nodes - np.ma.count_masked(unsatisfactory_mask)

            number_of_bad_nodes = (dewatering_rate > 3).sum()    

        if levels == 3:
            number_of_very_good_nodes = (dewatering_rate <= 1.5).sum()
            
            good_mask = np.ma.masked_outside(dewatering_rate, 1.5, 3)
            number_of_good_nodes = total_number_of_nodes - np.ma.count_masked(good_mask)
                        
            moderate_mask = np.ma.masked_outside(dewatering_rate, 3, 4.5)
            number_of_moderate_nodes = total_number_of_nodes - np.ma.count_masked(moderate_mask)

            unsatisfactory_mask = np.ma.masked_outside(dewatering_rate, 4.5, 6)
            number_of_unsatisfactory_nodes = total_number_of_nodes - np.ma.count_masked(unsatisfactory_mask)

            number_of_bad_nodes = (dewatering_rate > 6).sum()
        print("==" * 50)
        print(f"{args.case} minute closing time, {text}")
        print(f"Total number of dewatered nodes: {total_number_of_nodes}")
        print(f"Number of very good nodes: {number_of_very_good_nodes}, Corresponding area: {100 * number_of_very_good_nodes / total_number_of_nodes:.2f} %")
        print(f"Number of good nodes: {number_of_good_nodes}, Corresponding area: {100 * number_of_good_nodes / total_number_of_nodes:.2f} %")
        print(f"Number of moderate nodes: {number_of_moderate_nodes}, Corresponding area: {100 * number_of_moderate_nodes / total_number_of_nodes:.2f} %")
        print(f"Number of unsatisfactory nodes: {number_of_unsatisfactory_nodes}, Corresponding area: {100 * number_of_unsatisfactory_nodes / total_number_of_nodes:.2f} %")    
        print(f"Number of bad nodes: {number_of_bad_nodes}, Corresponding area: {100 * number_of_bad_nodes / total_number_of_nodes:.2f} %") 
        result = {
            "very good": 100 * number_of_very_good_nodes / total_number_of_nodes,
            "good": 100 * number_of_good_nodes / total_number_of_nodes,
            "moderate": 100 * number_of_moderate_nodes / total_number_of_nodes,
            "unsatisfactory": 100 * number_of_unsatisfactory_nodes / total_number_of_nodes,
            "bad": 100 * number_of_bad_nodes / total_number_of_nodes
        }
        pickle.dump(result, open(f"area_eco_status/status_{args.level}_{args.case}.p", "wb"))
        # for index in range(len(area_array)):
        #     print("index: {}, area: {}".format(index, area_array[index]))
        # print(np.sum(area_array))
        # np.savetxt("status_areas_level_3_1.csv", area_array, delimiter=",")
    x_max_1 = 749826
    x_min_1 = 749273
    y_max_1 = 7.0916*10**6
    y_min_1 = 7.09125*10**6

    x_max_2 = 750970
    x_min_2 = 750333
    y_max_2 = 7.09007*10**6
    y_min_2 = 7.08948*10**6

    x_max_3 = 751847
    x_min_3 = 750879
    y_max_3 = 7.08974*10**6
    y_min_3 = 7.08898*10**6

    individual_plots = True
    combined_plots = False

    if individual_plots:
        fig, ax = plt.subplots(1,1)
        ax.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
        ax.set_xticks([], [])
        ax.set_yticks([], [])
        plt.hlines(y_max_1, x_min_1, x_max_1, 'w')
        plt.hlines(y_min_1, x_min_1, x_max_1, 'w')
        plt.vlines(x_max_1, y_max_1, y_min_1, 'w')
        plt.vlines(x_min_1, y_max_1, y_min_1, 'w')
        plt.hlines(y_max_2, x_min_2, x_max_2, 'w')
        plt.hlines(y_min_2, x_min_2, x_max_2, 'w')
        plt.vlines(x_max_2, y_max_2, y_min_2, 'w')
        plt.vlines(x_min_2, y_max_2, y_min_2, 'w')
        plt.hlines(y_max_3, x_min_3, x_max_3, 'w')
        plt.hlines(y_min_3, x_min_3, x_max_3, 'w')
        plt.vlines(x_max_3, y_max_3, y_min_3, 'w')
        plt.vlines(x_min_3, y_max_3, y_min_3, 'w')
        c = ax.contourf(X, Y, dewatering_rate, levels = levels_cmap, extend="both", cmap=custom_cmap)
        cbar = fig.colorbar(c, orientation='vertical', aspect=50, ticks=levels_ticks)
        cbar.ax.set_yticklabels(["Very good", "Good", "Moderate", "Unsatisfactory", "Bad"])
        cbar.ax.axes.tick_params(length=0)
        mng = plt.get_current_fig_manager()
        mng.full_screen_toggle()
        plt.savefig(f"richard_1.png", dpi=600, bbox_inches='tight', transparent=False)

        fig, ax = plt.subplots(1,1)
        ax.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
        ax.set_xticks([], [])
        ax.set_yticks([], [])

        plt.xlim((x_min_1, x_max_1))
        plt.ylim((y_min_1, y_max_1))
        c = ax.contourf(X, Y, dewatering_rate, levels = levels_cmap, extend="both", cmap=custom_cmap)
        cbar = fig.colorbar(c, orientation='vertical', aspect=50, ticks=levels_ticks)
        cbar.ax.set_yticklabels(["Very good", "Good", "Moderate", "Unsatisfactory", "Bad"])
        cbar.ax.axes.tick_params(length=0)
        mng = plt.get_current_fig_manager()
        mng.full_screen_toggle()
        plt.savefig(f"richard_2.png", dpi=600, bbox_inches='tight', transparent=False)

        fig, ax = plt.subplots(1,1)
        ax.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
        ax.set_xticks([], [])
        ax.set_yticks([], [])
        # plt.xlim((750333, 750970))
        # plt.ylim((7.08948*10**6, 7.09007*10**6))
        plt.xlim((x_min_2, x_max_2))
        plt.ylim((y_min_2, y_max_2))
        c = ax.contourf(X, Y, dewatering_rate, levels = levels_cmap, extend="both", cmap=custom_cmap)
        cbar = fig.colorbar(c, orientation='vertical', aspect=50, ticks=levels_ticks)
        cbar.ax.set_yticklabels(["Very good", "Good", "Moderate", "Unsatisfactory", "Bad"])
        cbar.ax.axes.tick_params(length=0)
        mng = plt.get_current_fig_manager()
        mng.full_screen_toggle()
        plt.savefig(f"richard_3.png", dpi=600, bbox_inches='tight', transparent=False)

        fig, ax = plt.subplots(1,1)
        ax.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
        ax.set_xticks([], [])
        ax.set_yticks([], [])
        # plt.xlim((750879, 751847))
        # plt.ylim((7.08889*10**6, 7.08974*10**6))
        plt.xlim((x_min_3, x_max_3))
        plt.ylim((y_min_3, y_max_3))
        c = ax.contourf(X, Y, dewatering_rate, levels = levels_cmap, extend="both", cmap=custom_cmap)
        cbar = fig.colorbar(c, orientation='vertical', aspect=50, ticks=levels_ticks)
        cbar.ax.set_yticklabels(["Very good", "Good", "Moderate", "Unsatisfactory", "Bad"])
        cbar.ax.axes.tick_params(length=0)
        mng = plt.get_current_fig_manager()
        mng.full_screen_toggle()
        plt.savefig(f"richard_4.png", dpi=600, bbox_inches='tight', transparent=False)
    if combined_plots:

        fig, axes = plt.subplots(2,2)
        ((ax1, ax2), (ax3, ax4)) = axes
        ax1.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
        ax1.set_xticks([], [])
        ax1.set_yticks([], [])
        ax1.hlines(y_max_1, x_min_1, x_max_1, 'w')
        ax1.hlines(y_min_1, x_min_1, x_max_1, 'w')
        ax1.vlines(x_max_1, y_max_1, y_min_1, 'w')
        ax1.vlines(x_min_1, y_max_1, y_min_1, 'w')
        ax1.hlines(y_max_2, x_min_2, x_max_2, 'w')
        ax1.hlines(y_min_2, x_min_2, x_max_2, 'w')
        ax1.vlines(x_max_2, y_max_2, y_min_2, 'w')
        ax1.vlines(x_min_2, y_max_2, y_min_2, 'w')
        ax1.hlines(y_max_3, x_min_3, x_max_3, 'w')
        ax1.hlines(y_min_3, x_min_3, x_max_3, 'w')
        ax1.vlines(x_max_3, y_max_3, y_min_3, 'w')
        ax1.vlines(x_min_3, y_max_3, y_min_3, 'w')
        c = ax1.contourf(X, Y, dewatering_rate, levels = levels_cmap, extend="both", cmap=custom_cmap)
        
        ax2.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
        ax2.set_xticks([], [])
        ax2.set_yticks([], [])

        ax2.set_xlim((x_min_1, x_max_1))
        ax2.set_ylim((y_min_1, y_max_1))
        c = ax2.contourf(X, Y, dewatering_rate, levels = levels_cmap, extend="both", cmap=custom_cmap)
        
        ax3.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
        ax3.set_xticks([], [])
        ax3.set_yticks([], [])
        # plt.xlim((750333, 750970))
        # plt.ylim((7.08948*10**6, 7.09007*10**6))
        ax3.set_xlim((x_min_2, x_max_2))
        ax3.set_ylim((y_min_2, y_max_2))
        c = ax3.contourf(X, Y, dewatering_rate, levels = levels_cmap, extend="both", cmap=custom_cmap)
        
        ax4.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
        ax4.set_xticks([], [])
        ax4.set_yticks([], [])
        # plt.xlim((750879, 751847))
        # plt.ylim((7.08889*10**6, 7.08974*10**6))
        ax4.set_xlim((x_min_3, x_max_3))
        ax4.set_ylim((y_min_3, y_max_3))
        c = ax4.contourf(X, Y, dewatering_rate, levels = levels_cmap, extend="both", cmap=custom_cmap)
        cbar = fig.colorbar(c, orientation='vertical', aspect=50, ticks=levels_ticks, ax=axes.ravel().tolist())
        cbar.ax.set_yticklabels(["Very good", "Good", "Moderate", "Unsatisfactory", "Bad"])
        cbar.ax.axes.tick_params(length=0)

    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig(f"dewatering_boxes.png", dpi=600, bbox_inches='tight', transparent=False)

    # plt.show()
if __name__ == '__main__':
    main()