import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import os
import findiff
import support_functions as sf
from matplotlib.lines import Line2D
from matplotlib import rc 
PATH_TO_DATA = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/results"
BACKGROUND_IMAGE = "../paper-1/geo_ref.tif"



def main():


    rc('text', usetex=True)
    X, Y, _ = sf.read_coordinate_data(PATH_TO_DATA + "/start-stop/24", 'water_level')
    level = sf.read_simulation_data(PATH_TO_DATA + "/50-21-5", 'water_level')
    depth = sf.read_simulation_data(PATH_TO_DATA + "/50-21-5", 'water_depth')
    vel = sf.read_simulation_data(PATH_TO_DATA + "/50-21-5", 'depth_averaged_velocity')
    background = sf.read_background_image(BACKGROUND_IMAGE)
    
    steady_state_50 = level[0]
    steady_state_50[~np.isnan(steady_state_50)] = 1
    steady_state_50[np.isnan(steady_state_50)] = 0
    steady_state_21 = level[-1]    
    steady_state_21[~np.isnan(steady_state_21)] = 1
    steady_state_21[np.isnan(steady_state_21)] = 0
    z = steady_state_50 - steady_state_21
    z[z==0] = np.NaN
    
    
    area_mask = np.ma.masked_invalid(z).mask

    salmon = False
    trout = False
    grayling = True
    if salmon:
        u_max = 0.60
        u_min = 0.15
        d_max = 0.55
        d_min = 0.25

    if trout:
        u_max = 0.50
        u_min = 0.20
        d_max = 0.30
        d_min = 0.20
        
    if grayling:
        u_max = 0.90
        u_min = 0.23
        d_max = 0.50
        d_min = 0.30
    depth_mask_21 = np.ma.masked_outside(depth[-1], d_min, d_max)
    mask = depth_mask_21.mask
    vel_mask_21 = np.ma.masked_outside(vel[-1], u_min, u_max)
    union_21 = np.ma.array(vel_mask_21, mask=mask)

    depth_mask_50 = np.ma.masked_outside(depth[0], d_min, d_max)
    mask = depth_mask_50.mask
    vel_mask_50 = np.ma.masked_outside(vel[0], u_min, u_max)
    union_50 = np.ma.array(vel_mask_50, mask=mask)
    union_50.filled(np.NaN)
    union_50_filled = union_50.filled(np.NaN)

    wet_spawning_area = np.ma.array(union_50_filled, mask=~area_mask)
    test = np.ma.array(wet_spawning_area, mask=union_21.mask)
    # area_wet = sf.compute_area(wet_spawning_area, X, Y)
    # area_intersection = sf.compute_area(test, X, Y)
    # print("Wet area: {}".format(area_wet / 10**4))
    # print("Intersection: {}".format(area_intersection))
    area_50 = sf.compute_area(steady_state_50, X, Y)
    area_21 = sf.compute_area(steady_state_21, X, Y)
    print("50: {}".format(area_50))
    print("21: {}".format(area_21))
    fig, ax = plt.subplots(1,1)
    ax.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    ax.set_xticks([], [])
    ax.set_yticks([], [])
    c = ax.contourf(X, Y, wet_spawning_area, colors=["c"])
    c = ax.contourf(X, Y, test, colors=["r"])
    custom_lines = [Line2D([0], [0], color="r", lw=4),
                Line2D([0], [0], color="c", lw=4)]
    ax.legend(custom_lines, ["Intersection", "Wetted Spawning Habitats"])

    # plt.show()

    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("wet_spawning_habitat_grayling.png", dpi=600, bbox_inches='tight', transparent=True)
if __name__ == "__main__":
    main()