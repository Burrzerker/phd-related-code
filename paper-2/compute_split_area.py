import numpy as np
import argparse
import scipy.io
import matplotlib.pyplot as plt
import os
from scipy.io import savemat as savemat
import support_functions as sf
from matplotlib import rc 
from matplotlib.lines import Line2D
PATH_TO_DATA = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/results"
BACKGROUND_IMAGE = "../paper-1/geo_ref.tif"

def parse_arguments():
    parser = argparse.ArgumentParser(description='Arguments for selecting cases and species.')
    parser.add_argument('--case', action='store', type=int, required=True)
    parser.add_argument('--species', action='store', type=str, required=True)

    args = parser.parse_args()
    return args

def main():
    rc('text', usetex=True)

    args = parse_arguments()
    
    X, Y, _ = sf.read_coordinate_data(PATH_TO_DATA + "/start-stop/{}".format(args.case), 'water_level')
    depth = sf.read_simulation_data(PATH_TO_DATA + "/start-stop/{}".format(args.case), 'water_depth')
    vel = sf.read_simulation_data(PATH_TO_DATA + "/start-stop/{}".format(args.case), 'depth_averaged_velocity')
    
  
    # background = sf.read_background_image(BACKGROUND_IMAGE)

    
    salmon = False
    trout = False
    grayling = False

    if args.species == "salmon":
        salmon = True
    if args.species == "trout":
        trout = True
    if args.species == "grayling":
        grayling = True

    if salmon:
        print("Chosen species is salmon.")
        # Salmon velocity and depth
        u_max = 0.60
        u_min = 0.15
        d_max = 0.55
        d_min = 0.25

    if trout:
        print("Chosen species is trout.")
        # Trout velocity and depth
        u_max = 0.50
        u_min = 0.20
        d_max = 0.30
        d_min = 0.20
        
    if grayling:
        print("Chosen species is grayling.")
        # Trout velocity and depth
        u_max = 0.90
        u_min = 0.23
        d_max = 0.50
        d_min = 0.30
    timesteps = np.linspace(0, len(depth[:, 0, 0]), len(depth[:, 0, 0]))
    area = np.zeros(len(timesteps))
    
    test_depth = depth[8, :, :]
    test_vel = vel[8, :, :]
    depth_mask = np.ma.masked_outside(test_depth, d_min, d_max)
    mask = depth_mask.mask
    vel_mask = np.ma.masked_outside(test_vel, u_min, u_max)
    union = np.ma.array(vel_mask, mask=mask)

    area_matrix = np.zeros(shape=(6, len(timesteps)))
    
    # box = 6

    # for number in range(1,6):
    #     print(number)
    #     sf.compute_box_area(union, X, Y, number)
    
    
    for index, timestep in enumerate(timesteps):
        depth_mask = np.ma.masked_outside(depth[index], d_min, d_max)
        mask = depth_mask.mask
        vel_mask = np.ma.masked_outside(vel[index], u_min, u_max)
        union = np.ma.array(vel_mask, mask=mask)
        for number in range(1,7):
            area_matrix[number-1, index]  = sf.compute_box_area(union, X, Y, number)
        # print("Area: {}".format(area[index] / 10**4))
        print("Completion: {:.2f} %".format(timestep * 100/ len(timesteps)))
    
    savemat('area_spawning_habitats/area_{}_habitat_{}.mat'.format(args.case, args.species, args.case), {'{}_habitat'.format(args.species): area_matrix})

    # fig, ax = plt.subplots(1,1)
    # ax.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    # ax.contourf(X, Y, union_21, [0.001, 1], colors=["r"])
    # ax.contourf(X, Y, union_50, [0.001, 1], colors=["c"])
    # ax.set_xticks([], [])
    # ax.set_yticks([], [])
    # # cmap = plt.cm.coolwarm
    # custom_lines = [Line2D([0], [0], color="r", lw=4),
    #             Line2D([0], [0], color="c", lw=4)]
    # ax.legend(custom_lines, ["$21~m^3/s$", "$50~m^3/s$"])

if __name__ == "__main__":
    main()
    # plt.show()