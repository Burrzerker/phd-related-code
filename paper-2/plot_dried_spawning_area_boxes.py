import numpy as np
import matplotlib.pyplot as plt
import os
import argparse as argparse
import support_functions as sf
from matplotlib import rc 
from scipy.io import loadmat

def parse_arguments():
    parser = argparse.ArgumentParser(description='Arguments for selecting cases and species.')
    parser.add_argument('--case', action='store', type=int, required=True)

    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    rc('text', usetex=True)
    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams['lines.markersize'] = 0.5
    trout = loadmat(f"area_dry_boxes/area_{args.case}_dried_habitat_trout.mat")['trout_habitat']
    grayling = loadmat(f"area_dry_boxes/area_{args.case}_dried_habitat_grayling.mat")['grayling_habitat']
    salmon = loadmat(f"area_dry_boxes/area_{args.case}_dried_habitat_salmon.mat")['salmon_habitat']

    time = np.linspace(0, len(trout[0,:]), len(trout[0,:]))
    lower_y_lim = 0
    upper_y_lim = 3000

    fig, axes = plt.subplots(3,2, sharex=True)
    ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = axes
    # fig.set_size_inches(8, 6)
    font_size = 16
    ax1.plot(time, grayling[0,:])
    ax1.plot(time, trout[0,:])
    ax1.plot(time, salmon[0,:])
    ax1.set_title("Zone 1", fontsize=font_size)
    ax1.set_ylim(lower_y_lim, upper_y_lim)
    ax1.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    
    ax2.plot(time, grayling[1,:])
    ax2.plot(time, trout[1,:])
    ax2.plot(time, salmon[1,:])
    ax2.set_title("Zone 2", fontsize=font_size)
    ax2.set_ylim(lower_y_lim, upper_y_lim)
    ax2.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    # ax2.legend(["European grayling", "Brown trout", "Atlantic salmon"], loc='upper right')

    ax3.plot(time, grayling[2,:])
    ax3.plot(time, trout[2,:])
    ax3.plot(time, salmon[2,:])
    ax3.set_title("Zone 3", fontsize=font_size)
    ax3.set_ylim(lower_y_lim, upper_y_lim)
    
    ax3.ticklabel_format(axis="y", style="sci", scilimits=(0,0))

    # ax4.set_ylabel("$A_{dry} (t)$ / $\max(A_{spawning}(t))$", fontsize=14)
    ax4.plot(time, grayling[3,:])
    ax4.plot(time, trout[3,:])
    ax4.plot(time, salmon[3,:])
    ax4.set_title("Zone 4", fontsize=font_size)
    # ax4.legend(["European grayling", "Brown trout", "Atlantic salmon"], loc='center left', bbox_to_anchor=(1, 0.5))
    ax4.set_ylim(lower_y_lim, upper_y_lim)
    ax4.ticklabel_format(axis="y", style="sci", scilimits=(0,0))

    ax5.plot(time, grayling[4,:])
    ax5.plot(time, trout[4,:])
    ax5.plot(time, salmon[4,:])
    ax5.set_title("Zone 5", fontsize=font_size)
    ax5.set_ylim(lower_y_lim, upper_y_lim)
    ax5.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    ax5.set_xlabel("t $[min]$", fontsize=font_size)
    
    ax6.plot(time, grayling[5,:])
    ax6.plot(time, trout[5,:])
    ax6.plot(time, salmon[5,:])
    ax6.set_ylim(lower_y_lim, upper_y_lim)
    ax6.set_title("Zone 6", fontsize=font_size)
    ax6.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    ax6.set_xlabel("t $[min]$", fontsize=font_size)
    # ax6.legend(["European grayling", "Brown trout", "Atlantic salmon"], loc='upper right')
    fig.text(-0.03, 0.5, "Potential spawning area $>LWM$ $[m^2]$", va='center', rotation='vertical', fontsize=font_size)
    fig.tight_layout(pad=0.5)
    
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig(f"dried_boxes_plots/dried_boxes_{args.case}.png", dpi=600, bbox_inches='tight', transparent=False)

    # plt.show()

if __name__ == "__main__":
    main()
