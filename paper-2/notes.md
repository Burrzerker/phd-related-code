# Notes Paper 2

Possible Impact on Salmonoid Habitats in a Bypass Reach Given Flexible Operating Conditions

## Salmonoid Specis in Stornorrfors
    - Atlantic Salmon 
    - Brown Trout
    - European Grayling 

## Things to Compute and tabulate
    - Compare max / min spawning habitat contours for the dynamic cases
    - Compare how the max / min spawning habitats compare to the steady state cases
    - Compute the area of the different dewatering times in the dewatering contour plots.

## Possible plots

### 50->21 m^3/s plots
    - Dewatered area (DONE, see dewatered_area.py)
    - Dewatering rate (maybe as a function of closing time) (Done)
        - Contour plot of different dewatering rates
          - >60 cm/h
          - 20-60 cm/h
          - 10-20 cm/h
          - <10 cm/h
    - Spawning habitats (DONE for all species, see spawning_habitats.py)
    - Intersection of spawning habitats for 50 m³/s and 21 m³/s cases (Done)

### Dynamics plots
    - Area of spawning habitats as a function of time (DONE)
    - Dried out spawning habitats as a function of time (DONE) (exposed eggs generally don't survive)
    
## Papers on Spawning Habitats
    - Atlantic Salmon (Spawning habitat of Atlantic Salmon and brown trout: General criteria and
    intragravel factors)
    - Brown Trout (Spawning habitat of Atlantic Salmon and brown trout: General criteria and
    intragravel factors)
    - European Grayling (https://onlinelibrary.wiley.com/doi/epdf/10.1002/rrr.3450030121)
