import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import os
import support_functions as sf
from matplotlib import rc 
from matplotlib.lines import Line2D
PATH_TO_DATA = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/results"
BACKGROUND_IMAGE = "background.tif"

def main():

    rc('text', usetex=True)
    X, Y, _ = sf.read_coordinate_data(PATH_TO_DATA + "/start-stop/24", 'water_level')
    depth = sf.read_simulation_data(PATH_TO_DATA + "/50-21-5", 'water_depth')
    vel = sf.read_simulation_data(PATH_TO_DATA + "/50-21-5", 'depth_averaged_velocity')
    level = sf.read_simulation_data(PATH_TO_DATA + "/50-21-5", 'water_level')
    background = sf.read_background_image(BACKGROUND_IMAGE)
    
    steady_state_50 = level[0]
    steady_state_50[~np.isnan(steady_state_50)] = 1
    steady_state_50[np.isnan(steady_state_50)] = 0
    steady_state_21 = level[-1]    
    steady_state_21[~np.isnan(steady_state_21)] = 1
    steady_state_21[np.isnan(steady_state_21)] = 0
    z = steady_state_50 - steady_state_21

    salmon = False
    trout = False
    grayling = True
    if salmon:
        u_max = 0.60
        u_min = 0.15
        d_max = 0.55
        d_min = 0.25

    if trout:
        u_max = 0.50
        u_min = 0.20
        d_max = 0.30
        d_min = 0.20
        
    if grayling:
        u_max = 0.90
        u_min = 0.23
        d_max = 0.50
        d_min = 0.30

    x_max_2 = 750970
    x_min_2 = 750333
    y_max_2 = 7.09007*10**6
    y_min_2 = 7.08948*10**6

    depth_mask_21 = np.ma.masked_outside(depth[-1], d_min, d_max)
    mask = depth_mask_21.mask
    vel_mask_21 = np.ma.masked_outside(vel[-1], u_min, u_max)
    union_21 = np.ma.array(vel_mask_21, mask=mask)

    depth_mask_50 = np.ma.masked_outside(depth[0], d_min, d_max)
    mask = depth_mask_50.mask
    vel_mask_50 = np.ma.masked_outside(vel[0], u_min, u_max)
    union_50 = np.ma.array(vel_mask_50, mask=mask)
    
    area_50 = sf.compute_area(union_50, X, Y)
    area_21 = sf.compute_area(union_21, X, Y)
    print("Q = 50: {}".format(area_50 / 10**4))
    print("Q = 21: {}".format(area_21 / 10**4))

    fig, ax = plt.subplots(1,1)
    ax.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    ax.contourf(X, Y, union_21, [0.001, 1], colors=["r"])
    ax.contourf(X, Y, union_50, [0.001, 1], colors=["c"])
    ax.set_xticks([], [])
    ax.set_yticks([], [])
    ax.set_xlim([x_min_2, x_max_2])
    ax.set_ylim([y_min_2, y_max_2])
    # cmap = plt.cm.coolwarm
    custom_lines = [Line2D([0], [0], color="r", lw=8),
                Line2D([0], [0], color="c", lw=8)]
    ax.legend(custom_lines, ["$21~m^3/s$", "$50~m^3/s$"], fontsize=14)
    
    # plt.figure()
    # plt.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    # plt.contourf(X, Y, union_21, [0.001, 1])
    
    # plt.figure()
    # plt.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    # plt.contourf(X, Y, union_50, [0.001, 1])
    
    
    # fig, axes = plt.subplots(1,2)
    # (ax1, ax2) = axes
    # ax1.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    # ax1.set_xticks([], [])
    # ax1.set_yticks([], [])
    # ax1.set_title("$Q=21 m^3/s$")
    # c = ax1.contourf(X, Y, union_21, [0.001, 1])

    # ax2.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    # ax2.set_xticks([], [])
    # ax2.set_yticks([], [])
    # ax2.set_title("$Q=50 m^3/s$")
    # c = ax2.contourf(X, Y, union_50, [0.001, 1])
    
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig("spawning_habitats_salmon.png", dpi=600, bbox_inches='tight', transparent=True)
if __name__ == '__main__':
    main()
    # plt.show()