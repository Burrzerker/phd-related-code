import numpy as np
import matplotlib.pyplot as plt
import os
import support_functions as sf
from matplotlib import rc 
from scipy.io import loadmat


def main():
    rc('text', usetex=True)
    plt.rcParams["font.family"] = "Times New Roman"
    wet_trout_24 = loadmat("area_data/24/area_trout_habitat_24_new.mat")['trout_habitat'][0]
    wet_grayling_24 = loadmat("area_data/24/area_grayling_habitat_24_new.mat")['grayling_habitat'][0]
    wet_salmon_24 = loadmat("area_data/24/area_salmon_habitat_24_new.mat")['salmon_habitat'][0]

    wet_trout_29 = loadmat("area_data/29/area_trout_habitat_29_new.mat")['trout_habitat'][0]
    wet_grayling_29 = loadmat("area_data/29/area_grayling_habitat_29_new.mat")['grayling_habitat'][0]
    wet_salmon_29 = loadmat("area_data/29/area_salmon_habitat_29_new.mat")['salmon_habitat'][0]

    wet_trout_36 = loadmat("area_data/36/area_trout_habitat_36_new.mat")['trout_habitat'][0]
    wet_grayling_36 = loadmat("area_data/36/area_grayling_habitat_36_new.mat")['grayling_habitat'][0]
    wet_salmon_36 = loadmat("area_data/36/area_salmon_habitat_36_new.mat")['salmon_habitat'][0]

    wet_trout_48 = loadmat("area_data/48/area_trout_habitat_48_new.mat")['trout_habitat'][0]
    wet_grayling_48 = loadmat("area_data/48/area_grayling_habitat_48_new.mat")['grayling_habitat'][0]
    wet_salmon_48 = loadmat("area_data/48/area_salmon_habitat_48_new.mat")['salmon_habitat'][0]

    wet_trout_72 = loadmat("area_data/72/area_trout_habitat_72_new.mat")['trout_habitat'][0]
    wet_grayling_72 = loadmat("area_data/72/area_grayling_habitat_72_new.mat")['grayling_habitat'][0]
    wet_salmon_72 = loadmat("area_data/72/area_salmon_habitat_72_new.mat")['salmon_habitat'][0]

    wet_trout_144 = loadmat("area_data/144/area_trout_habitat_144_new.mat")['trout_habitat'][0]
    wet_grayling_144 = loadmat("area_data/144/area_grayling_habitat_144_new.mat")['grayling_habitat'][0]
    wet_salmon_144 = loadmat("area_data/144/area_salmon_habitat_144_new.mat")['salmon_habitat'][0]

    dry_trout_24 = loadmat("area_data/24/area_trout_dried_habitat_24_new.mat")['trout_habitat'][0]
    dry_grayling_24 = loadmat("area_data/24/area_grayling_dried_habitat_24_new.mat")['grayling_habitat'][0]
    dry_salmon_24 = loadmat("area_data/24/area_salmon_dried_habitat_24_new.mat")['salmon_habitat'][0]

    dry_trout_29 = loadmat("area_data/29/area_trout_dried_habitat_29_new.mat")['trout_habitat'][0]
    dry_grayling_29 = loadmat("area_data/29/area_grayling_dried_habitat_29_new.mat")['grayling_habitat'][0]
    dry_salmon_29 = loadmat("area_data/29/area_salmon_dried_habitat_29_new.mat")['salmon_habitat'][0]

    dry_trout_36 = loadmat("area_data/36/area_trout_dried_habitat_36_new.mat")['trout_habitat'][0]
    dry_grayling_36 = loadmat("area_data/36/area_grayling_dried_habitat_36_new.mat")['grayling_habitat'][0]
    dry_salmon_36 = loadmat("area_data/36/area_salmon_dried_habitat_36_new.mat")['salmon_habitat'][0]

    dry_trout_48 = loadmat("area_data/48/area_trout_dried_habitat_48_new.mat")['trout_habitat'][0]
    dry_grayling_48 = loadmat("area_data/48/area_grayling_dried_habitat_48_new.mat")['grayling_habitat'][0]
    dry_salmon_48 = loadmat("area_data/48/area_salmon_dried_habitat_48_new.mat")['salmon_habitat'][0]

    dry_trout_72 = loadmat("area_data/72/area_trout_dried_habitat_72_new.mat")['trout_habitat'][0]
    dry_grayling_72 = loadmat("area_data/72/area_grayling_dried_habitat_72_new.mat")['grayling_habitat'][0]
    dry_salmon_72 = loadmat("area_data/72/area_salmon_dried_habitat_72_new.mat")['salmon_habitat'][0]

    dry_trout_144 = loadmat("area_data/144/area_trout_dried_habitat_144_new.mat")['trout_habitat'][0]
    dry_grayling_144 = loadmat("area_data/144/area_grayling_dried_habitat_144_new.mat")['grayling_habitat'][0]
    dry_salmon_144 = loadmat("area_data/144/area_salmon_dried_habitat_144_new.mat")['salmon_habitat'][0]

    time = np.linspace(0, len(dry_trout_24), len(dry_trout_24))

    lower_y_lim = 0
    upper_y_lim = 1

    fig, axes = plt.subplots(3,2, sharex=True)
    ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = axes
    # fig.set_size_inches(8, 6)
    font_size = 16
    ax6.plot(time, dry_grayling_24 / max(wet_grayling_24))
    ax6.plot(time, dry_trout_24 / max(wet_trout_24))
    ax6.plot(time, dry_salmon_24 / max(wet_salmon_24))
    ax6.set_title("60 changes per day", fontsize=font_size)
    ax6.set_ylim(lower_y_lim, upper_y_lim)
    ax6.legend(["European grayling", "Brown trout", "Atlantic salmon"], loc='upper right')
    ax6.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    ax6.set_xlabel("t $[min]$", fontsize=font_size)

    ax5.plot(time, dry_grayling_29 / max(wet_grayling_29))
    ax5.plot(time, dry_trout_29 / max(wet_trout_29))
    ax5.plot(time, dry_salmon_29 / max(wet_salmon_29))
    ax5.set_title("50 changes per day", fontsize=font_size)
    ax5.set_ylim(lower_y_lim, upper_y_lim)
    ax5.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    ax5.set_xlabel("t $[min]$", fontsize=font_size)

    ax4.plot(time, dry_grayling_36 / max(wet_grayling_36))
    ax4.plot(time, dry_trout_36 / max(wet_trout_36))
    ax4.plot(time, dry_salmon_36 / max(wet_salmon_36))
    ax4.set_title("40 changes per day", fontsize=font_size)
    ax4.set_ylim(lower_y_lim, upper_y_lim)
    # ax4.legend(["European grayling", "Brown trout", "Atlantic salmon"], loc='center left', bbox_to_anchor=(1, 0.5))
    ax4.ticklabel_format(axis="y", style="sci", scilimits=(0,0))

    # ax3.set_ylabel("$A_{dry} (t)$ / $\max(A_{spawning}(t))$", fontsize=14)
    ax3.plot(time, dry_grayling_48 / max(wet_grayling_48))
    ax3.plot(time, dry_trout_48 / max(wet_trout_48))
    ax3.plot(time, dry_salmon_48 / max(wet_salmon_48))
    ax3.set_title("30 changes per day", fontsize=font_size)
    ax3.set_ylim(lower_y_lim, upper_y_lim)
    ax3.ticklabel_format(axis="y", style="sci", scilimits=(0,0))

    ax2.plot(time, dry_grayling_72 / max(wet_grayling_72))
    ax2.plot(time, dry_trout_72 / max(wet_trout_72))
    ax2.plot(time, dry_salmon_72 / max(wet_salmon_72))
    ax2.set_title("20 changes per day", fontsize=font_size)
    ax2.set_ylim(lower_y_lim, upper_y_lim)
    ax2.ticklabel_format(axis="y", style="sci", scilimits=(0,0))

    ax1.plot(time, dry_grayling_144 / max(wet_grayling_144))
    ax1.plot(time, dry_trout_144 / max(wet_trout_144))
    ax1.plot(time, dry_salmon_144 / max(wet_salmon_144))
    ax1.set_ylim(lower_y_lim, upper_y_lim)
    ax1.set_title("10 changes per day", fontsize=font_size)
    ax1.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    fig.text(-0.02, 0.5, "Proportion of spawning area $>LWM$", va='center', rotation='vertical', fontsize=font_size)
    fig.tight_layout(pad=0.5)
    
    plt.savefig("dry_area_spawning_habitats_percent_ylabel.png", dpi=600, bbox_inches='tight', transparent=False)

    # plt.show()

if __name__ == "__main__":
    main()
