import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import os
import findiff
import support_functions as sf
from matplotlib import rc 

def main():    
    rc('text', usetex=True)
    plt.rcParams["font.family"] = "Times New Roman"
    number_of_minutes = 45
    t = np.linspace(0, number_of_minutes, number_of_minutes +1)

    Q_1 = one_minute(number_of_minutes)
    Q_5 = five_minutes(number_of_minutes)
    Q_15 = fifteen_minutes(number_of_minutes)
    Q_30 = thirty_minutes(number_of_minutes)

    fig, axes = plt.subplots(2,2, sharex=True)
    ((ax1, ax2), (ax3, ax4)) = axes
    ax1.plot(t, Q_1, 'k')
    ax1.set_title('1 minute closing time', fontsize=16)
    ax1.set_yticks([21, 50])
    ax1.set_ylabel("Discharge $[m^3/s]$", fontsize=16)
    
    ax2.set_title('5 minutes closing time', fontsize=16)
    ax2.plot(t, Q_5, 'k')
    ax2.set_yticks([21, 50])

    ax3.set_title('15 minutes closing time', fontsize=16)
    ax3.plot(t, Q_15, 'k')
    ax3.set_yticks([21, 50])
    ax3.set_ylabel("Discharge $[m^3/s$]", fontsize=16)
    ax3.set_xlabel("t [min]", fontsize=16)
    
    ax4.set_title('30 minutes closing time', fontsize=16)
    ax4.plot(t, Q_30, 'k')
    ax4.set_yticks([21, 50])
    ax4.set_xlabel("t [min]", fontsize=16)
    fig.tight_layout()
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig("dewatering_hydrographs.png", dpi=600, bbox_inches='tight', transparent=False)
    # plt.show()

def one_minute(number_of_minutes):
    Q = np.linspace(0, number_of_minutes, number_of_minutes + 1)
    Q[:11] = 50
    Q[11:] = 21
    return Q

def five_minutes(number_of_minutes):
    Q = np.linspace(0, number_of_minutes, number_of_minutes + 1)
    Q[:10] =  50
    linear_interpolation = np.linspace(50, 21, 5)
    Q[10:10 + len(linear_interpolation)] = linear_interpolation
    Q[10 + len(linear_interpolation):] = 21
    return Q

def fifteen_minutes(number_of_minutes):
    Q = np.linspace(0, number_of_minutes, number_of_minutes + 1)
    Q[:10] =  50
    linear_interpolation = np.linspace(50, 21, 15)
    Q[10:10 + len(linear_interpolation)] = linear_interpolation
    Q[10 + len(linear_interpolation):] = 21
    return Q

def thirty_minutes(number_of_minutes):
    Q = np.linspace(0, number_of_minutes, number_of_minutes + 1)
    Q[:10] =  50
    linear_interpolation = np.linspace(50, 21, 30)
    Q[10:10 + len(linear_interpolation)] = linear_interpolation
    Q[10 + len(linear_interpolation):] = 21
    return Q

if __name__ == "__main__":
    main()