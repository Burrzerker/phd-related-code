import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import os
import support_functions as sf
from matplotlib.lines import Line2D
from matplotlib import rc 
from mpl_toolkits.mplot3d import Axes3D
PATH_TO_DATA = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/results"
BACKGROUND_IMAGE = "../paper-1/geo_ref.tif"

def main():
    rc('text', usetex=True)

    X, Y, _ = sf.read_coordinate_data(PATH_TO_DATA + "/start-stop/24", 'water_level')
    level = sf.read_simulation_data(PATH_TO_DATA + "/50-21-5", 'water_level')
    background = sf.read_background_image(BACKGROUND_IMAGE)
    
    steady_state_50 = level[0]
    steady_state_50[~np.isnan(steady_state_50)] = 1
    steady_state_50[np.isnan(steady_state_50)] = 0
    steady_state_21 = level[-1]    
    steady_state_21[~np.isnan(steady_state_21)] = 1
    steady_state_21[np.isnan(steady_state_21)] = 0
    z = steady_state_50 - steady_state_21

    dewatered_area = sf.compute_area(z, X, Y)  
    print("Dewatered Area: {} km^2".format(dewatered_area / 10**6))

    fig, ax = plt.subplots(1,1)
    ax.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    ax.set_xticks([], [])
    ax.set_yticks([], [])
    custom_lines = [Line2D([0], [0], color="r", lw=4)]
    ax.legend(custom_lines, ["Dewatered Area"])
    c = ax.contourf(X, Y, z, [0.001, 1], colors=["r"])
    # fig.colorbar(c)
    # mng = plt.get_current_fig_manager()
    # mng.full_screen_toggle()
    # plt.savefig("dried_area_50_21.png", dpi=600, bbox_inches='tight', transparent=True)
    plt.show()

if __name__ == "__main__":
    main()