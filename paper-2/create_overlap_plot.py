import numpy as np
import argparse
import matplotlib.pyplot as plt
import os
import pickle
import support_functions as sf
from matplotlib import rc 
from matplotlib.colors import ListedColormap
from scipy.io import loadmat

BACKGROUND_IMAGE = "background.tif"


def main():
    rc('text', usetex=True)
    X, Y, _ = sf.read_coordinate_data("/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/results/start-stop/24", 'water_level')
    background = sf.read_background_image(BACKGROUND_IMAGE)

    x_max = np.amax(X)
    y_max = np.amax(Y)
    x_min = np.amin(X)
    y_min = np.amin(Y)
    print(x_min + (x_max - x_min)/4)
    print(y_min + (y_max - y_min)/4)

    fig, ax = plt.subplots(1,1)
    ax.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    plt.axis('off')

    # ax.set_xticks([], [])
    # ax.set_yticks([], [])
    
    # First box
    x_max = 747930
    x_min = 746600
    y_max = 7.09358*10**6
    y_min = 7.0928*10**6 

    x_max, x_min, y_max, y_min = sf.return_box_coordinates(1)

    plt.hlines(y_max, x_min, x_max, 'r')
    plt.hlines(y_min, x_min, x_max, 'r')
    plt.vlines(x_max, y_max, y_min, 'r')
    plt.vlines(x_min, y_max, y_min, 'r')

    # Second box
    x_max, x_min, y_max, y_min = sf.return_box_coordinates(2)

    plt.hlines(y_max, x_min, x_max, 'r')
    plt.hlines(y_min, x_min, x_max, 'r')
    plt.vlines(x_max, y_max, y_min, 'r')
    plt.vlines(x_min, y_max, y_min, 'r')
    
    # Third box
    x_max, x_min, y_max, y_min = sf.return_box_coordinates(3)

    plt.hlines(y_max, x_min, x_max, 'r')
    plt.hlines(y_min, x_min, x_max, 'r')
    plt.vlines(x_max, y_max, y_min, 'r')
    plt.vlines(x_min, y_max, y_min, 'r')
    
    # Fourth box
    x_max, x_min, y_max, y_min = sf.return_box_coordinates(4)

    plt.hlines(y_max, x_min, x_max, 'r')
    plt.hlines(y_min, x_min, x_max, 'r')
    plt.vlines(x_max, y_max, y_min, 'r')
    plt.vlines(x_min, y_max, y_min, 'r')

    # Fifth box
    x_max, x_min, y_max, y_min = sf.return_box_coordinates(5)

    plt.hlines(y_max, x_min, x_max, 'r')
    plt.hlines(y_min, x_min, x_max, 'r')
    plt.vlines(x_max, y_max, y_min, 'r')
    plt.vlines(x_min, y_max, y_min, 'r')

    # Sixth box
    x_max, x_min, y_max, y_min = sf.return_box_coordinates(6)

    plt.hlines(y_max, x_min, x_max, 'r')
    plt.hlines(y_min, x_min, x_max, 'r')
    plt.vlines(x_max, y_max, y_min, 'r')
    plt.vlines(x_min, y_max, y_min, 'r')
    # plt.show

    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig(f"overlap_plot.png", dpi=600, bbox_inches='tight', transparent=True)



if __name__ == "__main__":
    main()