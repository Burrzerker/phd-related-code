import support_functions as sf
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc
import matplotlib as mpl
from scipy.optimize import curve_fit
import os
PATH_TO_DATA = "../paper-1/simulation-data"




def predicted_function(t, offset, amplitude, frequency, phase):
    return offset + amplitude * np.cos(2*np.pi*t*frequency + phase)

def main():
    rc('text', usetex=True) 

    simulations = sf.return_simulation_cases(PATH_TO_DATA + "/start-stop")
    scenario_24 = sf.read_simulation_data(simulations[0])
    validation_points = sf.return_validation_point_coordinates()

    scenario = scenario_24[181:, validation_points['8'][0], validation_points['8'][1]]
    timesteps = len(scenario)
    t = np.linspace(0, timesteps, timesteps)

    y_offset = (max(scenario) - ((max(scenario)-min(scenario))/2))
    amplitude = (max(scenario) - min(scenario))/2
    print(amplitude)
    curve = y_offset + amplitude*np.cos((1/60)*2*np.pi*t)

    initial_guess = [y_offset, amplitude, 1/60, 0]

    fit, _ = curve_fit(predicted_function, t, scenario, p0=initial_guess)
    print(fit)
    fitted_curve = predicted_function(t, fit[0], fit[1], fit[2], fit[3])
    plt.figure()
    line_1, = plt.plot(scenario_24[183:, validation_points['8'][0], validation_points['8'][1]], 'ro')
    line_2, = plt.plot(fitted_curve)
    plt.ylabel("WSE")
    plt.xlabel("t [min]")
    plt.legend([line_1, line_2], ["Simulation", "$y+Acos(2\pi t/\omega)$"])
    # plt.legend("$y+Acos(2\pi t/\omega)$")
    plt.show()



if __name__ == "__main__":
    main()