import scipy
import numpy as np
import support_functions as sf
from matplotlib import pyplot as plt
from matplotlib import rc
import matplotlib as mpl
import support_functions as sf

BACKGROUND_IMAGE = "background.tif"

def main():
    rc('text', usetex=True)
    plt.rcParams["font.family"] = "Times New Roman"
    # bathymetry = scipy.io.loadmat("../paper-1/manning.mat")['data']['Val'][0][0]
    bathymetry = scipy.io.loadmat("../paper-1/initial_bed_level.mat")['data']['Val'][0][0]

    X, Y, velocity = sf.read_coordinate_data("../paper-1/simulation-data/50-21-data/50-21-1", "depth_averaged_velocity")
    background = sf.read_background_image(BACKGROUND_IMAGE)
    
    fig, ax = plt.subplots(1,1)
    c = plt.contourf(X, Y, bathymetry)
    fig.colorbar(c, ax=ax,orientation='vertical', shrink=1, aspect=25, pad=0.05).set_label(label=r'Elevation $[m.a.s.l]$',size=16,weight='normal')
    # fig.colorbar(c, ax=ax,orientation='vertical', shrink=1, aspect=25, pad=0.05).set_label(label=r'Manning Number $[s/m^{1/3}]$',size=16,weight='normal')

    ax.set_xticks([], [])
    ax.set_yticks([], [])
    plt.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    # tick_label = np.linspace(0, 5, 11)
    # fig, axes = plt.subplots(1, 2, constrained_layout=True)
    # (ax1, ax2) = axes
    # c = ax1.contourf(X, Y, velocity[0, :, :], cmap="jet", levels=np.linspace(0.001, 3, 100), extend="max")
    # # fig.colorbar(c, ax=ax1,orientation='vertical', shrink=1, aspect=35, pad=0.05).set_label(label=r'Depth Averaged Velocity $[m/s]$',size=16,weight='normal')

    # ax1.set_xticks([], [])
    # ax1.set_yticks([], [])
    # ax1.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])

    # c = ax2.contourf(X, Y, velocity[-1, :, :], cmap="jet", levels=np.linspace(0.001, 3, 100), extend="max")
    

    # ax2.set_xticks([], [])
    # ax2.set_yticks([], [])
    # ax2.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    # fig.colorbar(c, ax=axes.ravel().tolist(), orientation='vertical', shrink=0.5, aspect=25,  pad=0.01, ticks=tick_label).set_label(label=r'Depth Averaged Velocity $[m/s]$',size=8,weight='normal')
    # cax,kw = mpl.colorbar.make_axes([ax for ax in axes.flat])
    # plt.colorbar(c, ax=[ax1, ax2])
    # plt.subplots_adjust(wspace=0, hspace=0.5)
if __name__ == '__main__':
    main()
    # plt.tight_layout()
    # plt.show()
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig("bathymetry.png", dpi=600, bbox_inches='tight', transparent=False) # 
