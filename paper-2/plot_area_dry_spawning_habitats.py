import numpy as np
import matplotlib.pyplot as plt
import os
import support_functions as sf
from matplotlib import rc 
from scipy.io import loadmat

def main():
    rc('text', usetex=True)
    # plt.rcParams.update({'font.size': 12})
    trout_24 = loadmat("area_data/24/area_trout_dried_habitat_24_new.mat")['trout_habitat'][0]
    grayling_24 = loadmat("area_data/24/area_grayling_dried_habitat_24_new.mat")['grayling_habitat'][0]
    salmon_24 = loadmat("area_data/24/area_salmon_dried_habitat_24_new.mat")['salmon_habitat'][0]

    trout_29 = loadmat("area_data/29/area_trout_dried_habitat_29_new.mat")['trout_habitat'][0]
    grayling_29 = loadmat("area_data/29/area_grayling_dried_habitat_29_new.mat")['grayling_habitat'][0]
    salmon_29 = loadmat("area_data/29/area_salmon_dried_habitat_29_new.mat")['salmon_habitat'][0]

    trout_36 = loadmat("area_data/36/area_trout_dried_habitat_36_new.mat")['trout_habitat'][0]
    grayling_36 = loadmat("area_data/36/area_grayling_dried_habitat_36_new.mat")['grayling_habitat'][0]
    salmon_36 = loadmat("area_data/36/area_salmon_dried_habitat_36_new.mat")['salmon_habitat'][0]

    trout_48 = loadmat("area_data/48/area_trout_dried_habitat_48_new.mat")['trout_habitat'][0]
    grayling_48 = loadmat("area_data/48/area_grayling_dried_habitat_48_new.mat")['grayling_habitat'][0]
    salmon_48 = loadmat("area_data/48/area_salmon_dried_habitat_48_new.mat")['salmon_habitat'][0]

    trout_72 = loadmat("area_data/72/area_trout_dried_habitat_72_new.mat")['trout_habitat'][0]
    grayling_72 = loadmat("area_data/72/area_grayling_dried_habitat_72_new.mat")['grayling_habitat'][0]
    salmon_72 = loadmat("area_data/72/area_salmon_dried_habitat_72_new.mat")['salmon_habitat'][0]
    
    trout_144 = loadmat("area_data/144/area_trout_dried_habitat_144_new.mat")['trout_habitat'][0]
    grayling_144 = loadmat("area_data/144/area_grayling_dried_habitat_144_new.mat")['grayling_habitat'][0]
    salmon_144 = loadmat("area_data/144/area_salmon_dried_habitat_144_new.mat")['salmon_habitat'][0]

    time = np.linspace(0, len(salmon_24), len(salmon_24))
    
    upper_y_lim = 1.75 * 10**4
    lower_y_lim = 0

    fig, axes = plt.subplots(3,2, sharex=True)
    ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = axes
    font_size = 12
    fig.text(-0.02, 0.5, "Dried area of possible spawning habitats in $m^2$", va='center', rotation='vertical', fontsize=font_size)

    ax6.plot(time, trout_24)
    ax6.plot(time, grayling_24)
    ax6.plot(time, salmon_24)
    ax6.set_title("60 changes per day", fontsize=font_size)
    ax6.set_ylim(lower_y_lim, upper_y_lim)
    ax6.set_xlabel("Time in minutes", fontsize=font_size)
    ax6.ticklabel_format(axis="y", style="sci", scilimits=(0,0))

    ax5.plot(time, trout_29)
    ax5.plot(time, grayling_29)
    ax5.plot(time, salmon_29)
    ax5.set_title("50 changes per day", fontsize=font_size)
    ax5.set_ylim(lower_y_lim, upper_y_lim)
    ax5.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    ax5.set_xlabel("Time in minutes", fontsize=font_size)

    ax4.plot(time, trout_36)
    ax4.plot(time, grayling_36)
    ax4.plot(time, salmon_36)
    ax4.set_title("40 changes per day", fontsize=font_size)
    ax4.set_ylim(lower_y_lim, upper_y_lim)
    ax4.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    ax4.legend(["Trout", "Grayling", "Salmon"], loc='center left', bbox_to_anchor=(1, 0.5))

    ax3.plot(time, trout_48)
    ax3.plot(time, grayling_48)
    ax3.plot(time, salmon_48)
    ax3.set_title("30 changes per day", fontsize=font_size)
    ax3.set_ylim(lower_y_lim, upper_y_lim)
    ax3.ticklabel_format(axis="y", style="sci", scilimits=(0,0))

    ax2.plot(time, trout_72)
    ax2.plot(time, grayling_72)
    ax2.plot(time, salmon_72)
    ax2.set_title("20 changes per day", fontsize=font_size)
    ax2.set_ylim(lower_y_lim, upper_y_lim)
    ax2.ticklabel_format(axis="y", style="sci", scilimits=(0,0))

    ax1.plot(time, trout_144)
    ax1.plot(time, grayling_144)
    ax1.plot(time, salmon_144)
    ax1.set_ylim(lower_y_lim, upper_y_lim)
    ax1.set_title("10 changes per day", fontsize=font_size)
    ax1.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    fig.tight_layout(pad=0.5)


    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig("dry_spawning_habitats.png", dpi=600, bbox_inches='tight', transparent=True)
    # plt.show()

if __name__ == "__main__":
    main()