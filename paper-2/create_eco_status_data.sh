# Larval trout / grayling
python plot_dewatering_rate.py --level 1 --case 1
python plot_dewatering_rate.py --level 1 --case 5
python plot_dewatering_rate.py --level 1 --case 15
python plot_dewatering_rate.py --level 1 --case 30

python plot_dewatering_rate.py --level 2 --case 1
python plot_dewatering_rate.py --level 2 --case 5
python plot_dewatering_rate.py --level 2 --case 15
python plot_dewatering_rate.py --level 2 --case 30

python plot_dewatering_rate.py --level 3 --case 1
python plot_dewatering_rate.py --level 3 --case 5
python plot_dewatering_rate.py --level 3 --case 15
python plot_dewatering_rate.py --level 3 --case 30