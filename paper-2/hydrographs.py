import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc


def main():
    rc('text', usetex=True)
    plt.rcParams["font.family"] = "Times New Roman"
    nr_of_minutes = 60*6 # Time of hydrographs
    time = np.linspace(0, nr_of_minutes, nr_of_minutes)
    hydrograph_60 = start_stop_60(nr_of_minutes)
    hydrograph_50 = start_stop_50(nr_of_minutes)
    hydrograph_40 = start_stop_40(nr_of_minutes)
    hydrograph_30 = start_stop_30(nr_of_minutes)
    hydrograph_20 = start_stop_20(nr_of_minutes)
    hydrograph_10 = start_stop_10(nr_of_minutes)
    
    fig, axes = plt.subplots(3,2, sharex=True)
    ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = axes
    # fig.suptitle('Hydrographs at the inlet for the different scenarios', fontsize=10)
    font_size = 16
    ax1.plot(time, hydrograph_10, 'k')
    ax1.set_title('10 changes per day', fontsize=font_size)
    ax1.set_yticks([21, 50])
    ax2.plot(time, hydrograph_20, 'k')
    ax2.set_title('20 changes per day', fontsize=font_size)
    ax2.set_yticks([21, 50])
    ax3.set_ylabel('Discharge $[m^3/s]$', fontsize=font_size)
    ax3.set_yticks([21, 50])
    ax3.plot(time, hydrograph_30, 'k')
    ax3.set_title('30 changes per day', fontsize=font_size)    
    ax4.plot(time, hydrograph_40, 'k')
    ax4.set_yticks([21, 50])
    ax4.set_title('40 changes per day', fontsize=font_size)
    ax5.plot(time, hydrograph_50, 'k')
    ax5.set_title('50 changes per day', fontsize=font_size)
    ax5.set_yticks([21, 50])
    ax5.set_xlabel("t [min]", fontsize=font_size)      
    ax6.plot(time, hydrograph_60, 'k')
    ax6.set_yticks([21, 50])
    ax6.set_title('60 changes per day', fontsize=font_size)
    ax6.set_xlabel("t [min]", fontsize=font_size)
    fig.tight_layout()

    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig("hydrograph.png", dpi=600, bbox_inches='tight', transparent=False)

def start_stop_60(nr_of_minutes):
    "Flow change every 24th minute"
    hydrograph = np.zeros(nr_of_minutes)

    hydrograph[0] = 50
    hydrograph[1] = 50 - 5.8 * 1
    hydrograph[2] = 50 - 5.8 * 2
    hydrograph[3] = 50 - 5.8 * 3
    hydrograph[4] = 50 - 5.8 * 4
    hydrograph[5] = 50 - 5.8 * 5
    hydrograph[5:29] = 21
    hydrograph[29] = 21 + 5.8 * 1
    hydrograph[30] = 21 + 5.8 * 2 
    hydrograph[31] = 21 + 5.8 * 3
    hydrograph[32] = 21 + 5.8 * 4
    hydrograph[33] = 21 + 5.8 * 5
    hydrograph[33:57] = 50
    hydrograph[57] = 50 - 5.8 * 1
    hydrograph[58] = 50 - 5.8 * 2
    hydrograph[59] = 50 - 5.8 * 3
    hydrograph[60] = 50 - 5.8 * 4
    hydrograph[61] = 50 - 5.8 * 5
    hydrograph[61:85] = 21
    hydrograph[85] = 21 + 5.8 * 1
    hydrograph[86] = 21 + 5.8 * 2 
    hydrograph[87] = 21 + 5.8 * 3
    hydrograph[88] = 21 + 5.8 * 4
    hydrograph[89] = 21 + 5.8 * 5
    hydrograph[89:113] = 50
    hydrograph[113] = 50 - 5.8 * 1
    hydrograph[114] = 50 - 5.8 * 2
    hydrograph[115] = 50 - 5.8 * 3
    hydrograph[116] = 50 - 5.8 * 4
    hydrograph[117] = 50 - 5.8 * 5
    hydrograph[117:141] = 21
    hydrograph[141] = 21 + 5.8 * 1
    hydrograph[142] = 21 + 5.8 * 2 
    hydrograph[143] = 21 + 5.8 * 3
    hydrograph[144] = 21 + 5.8 * 4
    hydrograph[145] = 21 + 5.8 * 5
    hydrograph[145:169] = 50
    hydrograph[169] = 50 - 5.8 * 1
    hydrograph[170] = 50 - 5.8 * 2
    hydrograph[171] = 50 - 5.8 * 3
    hydrograph[172] = 50 - 5.8 * 4
    hydrograph[173] = 50 - 5.8 * 5
    hydrograph[173:197] = 21
    hydrograph[197] = 21 + 5.8 * 1
    hydrograph[198] = 21 + 5.8 * 2 
    hydrograph[199] = 21 + 5.8 * 3
    hydrograph[200] = 21 + 5.8 * 4
    hydrograph[201] = 21 + 5.8 * 5
    hydrograph[201:225] = 50
    hydrograph[225] = 50 - 5.8 * 1
    hydrograph[226] = 50 - 5.8 * 2
    hydrograph[227] = 50 - 5.8 * 3
    hydrograph[228] = 50 - 5.8 * 4
    hydrograph[229] = 50 - 5.8 * 5
    hydrograph[229:253] = 21
    hydrograph[253] = 21 + 5.8 * 1
    hydrograph[254] = 21 + 5.8 * 2 
    hydrograph[255] = 21 + 5.8 * 3
    hydrograph[256] = 21 + 5.8 * 4
    hydrograph[257] = 21 + 5.8 * 5
    hydrograph[257:281] = 50
    hydrograph[281] = 50 - 5.8 * 1
    hydrograph[282] = 50 - 5.8 * 2
    hydrograph[283] = 50 - 5.8 * 3
    hydrograph[284] = 50 - 5.8 * 4
    hydrograph[285] = 50 - 5.8 * 5
    hydrograph[285:309] = 21
    hydrograph[309] = 21 + 5.8 * 1
    hydrograph[310] = 21 + 5.8 * 2 
    hydrograph[311] = 21 + 5.8 * 3
    hydrograph[312] = 21 + 5.8 * 4
    hydrograph[313] = 21 + 5.8 * 5
    hydrograph[313:337] = 50
    hydrograph[337] = 50 - 5.8 * 1
    hydrograph[338] = 50 - 5.8 * 2
    hydrograph[339] = 50 - 5.8 * 3
    hydrograph[340] = 50 - 5.8 * 4
    hydrograph[341] = 50 - 5.8 * 5
    hydrograph[341:] = 21

    return hydrograph

def start_stop_50(nr_of_minutes):
    """
    Flow change every 29 minutes
    """
    hydrograph = np.zeros(nr_of_minutes)

    hydrograph[0] = 50
    hydrograph[1] = 50 - 5.8 * 1
    hydrograph[2] = 50 - 5.8 * 2
    hydrograph[3] = 50 - 5.8 * 3
    hydrograph[4] = 50 - 5.8 * 4
    hydrograph[5] = 50 - 5.8 * 5
    hydrograph[5:34] = 21
    hydrograph[34] = 21 + 5.8 * 1
    hydrograph[35] = 21 + 5.8 * 2 
    hydrograph[36] = 21 + 5.8 * 3
    hydrograph[37] = 21 + 5.8 * 4
    hydrograph[38] = 21 + 5.8 * 5
    hydrograph[38:67] = 50
    hydrograph[67] = 50 - 5.8 * 1
    hydrograph[68] = 50 - 5.8 * 2
    hydrograph[69] = 50 - 5.8 * 3
    hydrograph[70] = 50 - 5.8 * 4
    hydrograph[71] = 50 - 5.8 * 5
    hydrograph[71:100] = 21
    hydrograph[100] = 21 + 5.8 * 1
    hydrograph[101] = 21 + 5.8 * 2 
    hydrograph[102] = 21 + 5.8 * 3
    hydrograph[103] = 21 + 5.8 * 4
    hydrograph[104] = 21 + 5.8 * 5        
    hydrograph[104:133] = 50
    hydrograph[133] = 50 - 5.8 * 1
    hydrograph[134] = 50 - 5.8 * 2
    hydrograph[135] = 50 - 5.8 * 3
    hydrograph[136] = 50 - 5.8 * 4
    hydrograph[137] = 50 - 5.8 * 5
    hydrograph[137:166] = 21
    hydrograph[166] = 21 + 5.8 * 1
    hydrograph[167] = 21 + 5.8 * 2 
    hydrograph[168] = 21 + 5.8 * 3
    hydrograph[169] = 21 + 5.8 * 4
    hydrograph[170] = 21 + 5.8 * 5 
    hydrograph[170:199] = 50
    hydrograph[199] = 50 - 5.8 * 1
    hydrograph[200] = 50 - 5.8 * 2
    hydrograph[201] = 50 - 5.8 * 3
    hydrograph[202] = 50 - 5.8 * 4
    hydrograph[203] = 50 - 5.8 * 5
    hydrograph[203:232] = 21
    hydrograph[232] = 21 + 5.8 * 1
    hydrograph[233] = 21 + 5.8 * 2 
    hydrograph[234] = 21 + 5.8 * 3
    hydrograph[235] = 21 + 5.8 * 4
    hydrograph[236] = 21 + 5.8 * 5
    hydrograph[236:265] = 50
    hydrograph[265] = 50 - 5.8 * 1
    hydrograph[266] = 50 - 5.8 * 2
    hydrograph[267] = 50 - 5.8 * 3
    hydrograph[268] = 50 - 5.8 * 4
    hydrograph[269] = 50 - 5.8 * 5
    hydrograph[270:299] = 21
    hydrograph[299] = 21 + 5.8 * 1
    hydrograph[300] = 21 + 5.8 * 2 
    hydrograph[301] = 21 + 5.8 * 3
    hydrograph[302] = 21 + 5.8 * 4
    hydrograph[303] = 21 + 5.8 * 5
    hydrograph[303:332] = 50
    hydrograph[332] = 50 - 5.8 * 1
    hydrograph[333] = 50 - 5.8 * 2
    hydrograph[334] = 50 - 5.8 * 3
    hydrograph[335] = 50 - 5.8 * 4
    hydrograph[336] = 50 - 5.8 * 5
    hydrograph[336:] = 21
    return hydrograph

def start_stop_40(nr_of_minutes):
    """
    Flow change every 36 minute
    """
    hydrograph = np.zeros(nr_of_minutes)

    hydrograph[0] = 50
    hydrograph[1] = 50 - 5.8 * 1
    hydrograph[2] = 50 - 5.8 * 2
    hydrograph[3] = 50 - 5.8 * 3
    hydrograph[4] = 50 - 5.8 * 4
    hydrograph[5] = 50 - 5.8 * 5    
    hydrograph[5:41] = 21
    hydrograph[41] = 21 + 5.8 * 1
    hydrograph[42] = 21 + 5.8 * 2 
    hydrograph[43] = 21 + 5.8 * 3
    hydrograph[44] = 21 + 5.8 * 4
    hydrograph[45] = 21 + 5.8 * 5
    hydrograph[45:81] = 50
    hydrograph[81] = 50 - 5.8 * 1
    hydrograph[82] = 50 - 5.8 * 2
    hydrograph[83] = 50 - 5.8 * 3
    hydrograph[84] = 50 - 5.8 * 4
    hydrograph[85] = 50 - 5.8 * 5        
    hydrograph[85:121] = 21
    hydrograph[121] = 21 + 5.8 * 1
    hydrograph[122] = 21 + 5.8 * 2 
    hydrograph[123] = 21 + 5.8 * 3
    hydrograph[124] = 21 + 5.8 * 4
    hydrograph[125] = 21 + 5.8 * 5
    hydrograph[125:161] = 50
    hydrograph[161] = 50 - 5.8 * 1
    hydrograph[162] = 50 - 5.8 * 2
    hydrograph[163] = 50 - 5.8 * 3
    hydrograph[164] = 50 - 5.8 * 4
    hydrograph[165] = 50 - 5.8 * 5
    hydrograph[165:201] = 21
    hydrograph[201] = 21 + 5.8 * 1
    hydrograph[202] = 21 + 5.8 * 2 
    hydrograph[203] = 21 + 5.8 * 3
    hydrograph[204] = 21 + 5.8 * 4
    hydrograph[205] = 21 + 5.8 * 5
    hydrograph[205:241] = 50
    hydrograph[241] = 50 - 5.8 * 1
    hydrograph[242] = 50 - 5.8 * 2
    hydrograph[243] = 50 - 5.8 * 3
    hydrograph[244] = 50 - 5.8 * 4
    hydrograph[245] = 50 - 5.8 * 5
    hydrograph[245:281] = 21
    hydrograph[281] = 21 + 5.8 * 1
    hydrograph[282] = 21 + 5.8 * 2 
    hydrograph[283] = 21 + 5.8 * 3
    hydrograph[284] = 21 + 5.8 * 4
    hydrograph[285] = 21 + 5.8 * 5
    hydrograph[285:321] = 50
    hydrograph[321] = 50 - 5.8 * 1
    hydrograph[322] = 50 - 5.8 * 2
    hydrograph[323] = 50 - 5.8 * 3
    hydrograph[324] = 50 - 5.8 * 4
    hydrograph[325] = 50 - 5.8 * 5
    hydrograph[325:] = 21
    return hydrograph

def start_stop_10(nr_of_minutes):
    """
    Flow change every 144 minute
    """
    hydrograph = np.zeros(nr_of_minutes)

    hydrograph[0] = 50
    hydrograph[1] = 50 - 5.8 * 1
    hydrograph[2] = 50 - 5.8 * 2
    hydrograph[3] = 50 - 5.8 * 3
    hydrograph[4] = 50 - 5.8 * 4
    hydrograph[5] = 50 - 5.8 * 5
    hydrograph[5:149] = 21
    hydrograph[149] = 21 + 5.8 * 1
    hydrograph[150] = 21 + 5.8 * 2 
    hydrograph[151] = 21 + 5.8 * 3
    hydrograph[152] = 21 + 5.8 * 4
    hydrograph[153] = 21 + 5.8 * 5
    hydrograph[153:297] = 50
    hydrograph[297] = 50 - 5.8 * 1
    hydrograph[298] = 50 - 5.8 * 2
    hydrograph[299] = 50 - 5.8 * 3
    hydrograph[300] = 50 - 5.8 * 4
    hydrograph[301] = 50 - 5.8 * 5
    hydrograph[301:] = 21
  
    return hydrograph

def start_stop_20(nr_of_minutes):
    """
    Flow change every 72 minute
    """
    hydrograph = np.zeros(nr_of_minutes)
    hydrograph[0] = 50
    hydrograph[1] = 50 - 5.8 * 1
    hydrograph[2] = 50 - 5.8 * 2
    hydrograph[3] = 50 - 5.8 * 3
    hydrograph[4] = 50 - 5.8 * 4
    hydrograph[5] = 50 - 5.8 * 5
    hydrograph[5:77] = 21
    hydrograph[77] = 21 + 5.8 * 1
    hydrograph[78] = 21 + 5.8 * 2 
    hydrograph[79] = 21 + 5.8 * 3
    hydrograph[80] = 21 + 5.8 * 4
    hydrograph[81] = 21 + 5.8 * 5
    hydrograph[81:153] = 50
    hydrograph[153] = 50 - 5.8 * 1
    hydrograph[154] = 50 - 5.8 * 2
    hydrograph[155] = 50 - 5.8 * 3
    hydrograph[156] = 50 - 5.8 * 4
    hydrograph[157] = 50 - 5.8 * 5
    hydrograph[158:230] = 21
    hydrograph[230] = 21 + 5.8 * 1
    hydrograph[231] = 21 + 5.8 * 2 
    hydrograph[232] = 21 + 5.8 * 3
    hydrograph[233] = 21 + 5.8 * 4
    hydrograph[234] = 21 + 5.8 * 5
    hydrograph[234:306] = 50
    hydrograph[306] = 50 - 5.8 * 1
    hydrograph[307] = 50 - 5.8 * 2
    hydrograph[308] = 50 - 5.8 * 3
    hydrograph[309] = 50 - 5.8 * 4
    hydrograph[310] = 50 - 5.8 * 5
    hydrograph[310:] = 21     

    return hydrograph

def start_stop_30(nr_of_minutes):
    """
    Flow change every 48 minute
    """
    hydrograph = np.zeros(nr_of_minutes)
    hydrograph[0] = 50
    hydrograph[1] = 50 - 5.8 * 1
    hydrograph[2] = 50 - 5.8 * 2
    hydrograph[3] = 50 - 5.8 * 3
    hydrograph[4] = 50 - 5.8 * 4
    hydrograph[5] = 50 - 5.8 * 5
    hydrograph[5:53] = 21
    hydrograph[53] = 21 + 5.8 * 1
    hydrograph[54] = 21 + 5.8 * 2 
    hydrograph[55] = 21 + 5.8 * 3
    hydrograph[56] = 21 + 5.8 * 4
    hydrograph[57] = 21 + 5.8 * 5
    hydrograph[57:106] = 50
    hydrograph[106] = 50 - 5.8 * 1
    hydrograph[107] = 50 - 5.8 * 2
    hydrograph[108] = 50 - 5.8 * 3
    hydrograph[109] = 50 - 5.8 * 4
    hydrograph[110] = 50 - 5.8 * 5
    hydrograph[110:158] = 21
    hydrograph[158] = 21 + 5.8 * 1
    hydrograph[159] = 21 + 5.8 * 2 
    hydrograph[160] = 21 + 5.8 * 3
    hydrograph[161] = 21 + 5.8 * 4
    hydrograph[162] = 21 + 5.8 * 5
    hydrograph[162:210] = 50
    hydrograph[210] = 50 - 5.8 * 1
    hydrograph[211] = 50 - 5.8 * 2 
    hydrograph[212] = 50 - 5.8 * 3
    hydrograph[213] = 50 - 5.8 * 4
    hydrograph[214] = 50 - 5.8 * 5
    hydrograph[214:262] = 21
    hydrograph[262] = 21 + 5.8 * 1
    hydrograph[263] = 21 + 5.8 * 2
    hydrograph[264] = 21 + 5.8 * 3
    hydrograph[265] = 21 + 5.8 * 4
    hydrograph[266] = 21 + 5.8 * 5
    hydrograph[266:314] = 50
    hydrograph[314] = 50 - 5.8 * 1
    hydrograph[315] = 50 - 5.8 * 2 
    hydrograph[316] = 50 - 5.8 * 3
    hydrograph[317] = 50 - 5.8 * 4
    hydrograph[318] = 50 - 5.8 * 5
    hydrograph[319:] = 21

    return hydrograph

if __name__ == "__main__":
    main()
    # plt.show()
    # plt.tight_layout(rect=[0, 0.03, 1, 0.95])
