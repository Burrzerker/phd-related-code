import argparse
import numpy as np
import scipy.io
from scipy.io import savemat as savemat
import matplotlib.pyplot as plt
import os
import findiff
import support_functions as sf
from matplotlib.lines import Line2D
from matplotlib import rc 
PATH_TO_DATA = "/media/anton/7672291f-6e78-4bda-abdb-faa29b97992b/old_river_full/new-calibrated-simulations/results"
BACKGROUND_IMAGE = "../paper-1/geo_ref.tif"

def parse_arguments():
    parser = argparse.ArgumentParser(description='Arguments for selecting cases and species.')
    parser.add_argument('--case', action='store', type=int, required=True)
    parser.add_argument('--species', action='store', type=str, required=True)

    args = parser.parse_args()
    return args

def main():
    args = parse_arguments()


    rc('text', usetex=True)
    X, Y, _ = sf.read_coordinate_data(PATH_TO_DATA + "/start-stop/{}".format(args.case), 'water_level')
    level = sf.read_simulation_data(PATH_TO_DATA + "/start-stop/{}".format(args.case), 'water_level')
    depth = sf.read_simulation_data(PATH_TO_DATA + "/start-stop/{}".format(args.case), 'water_depth')
    vel = sf.read_simulation_data(PATH_TO_DATA + "/start-stop/{}".format(args.case), 'depth_averaged_velocity')
    
    # background = sf.read_background_image(BACKGROUND_IMAGE)
    
    steady_state_50 = level[0]
    steady_state_50[~np.isnan(steady_state_50)] = 1
    steady_state_50[np.isnan(steady_state_50)] = 0
    steady_state_21 = level[-1]    
    steady_state_21[~np.isnan(steady_state_21)] = 1
    steady_state_21[np.isnan(steady_state_21)] = 0
    z = steady_state_50 - steady_state_21
    z[z==0] = np.NaN
    
    area_mask = np.ma.masked_invalid(z).mask

    salmon = False
    trout = False
    grayling = False

    if args.species == "salmon":
        salmon = True
    if args.species == "trout":
        trout = True
    if args.species == "grayling":
        grayling = True

    if salmon:
        print("Chosen species is salmon.")
        # Salmon velocity and depth
        u_max = 0.60
        u_min = 0.15
        d_max = 0.55
        d_min = 0.25

    if trout:
        print("Chosen species is trout.")
        # Trout velocity and depth
        u_max = 0.50
        u_min = 0.20
        d_max = 0.30
        d_min = 0.20
        
    if grayling:
        print("Chosen species is grayling.")
        # Trout velocity and depth
        u_max = 0.90
        u_min = 0.23
        d_max = 0.50
        d_min = 0.30

    timesteps = np.linspace(0, len(depth[:, 0, 0]), len(depth[:, 0, 0]))
    area = np.zeros(len(timesteps))


    # depth_mask = np.ma.masked_outside(depth[70, :, :], d_min, d_max)
    # mask = depth_mask.mask
    # vel_mask = np.ma.masked_outside(vel[70, :, :], u_min, u_max)
    # union = np.ma.array(vel_mask, mask=mask)
    # union_filled = union.filled(np.NaN)
    # dry_spawning_area = np.ma.array(union_filled, mask=area_mask)
    # area  = sf.compute_area(dry_spawning_area, X, Y)
    # print(area)
    # plt.figure()
    # plt.contourf(X, Y, dry_spawning_area)

    for index, timestep in enumerate(timesteps):

        depth_mask = np.ma.masked_outside(depth[index], d_min, d_max)
        mask = depth_mask.mask
        vel_mask = np.ma.masked_outside(vel[index], u_min, u_max)
        union = np.ma.array(vel_mask, mask=mask)
        union_filled = union.filled(np.NaN)
        dry_spawning_area = np.ma.array(union_filled, mask=area_mask)
        area[index]  = sf.compute_area(dry_spawning_area, X, Y)
            # print("Area: {}".format(area[index] / 10**4))
        print("Completion: {:.2f} %".format(timestep * 100/ len(timesteps)))
    
    savemat('area_data_test/area_{}_dried_habitat_{}_new.mat'.format(args.case, args.species, args.case), {'{}_habitat'.format(args.species): area})

    # print(sf.compute_area(dry_spawning_area, X, Y))
    
    # fig, ax = plt.subplots(1,1)
    # ax.imshow(background, extent=[np.min(X), np.max(X), np.min(Y), np.max(Y)])
    # ax.set_xticks([], [])
    # ax.set_yticks([], [])
    # c = ax.contourf(X, Y, dry_spawning_area, colors=["r"])
    # custom_lines = [Line2D([0], [0], color="r", lw=4)]
    # ax.legend(custom_lines, ["Dry Spawning Habitats"])
    
    # plt.show()

if __name__ == "__main__":
    main()
    plt.show()